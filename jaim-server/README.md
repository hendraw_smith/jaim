Jaim Server
=====================

## A. Development Notes

1. Saat development, sebaiknya running di Linux.
2. Siapkan juga SPSE-4.1.2 

## B. Installation

### Pre Requisite

1. Terpasang JDK-1.8 terbaru di /home/appserv/java.
2. Terpasang PostgresSQL 9.6.x atau terbaru, pastikan default pg_dump menggunakan versi terbaru.
3. Terpasang git.
4. Command line zip, unzip, curl support HTTPS.

