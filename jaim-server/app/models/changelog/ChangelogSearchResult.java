package models.changelog;

import ext.Param;
import ext.contracts.PaginationContract;
import ext.contracts.PaginationContract.PageAble;
import models.main.Changelog;

import java.util.List;

/**
 * @author HanusaCloud on 6/4/2018
 */
public class ChangelogSearchResult implements PageAble<Changelog> {

    private List<Changelog> items;
    private transient int total = 0;
    private transient final PaginationContract.ParamAble paramAble;
    private Meta meta;

    public ChangelogSearchResult(PaginationContract.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getCurPage() {
        return paramAble.getPage();
    }

    @Override
    public int getTotalCurrentPage() {
        return items != null ? items.size() : 0;
    }

    @Override
    public long getTotal() {
        return total;
    }

    @Override
    public Param[] getParams() {
        return paramAble.getParams();
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<Changelog> collection) {
        this.items = collection;
    }

    @Override
    public List<Changelog> getItems() {
        return this.items;
    }

    @Override
    public Class<Changelog> getReturnType() {
        return Changelog.class;
    }

    @Override
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public Meta getMeta() {
        return this.meta;
    }

}
