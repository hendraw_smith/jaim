package models.changelog;

import play.data.validation.Required;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author HanusaCloud on 6/4/2018
 */
public class ChangelogDataForm {

    public Long id;
    @Required
    public String stringDate;
    @Required
    public String description;
    @Required
    public String appId;

    public Date getDate() {
        try {
            return new SimpleDateFormat("dd-MM-yyyy").parse(stringDate);
        } catch (Exception e) {
            return null;
        }
    }

}
