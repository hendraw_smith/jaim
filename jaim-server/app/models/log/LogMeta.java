package models.log;

/**
 * Created by dadang on 4/5/18.
 */
public class LogMeta {

	public String date;
	public String repoId;
	public String type;
	public String status;
	public String sp_id;

	public enum Type {
		BACKUP,
		UPDATE
	}

	public enum Status {
		SUCCESS,
		FAILED
	}

	public int getRepoId(){
		return (repoId != null && !repoId.isEmpty()) ? Integer.valueOf(repoId) : 0;
	}

	public int getSpId(){
		return (sp_id != null && !sp_id.isEmpty()) ? Integer.valueOf(sp_id) : 0;
	}
}
