package models.git;

import com.google.gson.annotations.SerializedName;

public class PushBody {

	@SerializedName("object_kind")
	public String objectKind;
	public String ref;
}
