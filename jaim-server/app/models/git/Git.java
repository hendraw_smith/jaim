package models.git;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import play.Logger;
import play.Play;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 
 * @author Andik
 *
 */
public class Git {

	private String workingFolder;
	
	private static String GIT_EXECUTABLE;
	
	public Git(String workingFolder)
	{
		this.workingFolder=FilenameUtils.separatorsToSystem(workingFolder);
		if(SystemUtils.IS_OS_WINDOWS)
			GIT_EXECUTABLE=Play.configuration.getProperty("git.executable.windows");
		else
			GIT_EXECUTABLE=Play.configuration.getProperty("git.executable.linux");
		GIT_EXECUTABLE=FilenameUtils.separatorsToSystem(GIT_EXECUTABLE);
	}

	public String pull() throws IOException
	{
		return command("pull");
	}
	
	public String fetch() throws IOException
	{
		return command("fetch origin");
	}


	public String log(int logCount) throws IOException
	{
		return command("log -n " + logCount);
	}
	
	public String status() throws IOException
	{
		return command("status");
	}
	
	private String command(String cmd) throws IOException
	{
		ProcessBuilder pb=new ProcessBuilder((GIT_EXECUTABLE + " " + cmd).split(" +"));
		pb.directory(new File(workingFolder));
		Process proc=pb.start();
		InputStream is=proc.getInputStream();
		InputStream err=proc.getErrorStream();
		try {
			proc.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String str= IOUtils.toString(is, "UTF-8");
		String errStr= IOUtils.toString(err, "UTF-8");
		if(StringUtils.isNotEmpty(errStr))
			Logger.error("[git %s] %s", cmd, errStr);

		proc.destroy();
		return str;
	}
	
}
