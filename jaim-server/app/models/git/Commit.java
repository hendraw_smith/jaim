package models.git;

import java.util.Date;

/**
 * Created by dadang on 7/9/18.
 */
public class Commit {

	public String id;
	public String short_id;
	public Date created_at;
	public String title;
	public String message;

}
