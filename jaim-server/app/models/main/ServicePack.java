package models.main;

import jaim.common.model.BaseApplication;
import models.agent.Packager;
import models.search.ServicePackHistoryQuery;
import models.search.ServicePackSearchResult;
import org.apache.commons.io.FileUtils;
import play.Logger;
import play.Play;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@play.db.jdbc.Table(name="service_pack")
/**ServicePack berisi data sebuah SP atas satu aplikasi yang dikirim LPSE
 * 
 * @author andik
 *
 */
public class ServicePack extends BaseTable {


	@Enumerated(EnumType.STRING)
	public enum ServicePackStatus
	{
		NEW("Terdapat versi SPSE terbaru"), //baru dibuat
		READY_TO_UPDATE("Siap melakukan update"),
		DOWNLOADED("File update sudah didownload oleh SPSE"),//SP sudah didownload oleh LPSE
		CANCEL("Batal update SPSE"), //dibatalkan
		NO_UPDATE("Tidak ada update terbaru"), //tidak perlu ada update
		TOUCH_FAILED("Proses check SPSE gagal. SPSE tidak bisa diakses."), //gagal touch ke lkpp
		TOUCH("Proses check/menginfokan SPSE"), //sudah touch ke SPSE
		BACKUP_DB("Proses backup database"), //sedang backup db
		BACKUP_DB_FAILED("Gagal backup database"), //sedang backup db
		MIGRATION("Migrasi Database"), //sedang migrasi
		BACKUP_FILE("Proses backup spse"), // sedang backup file
		BACKUP_FILE_FAILED("Gagal backup file."), // sedang backup file failed
		UPDATE("Proses update SPSE"),//sedang update aplikasi
		UPDATE_FAILED("Gagal proses update file SPSE"), // update failed
		START("Flag mulai"),//flag mulai
		FINISH("Flag selesai"),
		DB_REQUIREMENT_PASSED("DB requirements passed"),
		DB_REQUIREMENT_FAILED("DB requirements failed"),
		PACKAGE_REQUIREMENT_PASSED("Package requirements passed"),
		PACKAGE_REQUIREMENT_FAILED("Package requirements failed");//flag berakhir

		public boolean isNew()
		{
			return NEW.equals(this);
		}

		public boolean isTouch() {
			return TOUCH.equals(this);
		}

		public final String desc;

		ServicePackStatus(String desc) {
			this.desc = desc;
		}

	}
	
	public boolean isSegera()
	{
		return scheduled_date != null && scheduled_date.getTime()== 0L;
	}
	
	public enum ScheduledDate
	{
		SEGERA
	}
	
	@Id
	public Long sp_id; //id service pack
	public String app_id;
	public Integer lpse_id;
	public boolean needs_update;
	
	public ServicePackStatus status;
	public Date create_date;
	public Date update_date;
	
	//jika field ini NULL maka, scheduled_date berarti SEGERA 30 detik setelah selesai download
	public Date scheduled_date;
	public Date download_date;
	public String keterangan;
	public Integer need_migration = 0; // = 0 jika tidak perlu, = 1 jika butuh migrasi, = 2 jika butuh backup, = 3 jika butuh backup dan migrasi
	public boolean do_backup;

	@Transient
	public Lpse lpse;
	@Transient
	public boolean updateWithFile;

	public String getBaseFolder(){
		return String.format("%s/tmp/%d-%03d/%08d", Play.applicationPath, lpse_id, lpse.repo_id, sp_id);
	}

	public String toString()
	{
		return String.format("[%s] %s:%s, schedule: %s", sp_id, lpse_id, app_id, scheduled_date);
	}
	
	public String getShellScriptFile()
	{
		return String.format("%s/JaimAgent.%s.sh", getBaseFolder(), app_id);
	}
	
	public String getZipPackageFile()
	{
		return String.format("%s/pkg.%s.zip", getBaseFolder(), app_id);
		
	}
	
	/**Buatkan SP baru. Jika masih ada yg status NEW maka error
	 * @param lpse_id
	 * @param app_id
	 * @param scheduled_date 
	 * @return
	 */
	public static ServicePack newServicePack(int lpse_id, String app_id, Date scheduled_date)
	{
		ServicePack sp=new ServicePack();
		
		sp.app_id=app_id;
		sp.status=ServicePackStatus.NEW;
		sp.create_date=new Date();
		sp.lpse_id=lpse_id;
		sp.scheduled_date=scheduled_date;
		sp.save();
		return sp;
	}
	/**Dapatkan isi dari file kerja
	 * @return
	 * @throws IOException 
	 */
	public Map<String, String> getWorkingFileContent() throws IOException
	{
		File tmpFile = new File(getBaseFolder());
		File[] files = tmpFile.listFiles();
		Map<String, String> values = new HashMap<>();
		String logFile = getUpdateLogFileName();
		String backupLogFile = getBackupLogFileName();
		if(files!=null)
			for(File file: files)
				/**1. file .sh
				 * 2. file .new
				 * 3. file .del
				 * 4. file .log
				 */
			{
				String name=file.getName();
				if(name.endsWith(".sh") || name.contains("new" ) || name.contains("del" ) || name.endsWith(logFile)
						|| name.endsWith(backupLogFile))
				{
					String str=FileUtils.readFileToString(file, "UTF-8");
					values.put(file.getName(), str);
				}
			}
		return values;
	}


	/**Generate file untuk dikirim ke LPSE
	 */
	public void generateFiles(BaseApplication appOfAgent, String jaimLogDir, String spseRootDir, String backupDbDir)
	{
		//clean temp folder
		try {
			File file=new File(getBaseFolder());
			file.mkdirs();
			FileUtils.cleanDirectory(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		needs_update=false;
		
		//c. Generate zip
		Application appOfServer=Application.findById(app_id);
		if (lpse == null) {
			withLpse();
		}
		// Gunakan versi -dev jika lpse ditandai untuk gunakan versi development
		if(lpse.is_development && appOfServer.name.contains("spse") && !appOfServer.name.endsWith("dev")){
		    String dev_id = appOfServer.name + "-dev";
			Application appOfServerDev = Application.findById(dev_id);
			if(appOfServerDev!=null)
				appOfServer = appOfServerDev;
		}
		Packager pack=new Packager(lpse.repo_id, appOfAgent, appOfServer, this, lpse);
		try {
			long delay= 0; //this.scheduled_date.getTime() - System.currentTimeMillis();
			delay=delay/1000;
			needs_update=pack.buildPackage(delay, getZipPackageFile(), getShellScriptFile(), jaimLogDir, getBaseFolder(), spseRootDir, backupDbDir);
			updateWithFile = !pack.faNewAndModifiedFiles.isEmpty();
			Logger.debug("[ServicePack] sp: %s, lpse: %s, needsUpdate: %s", sp_id, lpse.repo_id, needs_update);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		save();
	}

	public static Long nextSequence() {
		return Query.find("SELECT NEXTVAL('service_pack_id_seq')", Long.class).first();
	}

	public void prePersist() {
		update_date = new Date();
		withLpse();
		if(sp_id == null){
			sp_id = nextSequence();
		}
	}

	public Lpse getLpse() {
		return Lpse.findById(lpse_id);
	}

	public String getLogFilePathString() {
		return String.format("%s/Update.%08d.log", getBaseFolder(), sp_id);
	}

	public String getBackupLogFilePatString() {
		return String.format("%s/Backup.%08d.log", getBaseFolder(), sp_id);
	}

	public String getUpdateLogFileName() {
		return String.format("Update.%08d.log", sp_id);
	}

	public String getBackupLogFileName() {
		return String.format("Backup.%08d.log", sp_id);
	}

	public void withLpse(){
		lpse = Lpse.findById(lpse_id);
	}

	public static ServicePackSearchResult search(ServicePackHistoryQuery spQuery, int lpse_id){
		ServicePackSearchResult result = new ServicePackSearchResult(spQuery);
		StringBuilder sb = new StringBuilder();
		String keyword = spQuery.getKeyword();
		sb.append("SELECT * FROM service_pack")
				.append(" WHERE lpse_id = ").append(lpse_id);
		if (keyword != null && keyword != ""){
			sb.append(" AND sp_id = ").append(keyword).append(" OR app_id like ").append("'%").append(keyword).append("%'");
		}
		sb.append(" ORDER BY sp_id DESC, update_date DESC");
		result.executeQuery(sb.toString());

		return result;
	}

	public static Double countSuccessAverage() {

		long totalPatch = Query.find("select l.lpse_id from lpse l join service_pack sp on l.lpse_id = sp.lpse_id " +
						"and sp.sp_id = (select sp_id from service_pack spp where spp.lpse_id = l.lpse_id order by sp_id DESC limit 1) " +
						"where l.repo_id <> 999 AND sp.status <> ? AND sp.status <> ?", String.class,
				ServicePackStatus.NEW, ServicePackStatus.TOUCH).fetch().size();
		long totalSuccessPatch = Query.find("select l.lpse_id from lpse l join service_pack sp on l.lpse_id = sp.lpse_id " +
						"and sp.sp_id = (select sp_id from service_pack spp where spp.lpse_id = l.lpse_id order by sp_id DESC limit 1) " +
						"where l.repo_id <> 999 AND (sp.status = ? OR sp.status = ? OR sp.status = ?)", String.class,
				ServicePackStatus.FINISH, ServicePackStatus.DOWNLOADED, ServicePackStatus.NO_UPDATE).fetch().size();

		if(totalPatch == 0 || totalSuccessPatch == 0) {
			return 0d;
		}

		return Double.valueOf((totalSuccessPatch * 100) / totalPatch);
	}

	public static List<ServicePack> sp10LastUpdate() {
		return Query.find("SELECT sp.* FROM lpse l join service_pack sp ON l.lpse_id = sp.lpse_id " +
						"AND sp.sp_id = (SELECT sp_id FROM service_pack spp WHERE spp.lpse_id = l.lpse_id order by sp_id DESC LIMIT 1) " +
						"WHERE sp.update_date IS NOT NULL AND (sp.status = ? OR sp.status like '%FAILED') " +
						"ORDER BY sp.update_date DESC LIMIT 10", ServicePack.class, ServicePackStatus.FINISH.name()).fetch();
	}
}
