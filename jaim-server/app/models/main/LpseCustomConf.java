package models.main;


import com.google.gson.annotations.Expose;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.Date;
import java.util.List;

@Table(name = "lpse_custom_conf")
public class LpseCustomConf extends BaseTable {

	@Id(sequence = "seq_lpse_custom_conf")
	public Long id;

	@Expose
	public String key;

	@Expose
	public String value;

	public String info;

	public Integer lpse_id;

	// centrum user ID
	public Long created_by_id;

	public Date create_date;

	protected void prePersist() {
		create_date = new Date();
	}

	public static List<LpseCustomConf> findByLpse(Integer lpse_id) {
		return find("lpse_id = ?", lpse_id).fetch();
	}

	public static void deleteAllByLpse(Integer lpse_id) {
		Query.update("DELETE from lpse_custom_conf where lpse_id = ?", lpse_id);
	}

}
