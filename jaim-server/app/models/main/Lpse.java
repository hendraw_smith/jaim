package models.main;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import jaim.common.model.BaseLpse;
import jaim.common.util.Version;
import models.search.LpseSearchQuery;
import models.search.LpseSearchResult;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import play.Logger;
import play.Play;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.libs.Json;
import play.libs.WS;
import utils.UUIDGenerator;

import javax.persistence.MappedSuperclass;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Table
@MappedSuperclass
public class Lpse extends BaseLpse {

	private static final long LATIHAN_REPO_ID = 999;

	//ID dari AppUpdate terakhir
	public Long app_update_id;
	public boolean verified; //jika true, artinya Admin telah mengecek validitas lpse_URL terhadap lpse_id

	/** flag bahwa LPSE ini boleh update otomatis atau tidak
	 *  Admin Jaim harus mengeset flag ini secara manual supaya tidak otomatis terupdate
	 *  Jika telah 'updated' maka diset oleh sistem otomatis jadi false
	 */
	public boolean allow_update;

	/** flag bahwa LPSE ini menginstal versi development
	 */
	public boolean is_development;
	
	//hello terakhir ke server
	public Date last_hello;
	
	/**Informasi tentang LPSE: OS, JDK, Memory, dsb 
	 */
	public String host_info;
	public String client_info;
	public String uuid;

	//dapatkan SP terakhir
	public ServicePack getLastServicePack() {
		return ServicePack.find("lpse_id=? and update_date is not null ORDER BY update_date DESC", lpse_id).first();
	}

	public static Lpse findByToken(String token) {
		return find("token =?", token).first();
	}

	public static LpseSearchResult search(LpseSearchQuery model, Object... params) {
		LpseSearchResult result = new LpseSearchResult(model);

		StringBuilder sb = new StringBuilder("SELECT l.* FROM lpse l LEFT JOIN service_pack sp ");
		sb.append("ON l.lpse_id = sp.lpse_id AND sp.sp_id = (SELECT spp.sp_id from service_pack spp where spp.lpse_id = " +
				"l.lpse_id AND spp.update_date IS NOT NULL ORDER BY spp.update_date DESC limit 1) ");
		sb.append("WHERE l.repo_id <> ").append(999).append(" ");
		sb.append("AND l.ip_address IS NOT NULL AND l.spse4_version IS NOT NULL ");
		params = generateQuery(model, sb, params);

		sb.append("ORDER BY sp.update_date ASC, l.repo_id ASC ");
		result.executeQuery(sb.toString(), params);
		return result;
	}

	public static LpseSearchResult searchLatihan(LpseSearchQuery model, Object... params) {
		LpseSearchResult result = new LpseSearchResult(model);

		StringBuilder sb = new StringBuilder("SELECT l.* FROM lpse l LEFT JOIN service_pack sp ");
		sb.append("ON l.lpse_id = sp.lpse_id AND sp.sp_id = (SELECT spp.sp_id from service_pack spp where spp.lpse_id = " +
				"l.lpse_id AND spp.update_date IS NOT NULL ORDER BY spp.update_date DESC limit 1) ");
		sb.append("WHERE l.repo_id = ").append(999).append(" ");
		sb.append("AND l.ip_address IS NOT NULL AND l.spse4_version IS NOT NULL ");
		params = generateQuery(model, sb, params);

		sb.append("ORDER BY sp.update_date ASC, l.repo_id ASC ");
		Logger.info(sb.toString());
		result.executeQuery(sb.toString(), params);
		return result;
	}

	private static Object[] generateQuery(LpseSearchQuery model, StringBuilder sb, Object[] params) {
		if(model.getQueryMap() != null){
			String key = model.getQueryMap().keySet().toArray(new String[1])[0];
			String val = model.getQueryMap().values().toArray(new String[1])[0];
			if(StringUtils.isNumeric(val)){
				sb.append("AND ").append(key).append(" = ?");
				params = ArrayUtils.add(params, Long.parseLong(val));
			} else {
				sb.append("AND upper(").append(key).append(") LIKE ?");
				params = ArrayUtils.add(params, "%" + val.toUpperCase() + "%");
			}
		} else if(model.getKeyword() != null && !model.getKeyword().isEmpty()) {
			String keywordLike = "%" + model.getKeyword().toUpperCase() + "%";
			sb.append("AND ( ");
			sb.append("upper(l.lpse_nama) LIKE ? ");
			params = ArrayUtils.add(params, keywordLike);
			sb.append("OR upper(l.lpse_url) LIKE ? ");
			params = ArrayUtils.add(params, keywordLike);
			sb.append("OR upper(sp.status) LIKE ? ");
			params = ArrayUtils.add(params, keywordLike);
			if(StringUtils.isNumeric(model.getKeyword())){
				sb.append("OR l.repo_id = ? ");
				params = ArrayUtils.add(params, Integer.valueOf(model.getKeyword()));
				sb.append("OR sp.sp_id = ? ");
				params = ArrayUtils.add(params, Integer.valueOf(model.getKeyword()));
			}
			sb.append(") ");
		}
		return params;
	}

	public static boolean lpseIsExist(String url){
		return (find("lpse_url = ?", url).first() != null);
	}

	public boolean isExist() {
		if(repo_id != LATIHAN_REPO_ID) {
			return Lpse.find("repo_id = ? OR lpse_url = ?", repo_id, lpse_url).first() != null;
		}
		return Lpse.find("lpse_url = ?", lpse_url).first() != null;
	}

	public static boolean lpseIsExist(String url, int repo_id){
		if(repo_id != LATIHAN_REPO_ID) {
			return (find("repo_id = ? OR lpse_url = ?", repo_id, url).first() != null);
		}
		return lpseIsExist(url);
	}

	public String toString()
	{
		return String.format("[%s] %s - %s", repo_id, lpse_nama, lpse_url);
	}
	
	public void preDelete()
	{
		//delete SP
		ServicePack.delete("lpse_id=?", lpse_id);
	}

	public static Integer nextSequence() {
		return Query.find("SELECT NEXTVAL('lpse_id_seq')", Integer.class).first();
	}

	public void prePersist(){
		if(lpse_id == null){
			lpse_id = nextSequence();
		}
		if(uuid == null){
			try {
				// gabungan antara url lpse dan nama lpse
				uuid = UUIDGenerator.generateString(lpse_url, lpse_nama);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		if(!StringUtils.isEmpty(spse4_version)) {
			Version minVer = new Version("4.2");
			Version lpseVer = new Version(spse4_version);
			if(minVer.compareTo(lpseVer) == 1) {
				Logger.debug("[Lpse] LPSE version is lower than 4.2: %s", lpse_url);
				allow_update = false;
			}
		}
	}

	public static Lpse getLpse(int repo_id){
		return find("repo_id = ?", repo_id).first();
	}

	public static Lpse getLpse(int repo_id, String ip_address){
		return find("repo_id = ? and ip_address = ?", repo_id, ip_address).first();
	}

	public static Lpse getLpse(int repo_id, String ip_address, String uuid){
		if(uuid == null) uuid = "";
		return find("repo_id = ? and (ip_address = ? or uuid = ?)", repo_id, ip_address, uuid).first();
	}

	public static Lpse getLpse(String uuid) {
		return find("uuid = ?", uuid).first();
	}

	public void readIpAddress(){
		ip_address = getIpAddress(lpse_url);
	}

	public static String getIpAddress(String url){
		String ip_address = null;
		try {
			InetAddress inetAddress = InetAddress.getByName(new URL(url).getHost());
			ip_address = inetAddress.getHostAddress();
		} catch (UnknownHostException e) {
			Logger.error("[Lpse] ERROR %s unknown host.", url);
			e.printStackTrace();
		} catch (MalformedURLException e) {
			Logger.error("[Lpse] ERROR %s invalid url", url);
			e.printStackTrace();
		}
		return ip_address;
	}

	public ClientInfo getClientInfoModel() {
		if (client_info == null || client_info.isEmpty()) {
			return null;
		}
		ClientInfo clientInfo = Json.fromJson(client_info, ClientInfo.class);
		if (clientInfo.isExpired()) {
			return null;
		}
		return clientInfo;
	}

	public void setClientInfoModel(String info) {
		this.client_info = Json.toJson(new ClientInfo(info));
	}

	public static class ClientInfo {

		@SerializedName("info")
		public final String info;
		@SerializedName("expired")
		public final String expired;

		public ClientInfo(String info) {
			this.info = info;
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, 7);
			this.expired = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
		}

		public boolean isExpired() {
			try {
				Date expiredDate = new SimpleDateFormat("yyyy-MM-dd").parse(expired);
				return new Date().after(expiredDate);
			} catch (Exception e) {
				return true;
			}
		}

	}

	public boolean getInfo(boolean isLatihan) {
		if(!isLatihan) {
			if(repo_id == null) {
				return false;
			}
			String adpUrl = Play.configuration.getProperty("lpselist.adp");
			adpUrl = String.format(adpUrl + "?repoId=%s", repo_id);
			WS.HttpResponse resp = WS.url(adpUrl).get();
			if(!resp.success()) {
				return false;
			}
			if(resp.getJson() == null || !resp.getJson().isJsonObject()) {
				return false;
			}
			JsonObject jsonResp = resp.getJson().getAsJsonObject();

			lpse_url = jsonResp.get("repo_url").getAsString();
			if(lpse_url.endsWith("/")) {
				lpse_url = lpse_url.substring(0, lpse_url.length()-1);
			}
			if(lpse_url.endsWith("eproc")) {
				lpse_url = lpse_url.concat("4");
			}
			lpse_nama = jsonResp.get("repo_nama").getAsString();
			readIpAddress();
			return refreshVersi();
		}

		if(lpse_url == null) {
			return false;
		}

		String url = lpse_url +  "/publik/tentangkami";
		WS.HttpResponse resp = WS.url(url).get();
		if(resp.success()) {
			org.jsoup.nodes.Document doc= Jsoup.parse(resp.getString());
			lpse_nama = doc.select("title").text().split(":")[0];
			//dapatkan info lpse
			String lpseInfo=doc.select("p:contains(Informasi Sistem)").text();
			Pattern p=Pattern.compile("Informasi Sistem: Versi Aplikasi : SPSE v(.+)u([0-9]+).+ID LPSE: ([0-9]+)");
			Matcher m=p.matcher(lpseInfo);
			readIpAddress();
			if(m.matches()) {
				if(repo_id == null) {
					repo_id = Integer.parseInt(m.group(3));
				}
				spse4_version = m.group(1);
				spse4_build = Long.parseLong(m.group(2));
				return true;
			}
		}
		Logger.info("[Lpse] getInfo - GAGAL tarik info LPSE");

		return false;
	}

	public boolean refreshVersi() {
		if(lpse_url.endsWith("/")) {
			lpse_url = lpse_url.substring(0, lpse_url.length()-1);
		}
		Logger.debug("[Lpse] Processing: %s", this);
		String url = String.format("%s/appwsdce/adprest/versi", lpse_url);
		WS.HttpResponse resp = WS.url(url).timeout("15s").get();
		String versi = resp.getString();
		Pattern pattern = Pattern.compile("(.+)u([0-9]+)");
		Matcher matcher = pattern.matcher(versi);
		if(!matcher.matches()) {
			Logger.info("[Lpse] invalid url, lpse info not found: %s", lpse_url);
			return false;
		}
		spse4_version = matcher.group(1);
		spse4_build = Long.parseLong(matcher.group(2));

		return true;
	}

	public boolean refreshVersiAndSave() {
		if(refreshVersi()) {
			return save() == 1;
		}
		return false;
	}

	public static long countSpseWithLastVersion() {
		Application last = Application.lastVersion();
		return Lpse.count("spse4_version IS NOT NULL AND spse4_version = ? AND repo_id <> 999", last.app_id.substring(5));
	}

	public static long countSpseProd() {
		return Lpse.count("repo_id <> 999 AND spse4_version IS NOT NULL");
	}

	public static long countSpseWithLastBuild() {
		List<Application> applications = Application.find("1=1").fetch();
		long total = 0;
		for(Application app : applications) {
			Changelog changelog = Changelog.find("app_id = ? ORDER BY date DESC", app.app_id).first();
			if(changelog != null) {
				total += Lpse.count("spse4_version = ? AND spse4_build = ? AND repo_id <> 999", app.getVersion(), changelog.getFormattedDateVersion());
			}
		}
		return total;
	}

}
