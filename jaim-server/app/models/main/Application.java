package models.main;

import com.google.gson.Gson;
import jaim.common.model.BaseApplication;
import jaim.common.model.FileAttribute;
import jaim.common.util.Version;
import models.git.Git;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import play.Logger;
import play.Play;
import play.db.jdbc.Table;
import play.libs.Json;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**'Application' yang diinstal di LPSE
 * 
 * @author User
 *
 */
@Table(name="app")
public class Application extends BaseApplication {

	//disimpan dalam bentuk json di database
	public String json;

	public Date audit_update;
	
	public Application(String app_id, String root) {
		super(app_id, root);
	}
	
	private Application(String app_id) {
		super(app_id, null);
		String os=SystemUtils.IS_OS_WINDOWS? "windows":"linux";
		root=Play.configuration.getProperty("repository.root." + os);
	}

	/**Membaca metadata dari disk
	 * @throws IOException 
	 */
	public static void refreshRepositoryAndSaveMetadataIntoDB() throws IOException {
		String os=SystemUtils.IS_OS_WINDOWS? "windows":"linux";
		String root=Play.configuration.getProperty("repository.root." + os);

		Logger.info("RepositoryRoot: %s", root);
		File file = new File(root);
		String rootApps[]= new String[0];
		if(file.list() != null){
			rootApps = file.list();
		}
		assert rootApps != null;
		for(String rootApp: rootApps)
		{
			String appName = FilenameUtils.getName(rootApp);
			Application app = Application.find("app_id = ?", appName).first();
			if(app == null){
				app = new Application(appName);
			}
			refreshRepositoryAndSaveMetadataIntoDB(app);
		}
	}

	/**Membaca metadata dari disk
	 * @throws IOException
	 */
	public static void refreshRepositoryAndSaveMetadataIntoDB(Application app) throws IOException {
		if(app.audit_update != null){
			Git git=new Git(app.root + "/" + app.name);
			String result=git.pull();
			Logger.info("[git pull] %s: %s", app.app_id, result);
		}
		app.prepareFilesAttribute();
		app.save();
		//save
		Logger.info("Application %s:, files: %,d", app.app_id, app.fileAttributes.size());
		//save to temporary for debug purpose
		String fileName=String.format("%s/tmp/JaimServer.%s.json", Play.applicationPath, app.app_id);
		FileOutputStream out=new FileOutputStream(fileName);
		IOUtils.write(app.json, out, "UTF-8");
		out.close();
	}

	public int getLastUpdateByDay() {
		if(audit_update == null) return -1;
		return Days.daysBetween(new DateTime(audit_update), new DateTime(new Date())).getDays();
	}

	public String getFormatedAuditUpdate() {
		if(audit_update == null) return null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return sdf.format(audit_update);
	}
	
	public void prePersist() {
		json= Json.toJson(fileAttributes);
		audit_update = new Date();
	}
	
	public void postLoad()
	{
		super.prepareFilesAttribute(json);
	}

	/**
	 *
	 * @param appAgent
	 * @return
	 *  - true if needs update
	 *  - false if don't
	 */
	public boolean isClientNeedUpdate(BaseApplication appAgent) {
		Collection<FileAttribute> faAgents = appAgent.fileAttributes.values();
		Collection<FileAttribute> faServers = fileAttributes.values();
		for(FileAttribute faServer: faServers) {
			if(!faAgents.contains(faServer)) {
				return true;
			}
		}

		return false;
	}

	public static void removeOldVersion(String version, List<Application> applications){
		for(int i=0; i < applications.size(); i++){
			Version spseVersion = new Version(version);
			Application app = applications.get(i);
			try {
				Version appVersion = new Version(getVersion(app.name));
				if(appVersion.compareTo(spseVersion) < 0){
					applications.remove(i);
				}
			} catch (IndexOutOfBoundsException e){
				e.printStackTrace();
			}
		}
	}

	public static String getVersion(String name) throws IndexOutOfBoundsException {

		return name.split("-")[1];
	}

	public String getVersion() throws IndexOutOfBoundsException {

		return app_id.split("-")[1];
	}

	public static Application lastVersion() {
		List<Application> applications = Application.findAll();
		List<Version> versions = new ArrayList<>();
		for(Application app : applications) {
			String app_version = app.getVersion();
			if(!app.app_id.endsWith("dev"))
				versions.add(new Version(app_version));
		}
		Version last = Collections.max(versions);
		return Application.find("app_id = ?", "spse-" + last.get()).first();
	}
}
