package models.main;

import jaim.common.model.ChangelogItem;
import models.changelog.ChangelogDataForm;
import models.changelog.ChangelogSearchQuery;
import models.changelog.ChangelogSearchResult;
import org.jsoup.Jsoup;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author HanusaCloud on 6/4/2018
 */
@Table(name = "changelog")
public class Changelog extends BaseTable {

    @Id(sequence = "seq_changelog")
    public Long id;
    public Date date;
    public String description;
    public String app_id;
    public Integer last_version;
    public String commit_id;

    public Date auditupdate;

    public Changelog() {}

    @Override
    protected void prePersist() {
        super.prePersist();
        this.auditupdate = new Timestamp(System.currentTimeMillis());
    }

    public Changelog(ChangelogDataForm model) {
        this.id = model.id;
        this.date = model.getDate();
        this.description = model.description;
        this.app_id = model.appId;
    }

    public String getFormattedDate() {
        if (date == null) {
            return null;
        }
        try {
            return new SimpleDateFormat("dd-MM-yyyy").format(date);
        } catch (Exception e) {
            return null;
        }
    }

    public String getFormattedDateVersion() {
        if (date == null) {
            return null;
        }
        try {
            return new SimpleDateFormat("yyyyMMdd").format(date);
        } catch (Exception e) {
            return null;
        }
    }

    public String getFormattedModifiedDate() {
        if (auditupdate == null) {
            return null;
        }
        try {
            return new SimpleDateFormat("dd-MM-yyyy").format(auditupdate);
        } catch (Exception e) {
            return null;
        }
    }

    public String getShortDescription() {
        final String results = Jsoup.parse(description).text();
        if (results.length() < 150) {
            return results;
        }
        return results.substring(0, 150) + "...";
    }

    public static Map<String, List<String>> getByDate(String date, String appId) {
        Map<String, List<String>> results = new HashMap<>();
        List<Changelog> changelogs = find("date::date >= ?::date AND app_id = ? ORDER BY date DESC", date, appId).fetch();
        if (!changelogs.isEmpty()) {
            for (Changelog model : changelogs) {
                final String key = model.getFormattedDateVersion();
                if (!results.containsKey(key)) {
                    results.put(key, new ArrayList<>());
                }
                results.get(key).add(model.description);
            }
        }
        return results;
    }

    public static ChangelogItem[] getChangelogItems(String date, String appId) {
        Map<String, List<String>> results = getByDate(date, appId);
        return results.entrySet().stream()
                .map(e -> {
                    String[] strings = new String[e.getValue().size()];
                    return new ChangelogItem(e.getKey(), e.getValue().toArray(strings));
                })
                .toArray(ChangelogItem[]::new);
    }

    public static ChangelogSearchResult search(ChangelogSearchQuery model) {
        ChangelogSearchResult result = new ChangelogSearchResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM changelog");
        if (model.getKeyword() != null && !model.getKeyword().equals("")) {
            sb.append(" WHERE description LIKE ").append("'%").append(model.getKeyword()).append("%'");
        }
        sb.append(" ORDER BY date DESC");
        result.executeQuery(sb.toString());
        return result;
    }

    public static Changelog getLastChangelog(String app_id){
        return find("app_id=? order by date DESC", app_id).first();
    }

    public static Changelog getByDateAndLastVersion(Date date, int last_version, String app_id){
        return find("date = ? and last_version = ? and app_id = ?", date, last_version, app_id).first();
    }

    public static Changelog getByCommitId(String commit_id) {
        return find("commit_id = ?", commit_id).first();
    }
}
