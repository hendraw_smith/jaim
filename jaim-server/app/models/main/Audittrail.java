package models.main;

import jobs.UserInfoJob;
import models.common.UserInfo;
import models.search.AudittrailSearchQuery;
import models.search.AudittrailSearchResult;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Scope;

import javax.persistence.EnumType;
import java.util.Date;

/**
 * Created by dadang on 8/10/18.
 */
@play.db.jdbc.Table(name="audittrail")
public class Audittrail extends BaseTable {

	@Id
	public Long id;
	public AuditType audit_type;
	public String remote_address;
	public Long user_id;
	public Date activity_date;
	public String data_before; // JSON
	public String data_after; // JSON
	public String info;

	@Enumerated(EnumType.STRING)
	public enum AuditType {
		CLIENT_REQUEST,
		CREATE_LPSE,
		UPDATE_LPSE,
		DELETE_LPSE,
		CREATE_PATCH,
		UPDATE_PATCH,
		CHECK_PATCH,
		DOWNLOAD_PATCH,
		DOWNLOAD_SCRIPT,
		DOWNLOAD_PATCH_LOG,
		CREATE_CHANGELOG,
		UPDATE_CHANGELOG,
		DELETE_CHANGELOG,
		GIT_PULL,
		REFRESH_APPLICATION,
		CHANGELOG_DETAIL,
		LOGIN,
		LOGOUT,

		ERROR_GENERAL,
		ERROR_LPSE,
		ERROR_PATCH,
		ERROR_GIT,
		ERROR_INSTALLER,
	}

	private static Long nextSequence() {
		return Query.find("SELECT NEXTVAL('audittrail_id_seq')", Long.class).first();
	}
	public void prePersist() {
		if(id == null || id == 0) {
			id = nextSequence();
		}
		this.activity_date = new Date();
	}

	public static AudittrailSearchResult search(AudittrailSearchQuery model, Object... params) {
		AudittrailSearchResult result = new AudittrailSearchResult(model);

		StringBuilder sb = new StringBuilder("SELECT * FROM audittrail ");
		if(!StringUtils.isEmpty(model.getKeyword())) {
			String key = model.getKeyword().toUpperCase();
			String keyLike = "%" + key + "%";
			sb.append("WHERE UPPER(audit_type) like ? ");
			params = ArrayUtils.add(params, keyLike);
			sb.append("WHERE UPPER(info) like ? ");
			params = ArrayUtils.add(params, keyLike);
			sb.append("OR remote_address = ? ");
			params = ArrayUtils.add(params, key);
			if(StringUtils.isNumeric(key)) {
				sb.append("OR user_id = ? ");
				params = ArrayUtils.add(params,  Integer.valueOf(key));
			}
		}

		sb.append("ORDER BY activity_date DESC");
		result.executeQuery(sb.toString(), params);
		return result;
	}

	public String getSimpleInfo() {
		if(info != null && info.length() > 150) {
			return info.substring(0, 150) + "...";
		}
		return info;
	}

	public static void save(AuditType auditType, String dataBefore, Object dataAfter, String info){
		if(Play.configuration.getProperty("audittrail.enabled").equals("false")) {
			return;
		}
		Http.Request request = Http.Request.current();
		UserInfo userInfo = UserInfoJob.userInfoMap.get(Scope.Session.current().getId());
		Audittrail audittrail = new Audittrail();
		audittrail.audit_type = auditType;
		audittrail.remote_address = request.remoteAddress;
		audittrail.data_before = dataBefore;
		audittrail.data_after = dataAfter != null ? Json.toJson(dataAfter) : null;
		audittrail.user_id = userInfo != null && userInfo.user_id != null ? userInfo.user_id : null;
		audittrail.info = Json.toJson(request);
		if(info != null) {
			audittrail.info = info;
		} else if(userInfo != null) {
			audittrail.info = userInfo.getJson();
		}
		audittrail.save();
		Logger.info("[AudittrailCtr] saveToAudittrail - new Audittrail saved..");
	}
}
