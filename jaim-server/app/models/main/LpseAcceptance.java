package models.main;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;
import java.util.Optional;


@Table(name = "penerimaan")
public class LpseAcceptance extends BaseTable{
    public static String ACCEPT="TERIMA";
    public static String REJECT="TOLAK";

    @Id(sequence = "penerimaan_id_seq")
    public Long id;
    public String app_id;
    public Date build_date;
    public Integer lpse_id;
    public Long sp_id;
    public Date notifydate;
    public String acceptance_status;
    public Date acceptance_date;

    public static Optional<LpseAcceptance> getByServicePack(ServicePack sp) {
        LpseAcceptance l = find("sp_id = ?", sp.sp_id)
                .order("acceptance_date desc")
                .first();
        return Optional.ofNullable(l);
    }


    public Long getId() {
        return id;
    }

    public String getApp_id() {
        return app_id;
    }

    public Date getBuildDate() {
        return build_date;
    }

    public Integer getLpse_id() {
        return lpse_id;
    }

    public Date getNotifyDate() {
        return notifydate;
    }

    public String getAcceptanceStatus() {
        return acceptance_status;
    }

    public Date getAcceptanceDate() {
        return acceptance_date;
    }


    public LpseAcceptance(ServicePack sp){
        this.app_id = sp.app_id;
        this.lpse_id = sp.lpse_id;
        this.sp_id = sp.sp_id;
        this.notifydate = new Date();
        this.acceptance_status = null;
        this.acceptance_date = null;
    }

    public LpseAcceptance(Lpse lpse, String app_id, Date buildDate){
        this.app_id = app_id;
        this.lpse_id = lpse.lpse_id;
        this.build_date = buildDate;
        this.notifydate = new Date();
        this.acceptance_status = null;
        this.acceptance_date = null;
    }

    public void accept(){
        setAcceptanceStatus(ACCEPT);
    }

    public void reject(){
        setAcceptanceStatus(REJECT);
    }

    private void setAcceptanceStatus(String val){
        if(acceptance_status!=null)
            return;
        this.acceptance_status = val;
        this.acceptance_date = new Date();
        save();
    }

    public boolean isAccepted() {
        return ACCEPT.equals(this.acceptance_status);
    }

    public boolean isRejected() {
        return REJECT.equals(this.acceptance_status);
    }

}
