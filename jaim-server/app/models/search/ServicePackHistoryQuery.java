package models.search;

import ext.Param;
import ext.contracts.PaginationContract;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dadang on 7/19/18.
 */
public class ServicePackHistoryQuery implements PaginationContract.ParamAble {
	private final String keyword;
	private final int page;
	private final Param[] params;

	public ServicePackHistoryQuery(Scope.Params params) {
		this.keyword = params.get("search") != null ? params.get("search") : "";
		this.page = getCurrentPage(params);
		this.params = generateParams();
	}

	private Param[] generateParams() {
		List<Param> results = new ArrayList<>();
		if (keyword != null && !keyword.isEmpty()) {
			results.add(new Param("search", keyword));
		}
		Param[] params = new Param[results.size()];
		return results.toArray(params);
	}

	@Override
	public Param[] getParams() {
		return params;
	}

	@Override
	public int getPage() {
		return page;
	}

	public String getKeyword() {
		return keyword;
	}
}
