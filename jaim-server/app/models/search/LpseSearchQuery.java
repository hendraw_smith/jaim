package models.search;

import ext.Param;
import ext.contracts.PaginationContract.ParamAble;
import play.Logger;
import play.mvc.Scope.Params;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dadang on 8/6/18.
 */
public class LpseSearchQuery implements ParamAble {

	private final String keyword;
	private final int page;
	private final Param[] params;

	public LpseSearchQuery(Params params) {
		this.keyword = params.get("search") != null ? params.get("search") : "";
		this.page = getCurrentPage(params);
		this.params = generateParams();
	}

	private Param[] generateParams() {
		List<Param> results = new ArrayList<>();
		if (keyword != null && !keyword.isEmpty()) {
			results.add(new Param("search", keyword));
		}
		Param[] params = new Param[results.size()];
		return results.toArray(params);
	}

	@Override
	public Param[] getParams() {
		return params;
	}

	@Override
	public int getPage() {
		return page;
	}

	public String getKeyword() {
		return keyword;
	}

	public enum QueryMaps {

		STATUS("status", "sp.status", "Search LPSE by status terakhir PATCH"),
		SP_ID("patch id", "sp.sp_id", "Search LPSE by patch ID"),
		LPSE_ID("repo id", "l.repo_id", "Serch LPSE by Repo ID"),
		NAMA_LPSE("nama lpse", "l.lpse_nama", "Search LPSE by nama LPSE");

		String var;
		String key;
		String keterangan;
		QueryMaps(String var, String key, String keterangan){
			this.var = var;
			this.key = key;
			this.keterangan = keterangan;
		}
	}

	private String getVar(String key) {
		Map<String, String> variables = new HashMap<>();
		variables.put(QueryMaps.STATUS.var, QueryMaps.STATUS.key);
		variables.put(QueryMaps.SP_ID.var, QueryMaps.SP_ID.key);
		variables.put(QueryMaps.LPSE_ID.var, QueryMaps.LPSE_ID.key);
		variables.put(QueryMaps.NAMA_LPSE.var, QueryMaps.NAMA_LPSE.key);
		return variables.get(key);
	}

	public Map<String, String> getQueryMap() {
		Map<String, String> result = new HashMap<>();
		if(keyword.contains(":") && !keyword.trim().endsWith(":")){
			try {
				String[] keyValue = keyword.split(":");
				String key = getVar(keyValue[0].trim());
				if(key != null){
					result.put(key, keyValue[1].trim().toLowerCase());
					return result;
				}
			} catch (Exception e){
				Logger.error(e.getMessage());
			}
		}
		return null;
	}
}
