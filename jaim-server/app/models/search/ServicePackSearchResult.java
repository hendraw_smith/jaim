package models.search;

import ext.Param;
import ext.contracts.PaginationContract.ParamAble;
import ext.contracts.PaginationContract.PageAble;
import models.main.ServicePack;

import java.util.List;

/**
 * Created by dadang on 7/19/18.
 */
public class ServicePackSearchResult implements PageAble<ServicePack> {

	private List<ServicePack> items;
	private transient int total = 0;
	private transient final ParamAble paramAble;
	private Meta meta;

	public ServicePackSearchResult(ParamAble paramAble) {
		this.paramAble = paramAble;
	}

	@Override
	public int getCurPage() {
		return paramAble.getPage();
	}

	@Override
	public long getTotal() {
		return total;
	}

	@Override
	public int getTotalCurrentPage() {
		return items != null ? items.size() : 0;
	}

	@Override
	public Param[] getParams() {
		return paramAble.getParams();
	}

	@Override
	public void setTotal(int total) {
		this.total = total;
	}

	@Override
	public void setItems(List<ServicePack> collection) {
		this.items = collection;
	}

	@Override
	public List<ServicePack> getItems() {
		return items;
	}

	@Override
	public Class<ServicePack> getReturnType() {
		return ServicePack.class;
	}

	@Override
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@Override
	public Meta getMeta() {
		return this.meta;
	}
}
