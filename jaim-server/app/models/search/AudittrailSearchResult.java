package models.search;

import ext.Param;
import ext.contracts.PaginationContract.*;
import models.main.Audittrail;

import java.util.List;

/**
 * Created by dadang on 8/27/18.
 */
public class AudittrailSearchResult implements PageAble<Audittrail> {

	private List<Audittrail> items;
	private transient int total = 0;
	private transient final ParamAble paramAble;
	private Meta meta;

	public AudittrailSearchResult(ParamAble paramAble) {
		this.paramAble = paramAble;
	}

	@Override
	public int getCurPage() {
		return paramAble.getPage();
	}

	@Override
	public long getTotal() {
		return total;
	}

	@Override
	public int getTotalCurrentPage() {
		return items != null ? items.size() : 0;
	}

	@Override
	public Param[] getParams() {
		return paramAble.getParams();
	}

	@Override
	public void setTotal(int total) {
		this.total = total;
	}

	@Override
	public void setItems(List<Audittrail> collection) {
		this.items = collection;
	}

	@Override
	public List<Audittrail> getItems() {
		return items;
	}

	@Override
	public Class<Audittrail> getReturnType() {
		return Audittrail.class;
	}

	@Override
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@Override
	public Meta getMeta() {
		return this.meta;
	}
}
