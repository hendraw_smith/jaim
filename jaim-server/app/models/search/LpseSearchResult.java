package models.search;

import ext.Param;
import ext.contracts.PaginationContract;
import ext.contracts.PaginationContract.PageAble;
import models.main.Lpse;

import java.util.List;

/**
 * Created by dadang on 8/6/18.
 */
public class LpseSearchResult implements PageAble<Lpse> {

	private List<Lpse> items;
	private transient int total = 0;
	private transient final PaginationContract.ParamAble paramAble;
	private Meta meta;

	public LpseSearchResult(PaginationContract.ParamAble paramAble) {
		this.paramAble = paramAble;
	}

	@Override
	public int getCurPage() {
		return paramAble.getPage();
	}

	@Override
	public long getTotal() {
		return total;
	}

	@Override
	public int getTotalCurrentPage() {
		return items != null ? items.size() : 0;
	}

	@Override
	public Param[] getParams() {
		return paramAble.getParams();
	}

	@Override
	public void setTotal(int total) {
		this.total = total;
	}

	@Override
	public void setItems(List<Lpse> collection) {
		this.items = collection;
	}

	@Override
	public List<Lpse> getItems() {
		return items;
	}

	@Override
	public Class<Lpse> getReturnType() {
		return Lpse.class;
	}

	@Override
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@Override
	public Meta getMeta() {
		return this.meta;
	}

}
