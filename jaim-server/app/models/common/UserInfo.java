package models.common;

import com.google.gson.Gson;
import jobs.UserInfoJob;
import org.apache.commons.collections4.MapUtils;
import play.libs.Json;
import play.mvc.Scope;

/**
 * Class untuk menyimpan data user yang login dengan CENTRUM
 * Created by dadang on 8/8/18.
 */
public class UserInfo {

	public static final String USER_SESSION = "JAIM_USER_SESSION";

	public Long user_id;
	public String[] role_codes;
	public String jenis_kelamin;
	public String user_name;
	public String nama;
	public String alamat;
	public String nip;
	public String npwp;
	public String tanggal_lahir;
	public String email;
	public String phone;
	public Boolean is_pns;
	public String tanggal_daftar;
	public String status_user;
	public String centrum_code;

	public String sessionid;
	public long logintime;
	public long lastRequest;

	// flag user sedang logged in atau tidak
	public static boolean isLoggedIn() {
		if(MapUtils.isEmpty(UserInfoJob.userInfoMap))
			return false;
		return UserInfoJob.userInfoMap.containsKey(Scope.Session.current().getId());
	}

	public void save(){
		UserInfoJob.userInfoMap.put(sessionid, this);
		Scope.Session.current().put(USER_SESSION, user_id);
	}

	public static UserInfo current() {
		return UserInfoJob.userInfoMap.get(Scope.Session.current().getId());
	}

	// clear Current User
	public static void clear() {
		UserInfo userInfo = current();
		// remove current active user by sessionId
		Scope.Session.current().remove(USER_SESSION);
		clear(userInfo);
	}

	// clear Current User
	public static void clear(UserInfo userInfo) {
		if(userInfo != null) {
			if (UserInfoJob.userInfoMap.isEmpty())
				return;
			UserInfoJob.userInfoMap.remove(userInfo.sessionid);
		}
	}

	public String getJson() {
		return Json.toJson(this);
	}

}
