package models.agent;

import com.google.gson.GsonBuilder;
import jaim.common.model.BaseApplication;
import jaim.common.model.FileAttribute;
import jaim.common.util.Version;
import models.main.Lpse;
import models.main.ServicePack;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.Play;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;


public class Packager {

	private static final char NEW_LINE = (char)10;//NEW LINE harus pakai sistem UNIX
	private List<String> ignoredFiles;
	public Set<FileAttribute> faNewAndModifiedFiles=new HashSet<>();
	private Set<String> faDeletedFiles=new HashSet<>();
	public StringBuffer script=new StringBuffer();
	private BaseApplication appServer;
	private BaseApplication appAgent;
	private boolean multithread = true;
	private int repo_id;
	public File zipPackageFile;//berisi file yg harus dikirim ke LPSE
	private String basePath;
	private Lpse lpse;
	private boolean checkMigration;
	private ServicePack sp;
	
	public Packager(int repo_id, BaseApplication appAgent, BaseApplication appServer, ServicePack sp, Lpse lpse)
	{
		this.repo_id = repo_id;
		this.appAgent = appAgent;
		this.appServer = appServer;
		this.sp = sp;
		this.lpse = lpse;
		Version minVersion = new Version("4.3");
		Version spseVersion = new Version(lpse.spse4_version);
		this.checkMigration = minVersion.compareTo(spseVersion) <= 0;
	}
	
	/**Create ZIP file contains 
	 * 1. files to copy 
	 * 2. '.delete' file contains list of files to be deleted
	 * 
	 * jaimServer/conf/ignore.jaim  which is list of paths/files pattern to be ignored.
	 * 1. MUST NOT start with /
	 * 2. USE Unix Path STYLE 
	 * Example: 
		conf/application.conf
		conf/log4j.properties
		public/images/mm*

	 * @param delay
	 * @param zipPackageFile
	 * @param scriptFile
	 * @param jaimLogDir
	 * @param spseRootDir
	 * @param backupDbDir
	 * @return true: perlu update
	 * 		false: tidak perlu update
	 * @throws IOException 
	 * @throws ZipException 
	 */
	public boolean buildPackage(long delay, String zipPackageFile, String scriptFile, String jaimLogDir, String basePath, String spseRootDir, String backupDbDir) throws IOException {
		if(delay<=60)
		{
			Logger.warn("[Packager] sp_id: %s, delay is set to minimum 60s	", sp.sp_id);
			delay=60; 
		}
		this.basePath = basePath;
		Collection<FileAttribute> faAgents= appAgent.fileAttributes.values();
		Collection<FileAttribute> faServers= appServer.fileAttributes.values();
		//Step 1: Dapatkan file yg harus dikirim ke LPSE
		ignoredFiles=readIgnoreFiles();
		faNewAndModifiedFiles=new HashSet<>();
		faDeletedFiles=new HashSet<>();
		boolean needsUpdate;
		for(FileAttribute faServer: faServers) {
			if(!faAgents.contains(faServer)) {
				faNewAndModifiedFiles.add(faServer);
			}
		}
		//Step 2: dapatkan file yg harus di-delete dari LPSE
		//karena ada di LPSE tapi tidak ada di Jaim (kecuali yg masuk ignored)
		for(FileAttribute faAgent: faAgents) {
			/* TIDAK bisa menggunakan contains() karena contains() mengecek MD5 juga
			 * Di sini kita hanya perlu mengecek nama
			 */
			boolean existsInServer=false;
			for(FileAttribute faServer: faServers)
				if(faServer.path.equals(faAgent.path))
				{
					existsInServer=true;
					break;
				}
			if(!existsInServer && !isIgnored(faAgent.path))
				faDeletedFiles.add(faAgent.path);
		}
		needsUpdate=!faDeletedFiles.isEmpty() || !faNewAndModifiedFiles.isEmpty();
		saveToFileAsJson("del",faDeletedFiles);
		saveToFileAsJson("new",faNewAndModifiedFiles);
		
		Logger.debug("[Packager]Files sp: %s, JaimAgent: %,d, JaimServer: %,d new: %,d, deleted: %,d",
				sp.sp_id,
				faAgents.size(), faServers.size(),
				faNewAndModifiedFiles.size(), faDeletedFiles.size());

		if(needsUpdate || sp.need_migration > 0) {
			if(!faNewAndModifiedFiles.isEmpty()) {
				StopWatch sw=new StopWatch();
				sw.start();
				//create zip file from selected files
				BufferedOutputStream out=new BufferedOutputStream(new FileOutputStream(zipPackageFile));
				ZipOutputStream zipOut=new ZipOutputStream(out);
				for(FileAttribute fa: faNewAndModifiedFiles) {
					String fileToAdd = String.format("%s/%s", appServer.getPath(), fa.path);
					if (new File(fileToAdd).exists()) {
						InputStream is = new BufferedInputStream(new FileInputStream(fileToAdd));
						ZipEntry ze = new ZipEntry(fa.path);
						zipOut.putNextEntry(ze);
						IOUtils.copy(is, zipOut);
						zipOut.closeEntry();
						is.close();
					}
				}
				zipOut.close();
				sw.stop();
				play.Logger.debug("[Jaim Packager]: sp: %s, %s, duration: %s", sp.sp_id, repo_id, sw);
			}
			generateShellScript(delay, scriptFile, jaimLogDir, spseRootDir, backupDbDir);
		}
		return needsUpdate;
	}
	
	
	/**Method ini generate shell script yang akan di-eksekusi di LPSE
	 * JaimAgent.sh akan digenerate jika ada file yg dikirim ke LPSE
	 * 1. Stop SPSE 4
	 * 2. Backup DB jika ada migration
	 * 3. Delete file yg perlu di-delete
	 * 4. Extract file zip ke /home/appserv/spse-prod-x.x.x
	 * 5. start SPSE 4
	 * 
	 * Asumsi: file.zip disimpan di /home/appserv/spse-prod-x.x.x/tmp/pkg.jaim.zip
	 * 
	 * @throws IOException
	 */
	private void generateShellScript(long delay, String scriptFile, String jaimLogDir, String spseRootDir, String backupDbDir) throws IOException
	{
		Logger.debug("[Packager] generating Script sp: %s", sp.sp_id);
		String jaimServerTime=DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm 'WIB JaimServer Time'");
		
		add("#!_SHELL");// _SHELL diganti di SPSE dengan /bin/bash atau /bin/sh
		if (Play.mode.isProd()) {
			add("sleep " + delay);
		}
		sendStepFunction();
		add("sendStep 'started'");
		add("#Generate by JaimServer: %s", jaimServerTime);
		add("#Maintainer: andik@lkpp.go.id ");
		add("#Jaim ServicePack ID: %s ", sp.sp_id);
		add("#This script is run by JaimAgent in SPSE-4.");
		add();
		addNoFormatting("now=`date +%Y%m%d.%H%M`"); //karena mengandung %
		if(repo_id == 999){
			add("APP_HOME=%s/spselat", spseRootDir);
		} else {
			add("APP_HOME=%s/spse", spseRootDir);
		}

		add("JAIM_TEMP=%s", jaimLogDir);
		add("REPO_ID=%s", repo_id);
		add("SP_ID=%s", sp.sp_id);
		add("JAIM_BACKUP=$JAIM_TEMP/JaimBackup.%08d.zip", sp.sp_id);
		add("JAIM_LOG=$JAIM_TEMP/JaimAgent.%08d.log", sp.sp_id);
		add("echo \"########## JaimAgent Log. sp_id=%s ############\" >  $JAIM_LOG", sp.sp_id);
		add("echo \"\" >> $JAIM_LOG");
		add("echo \"JaimAgent Script Generate-Date: %s\"     >> $JAIM_LOG", jaimServerTime);
		add("echo \"JaimAgent Script  Running-Date: $now\"   >> $JAIM_LOG");
		add("echo \"Deleted files are copied in: $JAIM_BACKUP \" >> $JAIM_LOG");
		// Backup dir
		add("DIR=%s", backupDbDir);
		add("DIR_LOG=%s/backup_db_logs", jaimLogDir);
		addNoFormatting("DATE_FILE=`date '+%Y_%m_%d'`");
		add("DUMP_LOG_FILE=$DIR_LOG/Backup.%08d.log", sp.sp_id);
		add();
		add("mkdir -p $DIR >> $JAIM_LOG");
		add();
		add("mkdir -p $DIR_LOG >> $JAIM_LOG");
		addNoFormatting("PSQL_CM=$(which psql)");
		addNoFormatting("PG_DUMP_CM=$(which pg_dump)");
		add();
		requestFunction();
		sendJaimLogFunction();
		sendDumpActivityFunction();
		packageRequirementFunction();
		checkDbConfig();
		initDbRequirements();
		//#1
		logging();
		add("logging 'Stopping instance'");
		add("$APP_HOME/spse4 stop >> $JAIM_LOG 2>&1");
		add();
		// CHECK butuh dump atau tidak
		if(!checkMigration || (checkMigration && sp.need_migration > 1 && sp.do_backup)) {
			initDumpScript();
		}
		add();

		//#2 backup files before deleted
		add("sendStep 'backingupfile'");
		add("logging 'backup files'");
		add("{");
		add("cd $APP_HOME");
		for(String del: faDeletedFiles) {
			del= escapeDel(del);//escape
			add("zip $JAIM_BACKUP $APP_HOME/%s", del);
		}
		addNoFormatting("} >> $JAIM_LOG 2>&1 || { sendStep 'backupFileFailed'; sendJaimLog 'FAILED'; }");
		//#3 generate script untuk force delete
		add("logging 'removing files'");
		for(String del: faDeletedFiles){
			del= escapeDel(del);//escape
			add("rm -f -v $APP_HOME/%s  >> $JAIM_LOG", del);
		}
		// Unzip hanya jika ada file baru/dimodifikasi
		if(!faNewAndModifiedFiles.isEmpty()) {
			add("logging 'extract files from zip'");
			add("sendStep 'updating'");
			add("{");
			if(repo_id == 999){
				add("\tunzip -o $JAIM_TEMP/pkg.spselat.zip -d $APP_HOME ");
			} else {
				add("\tunzip -o $JAIM_TEMP/pkg.spse.zip -d $APP_HOME ");
			}
			addNoFormatting("} >> $JAIM_LOG 2>&1 || { sendStep 'updatingFailed'; sendJaimLog 'FAILED'; }");
		}
		add("$APP_HOME/spse4 start >> $JAIM_LOG");
		add("sendStep 'finished'");
		add("sendJaimLog 'SUCCESS'");

		//simpan script ini sebagai file 
		FileOutputStream out=new FileOutputStream(scriptFile);
		IOUtils.write(script.toString(), out, "UTF-8");
		out.close();
		
	}

    private static String escapeDel(String del) {
        return del.replace("\\(", "\\\\(").replace("\\)", "\\\\)")
                .replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)")
                .replace("$", "\\$").replaceAll("\\s+", "\\\\ ");
    }

    private void add(String pattern, Object...obj)
	{
		script.append(String.format(pattern, obj)).append(NEW_LINE);
	}

	private void addNoFormatting(String value) {
		script.append(value).append(NEW_LINE);
	}

	private void addNoNewLine(String value) {
		script.append(value);
	}
	
	private void add()
	{
		script.append(NEW_LINE);
	}

	/**
	 * All server must have curl and able to send request to jaim server,
	 * if status equals FAILED terminate immediately.*/
	private void requestFunction() {
		add("sendRequest() {");
		add("\tcurl --insecure -XPOST \\");
		add("\t-H 'Content-Type: multipart/form-data' \\");
		add("\t-H 'token: _TOKEN_' \\");
		add("\t-H 'Accept: application/json' \\");
		add("\t-F 'meta={\"date\":\"'${1}'\",\"repoId\":\"'${2}'\",\"type\":\"'${3}'\",\"status\":\"'${5}'\",\"sp_id\":\"'${6}'\"};type=application/json' \\");
		add("\t-F \"file=@\"$4 \\");
		add("\t_LOGGER_URL_ ");
		add("\tif [ \"FAILED\" = $5 ];");
		add("\tthen");
		add("\t\techo 'check error log ' $5");
		add("\t\texit 1");
		add("\tfi");
		add("}");
		add();
	}

	private void sendDumpActivityFunction() {
		add("sendDumpLog() {");
		addNoFormatting("\tsendRequest `date '+%Y-%m-%d-%H%M%S'` $REPO_ID \"BACKUP\" $DUMP_LOG_FILE $1 $SP_ID");
		add("}");
		add();
	}

	private void sendJaimLogFunction() {
		add("sendJaimLog() {");
		addNoFormatting("\tsendRequest `date '+%Y-%m-%d-%H%M%S'` $REPO_ID \"UPDATE\" $JAIM_LOG $1 $SP_ID");
		add("}");
		add();
	}

	private void sendStepFunction() {
		add("sendStep() {");
		add("\tcurl --insecure -XPOST \\");
		add("\t-H 'token: _TOKEN_' \\");
		add("\t_LOGGER_ACTIVITY_URL_$1 ");
		add("}");
		add();
	}

	private void logging() {
		add("logging() {");
		addNoFormatting("\techo `date '+%Y-%m-%d-%H%M%S'` ' activity: '$1 >> $JAIM_LOG");
		add("}");
		add();
	}

	private void sendClientInformation(Lpse lpse) {
		Lpse.ClientInfo clientInfo = lpse.getClientInfoModel();
		if (clientInfo != null && clientInfo.isExpired()) {
			addNoFormatting("sendOsInformation() {");
			addNoFormatting("\tOS_INFO=$(cat /etc/*release)");
			addNoFormatting("\tPROC=$(grep -c ^processor /proc/cpuinfo)");
			addNoFormatting("\tMEMORY=$(grep -Ei 'memtotal|memfree' /proc/meminfo)");
			addNoFormatting("\tINFO=\"${OS_INFO} \\nProcessor: ${PROC} \\n${MEMORY}\\n\"");
			addNoFormatting("\tcurl --insecure -XPOST \\");
			addNoFormatting("\t-H 'token: _TOKEN_' \\");
			addNoFormatting("\t-F \"info=${INFO}\" \\");
			addNoFormatting("\thttps://jaim.lkpp.go.id/servicePack/" + lpse.lpse_id + "/record/os");
			addNoFormatting("}");
			addNoFormatting("sendOsInformation");
		}
	}

	private void initDumpScript() {
		add("\tsendStep 'backingup'");
		backupScript();
		if(!checkMigration){
			add();
			add("\tlogging 'doing migration'");
			add("\tsendStep 'migrating'");
			add("\t$APP_HOME/spse4 migration >> $JAIM_LOG 2>&1");
		}
	}

	private void checkDbConfig(){
		addNoFormatting("echo 'Start ' `date '+%Y-%m-%d %H:%M:%S'` >> $DUMP_LOG_FILE");
		add("if [ -z $1 ] || [ -z $2 ] || [ -z $3 ] || [ -z $4 ];");
		add("then");
		add("\techo 'Terminate.. missing params, check parameters: username host port db_name password' >> $DUMP_LOG_FILE");
		add("\tsendDumpLog 'FAILED'");
		add("\texit 1 ");
		add("fi");
		add();
	}

	private void backupScript() {
		add("\tsetFinishLine() {");
		addNoFormatting("\t\techo 'Finish ' `date '+%Y-%m-%d %H:%M:%S'` >> $DUMP_LOG_FILE");
		add("\t}");
		add();
		add("\tTOTAL_BACKUP=$(ls -1q $DIR/ | wc -l)");
		add();
		add("\tif [ $TOTAL_BACKUP -ge 3 ];");
		add("\tthen");
		add("\t\techo 'Total backup file: ' $TOTAL_BACKUP >> $DUMP_LOG_FILE");
		add("\t\tOLDEST_FILE=\"$(ls -t $DIR | tail -1)\"");
		add("\t\ttecho 'Delete the oldest backup file or directory: ' $OLDEST_FILE  >> $DUMP_LOG_FILE");
		add("\t\trm -f -r $DIR/$OLDEST_FILE >> $DUMP_LOG_FILE 2>&1");
		add("\tfi");
		add();
		add("\texport PGPASSWORD=$5");
		add("\tDUMP_ERROR_LOG=$DIR_LOG/dump_error.log");
		addNoFormatting("\tNOW_PREFIX=`date '+%Y_%m_%d_%H%M%S'`");
		add("\techo \"Versi PostgreSQL: $($PSQL_CM -V)\" >> $DUMP_LOG_FILE");
		add("\techo \"Versi Pg Dump: $($PG_DUMP_CM -V)\" >> $DUMP_LOG_FILE");
		grantLargeObject();
		if (multithread) {
			multiThreadDump();
		} else {
			singleThreadDump();
		}

		add("\tif [ -s $DUMP_ERROR_LOG ] || grep -iq \"failed\" $DUMP_ERROR_LOG");
		add("\tthen");
		add("\t\techo 'Oopss.. dump failed.. terminate.. see logs below.' >> $DUMP_LOG_FILE");
		add("\t\techo \"\" >> $DUMP_LOG_FILE");
		add("\t\tcat $DUMP_ERROR_LOG >> $DUMP_LOG_FILE");
		add("\t\techo \"\" >> $DUMP_LOG_FILE");
		add("\t\trm -f -r $FILE_PATH >> $DUMP_LOG_FILE 2>&1");
		add("\t\techo \"Delete empty backup file.\" >> $DUMP_LOG_FILE");
		add("\t\trm -f $DUMP_ERROR_LOG >> $DUMP_LOG_FILE 2>&1");
		add("\t\techo \"Delete temporary error log.\" >> $DUMP_LOG_FILE");
		add("\t\tsetFinishLine");
		add("\t\tsendStep 'backingupFailed'");
		add("\t\tsendDumpLog 'FAILED'");
		add("\t\texit 1");
		add("\tfi");
		add();
		add("\techo 'Dump is succeed carry on.' >> $DUMP_LOG_FILE");
		printResultDump(multithread);
		add("\tsetFinishLine");
		add("\tsendDumpLog 'SUCCESS'");
	}

	private void multiThreadDump() {
		add("\tFILE_PATH=$DIR/$NOW_PREFIX'_'$4");
		add();
		add("\t{");
		add("\t\techo 'Start dumping database' >> $DUMP_LOG_FILE");
		add("\t\t$PG_DUMP_CM -U $1 -h $2 -p $3 -Fd -f $FILE_PATH -j 4 $4 2> $DUMP_ERROR_LOG");
		add("\t} 2> $DUMP_ERROR_LOG");
		add();
	}

	private void singleThreadDump() {
		add("\tFILE_PATH=$DIR/$NOW_PREFIX'_'$4.zip");
		add();
		add("\t{");
		add("\t\techo 'Start dumping database' >> $DUMP_LOG_FILE");
		add("\t\t$PG_DUMP_CM -U $1 -h $2 -p $3 $4 | gzip > $FILE_PATH 2> $DUMP_ERROR_LOG");
		add("\t} 2> $DUMP_ERROR_LOG");
		add();
	}

	private void initDbRequirements() {
		addNoFormatting("REQUIREMENT='true'");
		add("count() {");
		addNoFormatting("\tSELECT_USER=\"AND rol.rolname = '${1}'\"");
		addNoFormatting("\tif [ ${5} = 'all' ]");
		addNoFormatting("\tthen");
		addNoFormatting("\t\tSELECT_USER=\"\"");
		addNoFormatting("\tfi");
		countSql();
		addNoFormatting("\tif [ 'false' = $REQUIREMENT ];");
		addNoFormatting("\tthen");
		addNoFormatting("\t\techo 'gathering db requirements failed see error logs' >> $DUMP_LOG_FILE;");
		addNoFormatting("\t\tsendStep 'dbrequirementfailed';");
		addNoFormatting("\t\tsendDumpLog 'FAILED'");
		addNoFormatting("\t\techo '-1'");
		addNoFormatting("\tfi");
		addNoFormatting("}");

		addNoFormatting("echo 'generate total of all tables, sequences, and indexes from' $4 >> $DUMP_LOG_FILE");
		addNoFormatting("TOTAL_ALL=$(count $1 $2 $3 $4 'all')");
		addNoFormatting("if [ '-1' = $TOTAL_ALL ];");
		addNoFormatting("then");
		addNoFormatting("\texit 1");
		addNoFormatting("fi");
		addNoFormatting("echo 'total all:' $TOTAL_ALL >> $DUMP_LOG_FILE");
		addNoFormatting("TOTAL_OWNED=$(count $1 $2 $3 $4 'user')");
		addNoFormatting("echo \"total owned by '${1}':\" $TOTAL_OWNED >> $DUMP_LOG_FILE");
		addNoFormatting("sendStep 'dbrequirementpassed'");
		add();
	}

	private void countSql() {
		addNoFormatting("\t{");
		addNoNewLine("\t$PSQL_CM -U $1 -h $2 -p $3 $4 -w");
		addNoNewLine(" -c \"SELECT count(*) FROM pg_class cl");
		addNoNewLine(" JOIN pg_roles rol on rol.oid = cl.relowner");
		addNoNewLine(" JOIN pg_namespace nsp on nsp.oid = cl.relnamespace");
		addNoNewLine(" WHERE nsp.nspname IN (");
		addNoNewLine("SELECT table_schema");
		addNoNewLine(" FROM information_schema.tables");
		addNoNewLine(" WHERE table_catalog = '${4}'");
		addNoNewLine(" AND table_schema NOT IN ('information_schema', 'pg_catalog')");
		addNoNewLine(" GROUP BY table_schema)");
		addNoNewLine(" AND nsp.nspname NOT LIKE 'pg_toast%' ${SELECT_USER}");
		addNoFormatting(" AND cl.relkind IN ('r','S', 'i');\" | sed -n '3p' | tr -dc '[0-9]+'");
		addNoNewLine("\t} 2>> $DUMP_LOG_FILE || { ");
		addNoNewLine("REQUIREMENT='false';");
		addNoFormatting("}");
	}

	private void packageRequirementFunction() {
		addNoFormatting("function checkPackage() {");
		addNoFormatting("\tRESULT=$(which $1)");
		addNoFormatting("\tcase \"${1}\" in");
		addNoFormatting("\t\t*$RESULT*)");
		addNoFormatting("\t\t\techo \"package '${1}' does not exist, please install '${1}' it and re-run the script.\" >> $JAIM_LOG");
		addNoFormatting("\t\t\tsendStep 'packagerequirementfailed'");
		addNoFormatting("\t\t\tsendJaimLog 'FAILED'");
		addNoFormatting("\t\t\texit 1");
		addNoFormatting("\t\t;;");
		addNoFormatting("\tesac");
		addNoFormatting("}");
		if(sp.need_migration > 1 && sp.do_backup) {
			addNoFormatting("declare -a APPS=('grep' 'zip' 'unzip' 'curl' 'sed' 'pg_dump' 'psql')");
		} else {
			addNoFormatting("declare -a APPS=('grep' 'zip' 'unzip' 'curl' 'sed')");
		}
		addNoFormatting("for i in \"${APPS[@]}\"");
		addNoFormatting("do");
		addNoFormatting("\t checkPackage $i");
		addNoFormatting("done");
		addNoFormatting("sendStep 'packagerequirementpassed'");
		add();
		sendClientInformation(lpse);
	}

	private void printResultDump(boolean multi) {
		if (multi) {
			addNoFormatting("\techo 'Backup size: ' $(du -h $FILE_PATH) >> $DUMP_LOG_FILE");
			addNoFormatting("\techo 'Restore using pg_restore' >> $DUMP_LOG_FILE");
		} else {
			addNoFormatting("\techo 'Backup file: ' $FILE_PATH ' file size: ' $(stat -c%s $FILE_PATH) >> $DUMP_LOG_FILE");
		}
	}

	/**
	 * Command untuk grant Large object ke EPNS/user yang di setting di config spse
	 */
	private void grantLargeObject(){
		addNoFormatting("\t$PSQL_CM -U postgres -h $2 -p $3 -c \"ALTER DATABASE $4 SET lo_compat_privileges=on;\" >> $DUMP_LOG_FILE");
		addNoNewLine(" ");
		add();
		addNoFormatting("\t$PSQL_CM -U postgres -h $2 -p $3 -c \"GRANT SELECT ON LARGE OBJECT 31716245 TO $1;\" >> $DUMP_LOG_FILE");
		addNoNewLine(" ");
		add();
	}

	private boolean isIgnored(String path) {
		for(String ignore: ignoredFiles)
		{
			if(ignore.endsWith("*"))
			{
				ignore=ignore.replace("*", "");
				if(path.startsWith(ignore))
					return true;
			}
			else
				if(path.equals(ignore))
					return true;
		}
		return false;
	}

	private static List<String> readIgnoreFiles() throws IOException {
		/*Dapatkan files yg harus di-ignore.
		  Daftar file disimpan di conf/ignore.jaim
		 */
		String path=String.format("%s/conf/ignore.jaim", Play.applicationPath);
		File file=new File(path);
		List<String> files;
		if(file.exists())
			files=IOUtils.readLines(new FileInputStream(file), "UTF-8");
		else
			files=new ArrayList<>();
		return files;
	}

	//simpan Set untuh kebutuhan debugging
	private void saveToFileAsJson(String group, Set files) throws IOException {
		String fileName=String.format("%s/%s.%s.json", basePath, group, appServer.app_id);
		FileOutputStream out=new FileOutputStream(fileName);
		String json=new GsonBuilder().setPrettyPrinting().create().toJson(files);
		IOUtils.write(json, out, "UTF-8");
		out.close();
	}

}
