	package models.agent;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jaim.common.model.FileAttribute;
import models.main.Application;
import models.main.Lpse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import play.Logger;
import play.Play;
import play.libs.Json;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class AgentConnect {

	private Lpse lpse;
	private String jaim_token;
	private String app_id;
	private Packager pack;
	
	public AgentConnect(int lpse_id, Long update_id)
	{
		lpse=Lpse.find("lpse_id=?", lpse_id).first();
		//Untuk keperluan developent, URL di-override
		lpse.lpse_url=System.getProperty("lpse_url", lpse.lpse_url);
		app_id="spse-prod-4.1.2";
		
		//clean temp folder
		String tmp=String.format("%s/tmp/%03d", Play.applicationPath, lpse_id);
		try {
			File file=new File(tmp);
			file.mkdirs();
			FileUtils.cleanDirectory(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return true jika ada perubahan dan perlu updatee
	 * 	
	 */
	public boolean doStep1()
	{
		//a. Mintakan fileAtributes dari LPSE
		String url=String.format("%s/jaim/step1/%s", lpse.lpse_url, jaim_token);
		HttpResponse resp= WS.url(url).get();
		if(!resp.success())
		{
			Logger.error("[AgentConnect] ERROR %s", resp.getStatusText());
			return false;
		}
		String str=resp.getString();
		String tmpFile=String.format("%s/tmp/%03d/lpse.%s.json", Play.applicationPath, lpse.repo_id, app_id);
		File file=new File(tmpFile);
		file.getParentFile().mkdirs();
		java.lang.reflect.Type type=new TypeToken<Map<String, FileAttribute>>(){}.getType();
		file.getParentFile().mkdirs();
		try(OutputStream out=new FileOutputStream(file))
		{
			IOUtils.write(str,out, "UTF-8");
			Logger.debug("[AgentConnect] URL %s, response size: %,d, savedToFile %s", url, 
					str.length(), file.getName());
			//b. response dari LPSE berbentuk json-> convert jadi object
			Application appOfAgent = new Application(app_id, "");
			appOfAgent.fileAttributes= Json.fromJson(str, type);
			out.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
		//c. Generate zip
		Application appOfServer=Application.findById(app_id);
//		pack=new Packager(lpse.lpse_id, appOfAgent, appOfServer, jaim_token);
//		//siapkan fileAttributes dari appOfServer
//		appOfServer.fileAttributes=gson.fromJson(appOfServer.json, type);
//		try {
//			int delay=60; //delay (detik) sebelum update dilakukan
//			boolean needsUpdate=pack.buildPackage(delay);
//			Logger.debug("[AgentConnect] lpse: %s, needsUpdate: %s", lpse.lpse_id, needsUpdate);
//			return needsUpdate;
//		} catch (IOException | ZipException e) {
//			e.printStackTrace();
//		}
		return false;
	}
	
	public void doStep2()
	{
		String url=String.format("%s/jaim.JaimCtr/step2", lpse.lpse_url);
		WSRequest req=WS.url(url);
		req.fileParams=new WS.FileParam[]{new WS.FileParam(pack.zipPackageFile, "zipPackage")};
		req.setParameter("script", pack.script.toString());
		req.setParameter("jaim_token", jaim_token);
		req.setParameter("app_id", app_id);
		HttpResponse resp=req.post();
		if(!resp.success())
		{
			Logger.error("[AgentConnect] ERROR %s", resp.getStatusText());
			return;
		}
	}
}
