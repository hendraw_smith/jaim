package ext.contracts;

import com.google.gson.annotations.SerializedName;
import ext.Param;
import play.db.jdbc.Query;
import play.mvc.Scope;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * @author HanusaCloud on 6/5/2018
 */
public interface PaginationContract {

    interface ParamAble {
        Param[] getParams();

        int getPage();

        default Integer getCurrentPage(Scope.Params params) {
            return !isNull(params, "page") ? params.get("page", Integer.class) : 1;
        }

        default boolean isNull(Scope.Params params, String key) {
            return params.get(key) == null;
        }
    }

    /**
     * pageable T means that T as a model and not as a collection,
     * because well... pageable means contain more than one item hence the name pageable*/
    interface PageAble<T> {

        int MAX_SIZE_PER_PAGE = 24;
        int MAX_PAGE_ITEM_DISPLAY = 10;

        int getCurPage();
        long getTotal();
        int getTotalCurrentPage();
        Param[] getParams();
        void setTotal(int total);

        void setItems(List<T> collection);
        List<T> getItems();

        Class<T> getReturnType();

        default String getPrevLink() {
            return this.getLink() + "page=" + (getCurPage() - 1);
        }

        default String getNextLink() {
            return this.getLink() + "page=" + (getCurPage() + 1);
        }

        default boolean isPrevious() {
            return this.getCurPage() != 1;
        }

        default int getLimit() {
            return MAX_PAGE_ITEM_DISPLAY;
        }

        default boolean isNext() {
            return this.getCurPage() * getLimit() < getTotal();
        }

        default int getOffset() {
            return getCurPage() > 1 ? (getCurPage() - 1) * getLimit() : 0;
        }

        default int getShow() {
            return getCurPage() == 1 ? 1 : getOffset() + 1;
        }

        default int getPageCount() {
            return calculatePageCount();
        }

        default String getLink() {
            return getQueryString();
        }

        default String getQueryString() {
            if (getParams() != null) {
                String link = "?" + generateQuery(getParams());
                if (!link.contains("?")) {
                    link += "?";
                } else if (!link.endsWith("&") && !link.endsWith("?")) {
                    link += "&";
                }
                return link;
            }
            return "";
        }

        default int[] getPageNumbers() {
            int[] pageNumbers = new int[getPageCount()];
            for (int i = 0; i < pageNumbers.length; i++) {
                pageNumbers[i] = i + 1;
            }
            return pageNumbers;
        }

        default int getFrom() {
            return getTotalCurrentPage() < getLimit() ? (int) getTotal() : getLimit() * getCurPage();
        }

        default int generateTotal(String query) {
			final int total = Query.count("SELECT COUNT(*) FROM (" + query + ") p", Integer.class);
            setTotal(total);
            return total;
        }

        default int generateTotal(String query, Object[] params) {
            final int total = Query.count("SELECT COUNT(*) FROM (" + query + ") p", Integer.class, params);
            setTotal(total);
            return total;
        }

        default int calculatePageCount() {
            return  (int) Math.ceil(getTotal() * 1.0 / getLimit() * 1.0);
        }

        default boolean isEmpty() {
            return getTotal() == 0;
        }

        default String generateQuery(Param[] params) {
            StringBuilder result = new StringBuilder();

            String glue = "";
            for (Param param : params) {
                if (param.getValue() != null) {
                    String encodedName = param.getKey();
                    String value = String.valueOf(param.getValue());
                    result.append(glue).append(encodedName).append("=").append(encode(value));
                    glue = "&";
                }
            }

            return result.toString();
        }

        default String encode(String content) {
            try {
                return URLEncoder.encode(content, "ISO-8859-1");
            } catch (UnsupportedEncodingException var3) {
                throw new IllegalArgumentException(var3);
            }
        }

        default void executeQuery(String query) {
            generateTotal(query);
            if (!isEmpty()) {
                final String sql = query + " LIMIT " + getLimit() + " OFFSET " + getOffset();
                setItems(Query.find(sql, getReturnType()).fetch());
            }
            generateMeta();
        }

        default void executeQuery(String query, Object[] params) {
            generateTotal(query, params);
            if (!isEmpty()) {
                final String sql = query + " LIMIT " + getLimit() + " OFFSET " + getOffset();
                setItems(Query.find(sql, getReturnType(), params).fetch());
            }
            generateMeta();
        }

        default void generateMeta() {
            setMeta(new Meta(this));
        }

        default void setMeta(Meta meta) {

        }

        default Meta getMeta() {
            return new Meta(this);
        }

        class Meta {

            @SerializedName("page")
            private final int page;
            @SerializedName("total")
            private final int total;
            @SerializedName("show")
            private final int show;
            @SerializedName("from")
            private final int from;
            @SerializedName("keyword")
            private final String keyword;

            public Meta(PageAble pageAble) {
                this.total = (int) pageAble.getTotal();
                this.show = pageAble.getShow();
                this.from = pageAble.getFrom();
                this.page = pageAble.getCurPage();
                String keyword = null;
                for (Param param : pageAble.getParams()) {
                    if (param.getKey().contains("keyword")
                            || param.getKey().contains("search")) {
                        keyword = String.valueOf(param.getValue());
                    }
                }
                this.keyword = keyword;
            }

        }

    }

}
