package ext;

import groovy.lang.Closure;
import ext.contracts.PaginationContract;
import play.mvc.Scope;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

import java.io.PrintWriter;
import java.util.Map;

import static ext.contracts.PaginationContract.PageAble.MAX_PAGE_ITEM_DISPLAY;

/**
 * Created by dadang on 5/8/18.
 */
public class CustomTag extends FastTags {

	public static void _pagination(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		PaginationContract.PageAble page = (PaginationContract.PageAble) args.get("arg");
		StringBuilder content = new StringBuilder();
		if (page.getTotal() > 0) {
			int from=1, to=1;
			content.append("<div class=\"row\">");
			if (page.getLimit() < page.getTotal()) {
				content.append("<div class=\"col-sm-9 col-xs-12\">");
				content.append("<ul class=\"pagination pagination-sm\">");
				content.append("<li ").append(!page.isPrevious() ? "class=\"disabled\"" : "").append(">");
				content.append("<a href=\"").append(page.isPrevious() ? page.getPrevLink() : "javascript:void(0)").append("\">Previous</a>");
				content.append("</li>");
				int start, size;
				if (page.getPageCount() <= MAX_PAGE_ITEM_DISPLAY) {
					start = 1;
					size = page.getPageCount();
				} else {
					if (page.getCurPage() <= MAX_PAGE_ITEM_DISPLAY - MAX_PAGE_ITEM_DISPLAY / 2) {
						start = 1;
					} else if (page.getCurPage() >= page.getPageCount() - MAX_PAGE_ITEM_DISPLAY / 2) {
						start = page.getPageCount() - MAX_PAGE_ITEM_DISPLAY + 1;
					} else {
						start = page.getCurPage() - MAX_PAGE_ITEM_DISPLAY / 2;
					}
					size = MAX_PAGE_ITEM_DISPLAY;
				}
				from = start;
				for (int i = 0; i < size; i++) {
					content.append("<li ").append(page.getCurPage() == (start + i) ? "class=\"active\">" : ">");
					content.append("<a href=\"").append(page.getLink() + "page=" + (start + i)).append("\">");
					content.append((start + i));
					content.append("</a>");
					content.append("</li>");
				}
				to = start + (size-1);
				content.append("<li ").append(!page.isNext() ? "class=\"disabled\"" : "").append(">");
				content.append("<a href=\"").append(page.isNext() ? page.getNextLink() : "javascript:void(0)").append("\">Next</a>");
				content.append("</li>");
				content.append("</ul>");
				content.append("</div>");

			}
			content.append("<div class=\"col-sm-3 col-xs-12\">");
			content.append("<div class=\"pagination\">Menampilkan ")
					.append(page.getShow())
					.append(" sampai ")
					.append(page.getFrom())
					.append(" dari ")
					.append(page.getTotal())
					.append("</div>");
			content.append("</div>");
			content.append("</div>");
		}
		out.print(content.toString());
	}

	public static void _alert(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		StringBuilder content = new StringBuilder();
		Scope.Flash flash = Scope.Flash.current();
		if(flash != null && flash.get("error") != null) {	// error sebab diset oleh system spse
			content.append("<div class=\"alert alert-danger\">").append(flash.get("error")).append("</div>");
		}
		if(flash != null && flash.get("success") != null) {
			content.append("<div class=\"alert alert-success\">").append(flash.get("success")).append("</div>");
		}
		out.print(content.toString());
	}

}
