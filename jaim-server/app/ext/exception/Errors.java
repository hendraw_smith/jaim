package ext.exception;

import models.main.Audittrail.AuditType;

/**
 * Created by dadang on 8/7/18.
 */
public enum Errors {
	FORBIDDEN_ACCESS("ACCESS_FORBIDDEN", 403, "Tidak memiliki hak akses", AuditType.ERROR_GENERAL),

	PARAMETER_IS_NULL("PARAMETER_IS_NULL", 402, "Parameter tidak valid/null", AuditType.ERROR_GENERAL),

	LPSE_UPDATE_FORBIDDEN("LPSE_UPDATE_FORBIDDEN", 403, "LPSE tidak diijinkan update otomatis", AuditType.ERROR_LPSE),
	LPSE_NOT_FOUND("LPSE_NOT_FOUND", 404, "LPSE tidak ditemukan/belum terdaftar di jaim server", AuditType.ERROR_LPSE),
	LPSE_NO_UPDATE("LPSE_NO_UPDATE", 403, "LPSE tidak dapat update", AuditType.ERROR_LPSE),
	LPSE_DOWNGRADE_FORBIDDEN("LPSE_DOWNGRADE_FORBIDDEN", 403, "LPSE tidak dapat di downgrade", AuditType.ERROR_LPSE),

	SERVICEPACK_NOT_FOUND("NOT_FOUND", 404, "Tidak ditemukan", AuditType.ERROR_PATCH),
	SERVICEPACK_ALREADY_DOWNLOADED("SERVICEPACK_ALREADY_DOWNLOADED", 403, "Service pack sudah didownload", AuditType.ERROR_PATCH),
	SERVICEPACK_FILE_NOT_FOUND("SERVICEPACK_FILE_NOT_FOUND", 404, "File update tidak ditemukan", AuditType.ERROR_PATCH),
	SERVICEPACK_SCRIPT_NOT_FOUND("SERVICEPACK_SCRIPT_NOT_FOUND", 404, "File script sh tidak ditemukan", AuditType.ERROR_PATCH),
	SERVICEPACK_ALREADY_ACCEPTED("SERVICEPACK_ALREADY_ACCEPTED", 403, "Sudah pernah ditolak/diterima sebelumnya", AuditType.ERROR_PATCH),

	INSTALLER_NOT_FOUND("INSTALLER_NOT_FOUND", 404, "Installer tidak ditemukan", AuditType.ERROR_INSTALLER),

	APPLICATION_NOT_FOUND("APPLICATION_NOT_FOUND", 404, "Application tidak ditemukan", AuditType.ERROR_INSTALLER);

	String status;
	int code;
	String message;
	AuditType auditType;

	Errors(String status, int code, String message){
		this.status = status;
		this.code = code;
		this.message = message;
	}

	Errors(String status, int code, String message, AuditType auditType){
		this.status = status;
		this.code = code;
		this.message = message;
		this.auditType = auditType;
	}
}
