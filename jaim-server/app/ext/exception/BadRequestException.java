package ext.exception;

import models.main.Audittrail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dadang on 8/7/18.
 */
public class BadRequestException extends Exception {

	public String status;
	public int code;
	public String message;
	public List<String> paramName = new ArrayList<>();
	public Audittrail.AuditType auditType = Audittrail.AuditType.ERROR_GENERAL;

	public BadRequestException() {
		super("Bad Request Exception");
	}

	public BadRequestException(Errors errors){
		this(errors, null);
	}

	public BadRequestException(Errors errors, List<String> paramName){
		super(errors.status);
		this.status = errors.status;
		this.code = errors.code;
		this.message = errors.message;
		this.paramName = paramName;
		this.auditType = errors.auditType;
	}
}
