package ext;

/**
 * @author HanusaCloud on 6/4/2018
 */
public class Param {

    private final String key;
    private final Object value;

    public Param(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }
}
