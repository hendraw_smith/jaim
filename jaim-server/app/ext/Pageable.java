package ext;


import ext.contracts.PaginationContract;

import java.util.List;

/**
 * Created by rayhanfrds on 9/18/17.
 */
public class Pageable implements PaginationContract.PageAble {

	private int curPage;
	private int limit = MAX_SIZE_PER_PAGE;
	private long total = 0;
	private String link;
	private String prevLink;
	private String nextLink;
	private boolean previous;
	private boolean next;
	private int[] pageNumbers;
	private int pageCount;
	private int show = 0;
	private int from = 0;

	public Pageable() {}

	public Pageable(int curPage, int limit, int total, String link) {
		this(curPage, limit, (long) total, link);
	}


	public Pageable(int curPage, int limit, long total, String link) {
		this.limit = limit;
		this.total = total;
		this.pageCount = (int) Math.ceil(total * 1.0 / limit * 1.0);
		this.curPage = curPage > pageCount ? 1 : curPage;
		this.link = link;
		this.show = curPage == 1 ? 1 : limit + 1;
		this.from = (limit * curPage) < total ? (limit * curPage) : (int) total;
		this.previous = this.curPage != 1;
		this.next = this.curPage * limit < total;
		pageNumbers = new int[pageCount];
		for (int i = 0; i < pageNumbers.length; i++) {
			pageNumbers[i] = i + 1;
		}
		setLinks();
	}

	private void setLinks() {
		if (!link.contains("?")) {
			link += "?";
		} else if (!link.endsWith("&") && !link.endsWith("?")) {
			link += "&";
		}
		if (previous) {
			prevLink = this.link + "page=" + (curPage - 1);
		}
		if (next) {
			nextLink = this.link + "page=" + (curPage + 1);
		}
	}

	public Pageable(int limit, int total, String link) {
		this(1, limit, total, link);
	}

	public void nextPage() {
		if (isNext()) {
			setCurPage(getCurPage() + 1);
			setPrevious(getCurPage() != 1);
			setNext(getCurPage() * getLimit() < getTotal());
		}
	}

	public void previousPage() {
		if (isPrevious()) {
			setCurPage(getCurPage() - 1);
			setPrevious(getCurPage() != 1);
			setNext(getCurPage() * getLimit() < getTotal());
		}
	}

	/**
	 * @return the curPage
	 */
	public int getCurPage() {
		return curPage;
	}

	/**
	 * @param curPage the curPage to set
	 */
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	/**
	 * @return the limit
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * @return the total
	 */
	public long getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(long total) {
		this.total = total;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the prevLink
	 */
	public String getPrevLink() {
		return prevLink;
	}

	/**
	 * @param prevLink the prevLink to set
	 */
	public void setPrevLink(String prevLink) {
		this.prevLink = prevLink;
	}

	/**
	 * @return the nextLink
	 */
	public String getNextLink() {
		return nextLink;
	}

	/**
	 * @param nextLink the nextLink to set
	 */
	public void setNextLink(String nextLink) {
		this.nextLink = nextLink;
	}

	/**
	 * @return the previous
	 */

	public boolean isPrevious() {
		return previous;
	}

	/**
	 * @param previous the previous to set
	 */
	public void setPrevious(boolean previous) {
		this.previous = previous;
	}

	/**
	 * @return the next
	 */
	public boolean isNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(boolean next) {
		this.next = next;
	}

	/**
	 * @return the pageNumbers
	 */
	public int[] getPageNumbers() {
		return pageNumbers;
	}

	@Override
	public int getTotalCurrentPage() {
		return from;
	}

	@Override
	public Param[] getParams() {
		return new Param[0];
	}

	@Override
	public void setTotal(int total) {

	}

	@Override
	public void setItems(List collection) {

	}

	@Override
	public List getItems() {
		return null;
	}

	@Override
	public Class getReturnType() {
		return null;
	}


}
