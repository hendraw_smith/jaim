package controllers;

import controllers.base.BaseCtr;
import jobs.SpseChangeLogJob;
import models.changelog.ChangelogDataForm;
import models.changelog.ChangelogSearchQuery;
import models.changelog.ChangelogSearchResult;
import models.main.Application;
import models.main.Changelog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 6/4/2018
 */
public class ChangelogCtr extends BaseCtr {

    public static void index() {
        Map<String, Object> map = new HashMap<>();
        ChangelogSearchResult result = Changelog.search(new ChangelogSearchQuery(params));
        map.put("result", result);
        renderTemplate("ChangelogCtr/index.html", map);
    }

    public static void create() {
        List<Application> apps = Application.findAll();
        Map<String, Object> params = new HashMap<>();
        params.put("apps", apps);
        params.put("changelog", new Changelog());
        renderTemplate("ChangelogCtr/input.html", params);
    }

    public static void update(Long id) {
        List<Application> apps = Application.findAll();
        Map<String, Object> params = new HashMap<>();
        params.put("apps", apps);
        params.put("changelog", Changelog.findById(id));
        renderTemplate("ChangelogCtr/input.html", params);
    }

    public static void delete(Long id) {
        if (id == null) {
            flash.error("Gagal hapus changelogs!");
            index();
        }
        Changelog model = Changelog.findById(id);
        if (model == null) {
            flash.error("Gagal hapus, data tidak ditemukan!");
            index();
        }
        model.delete();
        flash.success("Berhasil hapus changelogs.");
        index();
    }

    public static void save(ChangelogDataForm model) {
        checkAuthenticity();
        validation.required(model);
        if (validation.hasErrors()) {
            flash.error("Oops... check kembali form!");
            if (model.id == null) {
                create();
            }
            update(model.id);
        }
        new Changelog(model).save();
        index();
    }

    public static void refresh(){
        new SpseChangeLogJob().now();
        flash.success("Changelogs baru sedang dibaca dari git..");

        index();
    }

}
