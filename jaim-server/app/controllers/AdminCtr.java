package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import controllers.base.BaseCtr;
import jaim.common.model.BaseApplication;
import jaim.common.model.SystemInfo;
import jobs.LpseVersiJob;
import jobs.TouchJaimAgent;
import models.common.UserInfo;
import models.git.Git;
import models.main.*;
import models.main.Audittrail.AuditType;
import models.main.ServicePack.ServicePackStatus;
import models.search.*;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.libs.Json;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.mvc.Router;

import java.io.IOException;
import java.util.*;

public class AdminCtr extends BaseCtr {

	public static void lpse() {
		LpseSearchResult result = Lpse.search(new LpseSearchQuery(params));
		List<Application> listApp=Application.find("1=1").fetch();
		render(result, listApp);
	}

	public static void lpseLatihan() {
		LpseSearchResult result = Lpse.searchLatihan(new LpseSearchQuery(params));
		List<Application> listApp=Application.find("1=1").fetch();
		render(result, listApp);
	}
	
	public static void lpseEdit(Long id) {
		notFoundIfNull(id);
		Lpse lpse = Lpse.findById(id);
		String url = String.format("%s/jaim/sys/info", lpse.lpse_url);
		try {
			HttpResponse resp = WS.url(url).get();
			if(resp.success()) {
				SystemInfo systemInfo = Json.fromJson(resp.getJson(), SystemInfo.class);
				renderArgs.put("sysInfo", systemInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		render(lpse);
	}

	public static void lpseEditSubmit(Lpse lpse) {
		Logger.debug("[AdminCtr] lpseEditSubmit(): %s", lpse);
		Lpse editedLpse = Lpse.findById(lpse.lpse_id);
		String oldLpse = Json.toJson(editedLpse);
		editedLpse.repo_id = lpse.repo_id;
		editedLpse.ip_address = lpse.ip_address;
		editedLpse.lpse_url = lpse.lpse_url;
		editedLpse.host_info = lpse.host_info;
		editedLpse.lpse_nama = lpse.lpse_nama;
		editedLpse.allow_update = lpse.allow_update;
		editedLpse.is_development = lpse.is_development;
		editedLpse.save();
		Audittrail.save(AuditType.UPDATE_LPSE, oldLpse, editedLpse, null);
		flash.success("LPSE berhasil diperbarui.");
		System.out.println(params.get("referer"));
		redirect(params.get("referer"));
	}
	
	public static void lpseBaru(boolean isLatihan, Lpse lpse) {
		if(lpse == null) {
			lpse = new Lpse();
			render(lpse, isLatihan);
		}
		if(lpse.lpse_url != null && lpse.lpse_url.endsWith("/")) {
			lpse.lpse_url=lpse.lpse_url.substring(0, lpse.lpse_url.length()-1);
		}
		if(lpse.lpse_url != null && !lpse.lpse_url.startsWith("http")) {
			flash.error("Invalid URL. Sertakan http:// atau https://");
			render(lpse, isLatihan);
		}
		boolean infoAvailable = lpse.getInfo(isLatihan);
		if(infoAvailable && lpse.isExist() && lpse.lpse_id == null) {
			flash.error("LPSE telah terdaftar. URL: %s", lpse.lpse_url);
			render(lpse, isLatihan);
		}
//		if(!infoAvailable) {
//			flash.error("Gagal ambil data LPSE/LPSE tidak terdaftar di ADP");
//			render(lpse, isLatihan);
//		}
		if(isLatihan && lpse.repo_id==null) {
			lpse.repo_id = 999;
		}
		lpse.save();
		Audittrail.save(AuditType.CREATE_LPSE, null, lpse, null);
		flash.success("%s berhasil ditambahkan.", lpse.lpse_nama);
		render(lpse, isLatihan);
	}
	/**
	 * Dapatkan versi LPSE dengan parsing
	 */
	public static void lpseRefreshVersi() {
		flash.success("Pengecekan Versi LPSE sedang berlangsung secara background. ");
		LpseVersiJob.startJob();
		lpse();
	}

	/**
	 * Dapatkan versi LPSE dari ADP
	 */
	public static void lpseVersi(Long id) {
		Logger.info("[AdminCtr] lpseRefreshVersi - Refresh LPSE_ID: %s", id);
		Lpse lpse = Lpse.findById(id);
		boolean success = lpse.refreshVersiAndSave();
		JsonObject result = new JsonObject();
		result.addProperty("success", success);
		result.addProperty("new_build", lpse.getBuild());

		renderJSON(result);
	}

	public static void createServicePackForm(int id)
	{
		String referer = request.headers.get("referer").values.get(0);
		Lpse lpse=Lpse.findById(id);
		List<Application> apps = lpse.is_development
				? Application.find("name LIKE '%-dev%'").fetch()
				: Application.find("name NOT LIKE '%-dev%'").fetch();
		Application.removeOldVersion(lpse.spse4_version, apps);
		Application app = apps.isEmpty()? new Application("","") : apps.get(0);
		ServicePack sp = new ServicePack();
		sp.keterangan = getUserInfo().user_name;
		render(lpse, app, sp, referer);
	}
	
	public static void viewServicePack(long id) throws IOException
	{
		ServicePack sp=ServicePack.findById(id);
		sp.withLpse();
		Application app=Application.findById(sp.app_id);
		Lpse lpse=Lpse.findById(sp.lpse_id);
		Set<Map.Entry<String, String>> values=sp.getWorkingFileContent().entrySet();
		render(lpse, app, sp, values);
	}

	public static void createServicePackSubmit(ServicePack sp) {
		sp.status = ServicePackStatus.NEW;
		sp.create_date = new Date();
		sp.scheduled_date = new Date();
		sp.keterangan = getUserInfo().email;
		sp.save();
		new TouchJaimAgent(sp.sp_id, sp.app_id).in(1);
		sp.withLpse();
		Logger.debug("[AdminCtr] new ServicePack() %s", sp);
		flash.success("Patch untuk spse %s berhasil dibuat.", sp.lpse.lpse_nama);
		Audittrail.save(AuditType.CREATE_PATCH, null, sp, null);
		redirect(params.get("referer"));
	}
	
	public static void servicePackLog (Long id) throws IOException {
		ServicePack sp=ServicePack.findById(id);
		Lpse lpse=sp.getLpse();
		String url=String.format("%s/jaim.JaimCtr/status?sp_id=%08d", lpse.lpse_url, id);
		try {
			HttpResponse resp= WS.url(url).get();
			Logger.info("[AdminCtr] getStatus sp_id: %s -> %s", url, resp.getString());
			if(resp.success()){
				flash.success("Berhasil mengambil log dari %s.", lpse.lpse_nama);
				Audittrail.save(AuditType.DOWNLOAD_PATCH_LOG, null, sp, null);
			} else {
				flash.error("Gagal mengambil log dari %s.", lpse.lpse_nama);
			}
		} catch (Exception e){
			e.printStackTrace();
			flash.error("Tidak dapat mengakses %s.", lpse.lpse_nama);
		}
		viewServicePack(id);
	}
	
	public static void listApplication(boolean fetch) throws Exception {
		List<Application> applications=Application.find("1=1").fetch();
		List<String> gitLogs=new ArrayList<>();
		for(BaseApplication app: applications) {
			StringBuilder str=new StringBuilder();
			Logger.debug("[git status] : %s", app.name);
			Git git=new Git(app.root + "/" + app.name);
			if(fetch) {
				str.append("==== git fetch ====\n");
				str.append(git.fetch());
			}
			str.append("==== git status ====\n");
			str.append(git.status());
			str.append("==== git log ====\n");
			str.append(git.log(3));
			gitLogs.add(str.toString());
		}
		render(applications, gitLogs);
	}
	
	public static void applicationGitPull(String version) throws Exception
	{
		//git pull lalu update metadata
		Application app=Application.findById(version);
		if(app == null){
			flash.error("Aplikasi/Installer %s tidak ditemukan.", version);
			listApplication(false);
		}
		Application.refreshRepositoryAndSaveMetadataIntoDB(app);
		Audittrail.save(AuditType.GIT_PULL, null, null, null);
		listApplication(false);
	}
	
	public static void lpseDelete(int id) {
		Lpse lpse=Lpse.findById(id);
		lpse.delete();
		Audittrail.save(AuditType.DELETE_LPSE, Json.toJson(lpse), null, null);
		flash.success("LPSE berhasil dihapus");
		lpse();
	}
	
	public static void logout() {
		try {
			UserInfo userInfo = UserInfo.current();
			UserInfo.clear();
			session.clear();
			String centrumURL= Play.configuration.getProperty("centrum.url");
			String url = String.format("%s/OAuthCtr/logout?code=%s&redirect_uri=%s",
					centrumURL, userInfo.centrum_code, Router.getFullUrl("HomeCtr.index"));
			redirect(url);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void refreshRepository() throws IOException {
		Application.refreshRepositoryAndSaveMetadataIntoDB();
		flash.success("Pengecekan daftar aplikasi telah dilakukan.");
		lpse();
	}

	public static void refreshIpAddress(String url){
		String result = Lpse.getIpAddress(url);
		renderJSON("{\"ip_address\":\"" + result + "\"}");
	}

	public static void getLpseFromADP() {
		String urlListLpseAdp = Play.configuration.getProperty("lpselist.adp");
		try {
			WS.HttpResponse resp = WS.url(urlListLpseAdp).timeout("5min").get();
			if(resp.success()) {
				JsonElement content = resp.getJson();
				JsonArray array = content.getAsJsonArray();
				for(JsonElement je : array) {
					JsonObject jo = je.getAsJsonObject();
					Integer repo_id = jo.get("repo_id").getAsInt();
					String repo_url = jo.get("repo_url").getAsString();
					if(repo_url.endsWith("/")) {
						repo_url = repo_url.substring(0, repo_url.length() - 1);
					}
					if(repo_url.endsWith("eproc")) {
						repo_url = repo_url.trim() + "4";
					}
					if(Lpse.lpseIsExist(repo_url, repo_id)) {
						continue;
					}
					Logger.debug("[AdminCtr] - Tarik LPSE baru dari ADP url: %s, id: %s", repo_url, repo_id);
					Lpse lpse = new Lpse();
					lpse.lpse_url = repo_url;
					lpse.repo_id = repo_id;
					lpse.readIpAddress();
					if(!StringUtils.isEmpty(lpse.ip_address)) {
						lpse.save();
					}
				}
			}
			LpseVersiJob.startJobForNewLpse();
			flash.success("Pengambilan LPSE baru sedang dilakukan di background.");
		} catch (Exception e) {
			flash.error("Terdapat kesalahan pada saat pengambilan LPSE baru. Error: "+e.getMessage());
		}
		lpse();
	}

	public static void servicePackHistory(int id){
		Lpse lpse = Lpse.findById(id);
		params.put("lpse_id", String.valueOf(id));
		ServicePackSearchResult result = ServicePack.search(new ServicePackHistoryQuery(params), id);

		render(lpse, result);
	}

	public static void audittrail() {
		AudittrailSearchResult result = Audittrail.search(new AudittrailSearchQuery(params));
		List<Application> listApp=Application.find("1=1").fetch();
		render(result, listApp);
	}

	public static void audittrailView(long id) {
		Audittrail audit = Audittrail.findById(id);

		render(audit);
	}

	public static void lpseInfo(Long id) {
		Lpse lpse = Lpse.find("lpse_id = ?", id).first();
		String url = String.format("%s/jaim/sys/info", lpse.lpse_url);
		HttpResponse resp = WS.url(url).get();
		if(resp.success()) {
			SystemInfo systemInfo = Json.fromJson(resp.getJson(), SystemInfo.class);
			renderArgs.put("sysInfo", systemInfo);
		}
		render(lpse);
	}

	public static void addCustomConf(Integer id) {
		Lpse lpse = Lpse.find("lpse_id = ?", id).first();
		List<LpseCustomConf> customConfs = LpseCustomConf.findByLpse(lpse.lpse_id);
		render(customConfs, lpse);
	}

	public static void saveCustomConf(Integer id) {
		UserInfo userInfo = UserInfo.current();
		Lpse lpse = Lpse.find("lpse_id = ?", id).first();
		if(lpse == null) {
			flash.error("LPSE tidak dapat ditemukan atau tidak terdaftar di Jaim.");
			addCustomConf(id);
		}
		String[] keys = params.getAll("keys");
		String[] values = params.getAll("values");
		String[] infos = params.getAll("infos");
		List<LpseCustomConf> confList = new ArrayList<>();
		for(int i = 0; keys != null && i < keys.length; i++) {
			if(StringUtils.isEmpty(keys[i]) || StringUtils.isEmpty(values[i])) {
				continue;
			}
			LpseCustomConf customConf = new LpseCustomConf();
			customConf.key = keys[i];
			customConf.value = values[i];
			customConf.info = infos[i];
			customConf.created_by_id = userInfo.user_id;
			customConf.lpse_id = lpse.lpse_id;
			confList.add(customConf);
		}

		LpseCustomConf.deleteAllByLpse(lpse.lpse_id);
		LpseCustomConf.saveAll(confList);
		touchAgentForConf(lpse);
		flash.success("Berhasil memperbarui konfigurasi tambahan SPSE.");
		addCustomConf(lpse.lpse_id);
	}

	private static void touchAgentForConf(Lpse lpse) {
		String url = String.format("%s/jaim/conf/confirm", lpse.lpse_url);
		HttpResponse resp = WS.url(url).timeout("30s").get();
		if(resp.success()) {
			Logger.info("[AdminCtr] touchAgentForConf - Success!");
		}
	}
}
