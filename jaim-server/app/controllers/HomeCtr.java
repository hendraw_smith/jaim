package controllers;

import jobs.UserInfoJob;
import models.main.Lpse;
import models.main.ServicePack;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Http;

import java.util.List;

/**
 * Created by dadang on 8/8/18.
 */
public class HomeCtr extends Controller {

	@Before
	protected static void renderData(){

		Http.Header header = request.headers.get("referer");
		String referer = request.getBase();
		if(header != null && header.values != null && header.values.size() > 0){
			referer = header.values.get(0);
		}
		renderArgs.put("referer", referer);
		renderArgs.put("userInfo", UserInfoJob.userInfoMap.get(session.getId()));
	}

	public static void index() {
		if (UserInfoJob.userInfoMap.get(session.getId()) != null) {
			adminHome();
			return;
		}
		render("index.html");
	}

	private static void adminHome() {
		long totalWithLastVersion = Lpse.countSpseWithLastVersion();
		long totalSpseProd = Lpse.countSpseProd();
		long totalWithLastBuild = Lpse.countSpseWithLastBuild();
		Double successAverage = ServicePack.countSuccessAverage();
		List<ServicePack> servicePacks = ServicePack.sp10LastUpdate();

		renderArgs.put("totalWithLastVersion", totalWithLastVersion);
		renderArgs.put("totalSpseProd", totalSpseProd);
		renderArgs.put("totalWithLastBuild", totalWithLastBuild);
		renderArgs.put("successAverage", successAverage);
		renderArgs.put("servicePacks", servicePacks);
		render("/HomeCtr/adminHome.html");
	}
}
