package controllers;

import com.google.gson.Gson;
import models.git.PushBody;
import models.main.Application;
import org.apache.commons.lang3.StringUtils;
import play.Play;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;

import java.io.IOException;
import java.util.List;

public class GitlabApiCtr extends Controller {


	public static void refreshInstaller() {
		Http.Header tokenHead = request.headers.get("x-gitlab-token");
		if(tokenHead == null) {
			response.status = 401;
			renderJSON("{\"message\":\"Invalid Gitlab Token\"}");
		}
		String token = tokenHead.value();
		if(StringUtils.isEmpty(token) || !token.equals(Play.configuration.getProperty("git.webhook.token"))) {
			response.status = 401;
			renderJSON("{\"message\":\"Invalid Gitlab Token\"}");
		}
		String reqBody = params.allSimple().get("body");
		if(StringUtils.isEmpty(reqBody)) {
			response.status = 400;
			renderJSON("{\"message\":\"Invalid Request\"}");
		}
		PushBody pushBody = Json.fromJson(reqBody, PushBody.class);
		if(StringUtils.isEmpty(pushBody.ref)) {
			response.status = 400;
			renderJSON("{\"message\":\"Invalid Request\"}");
		}

		List<Application> applications = Application.find("1=1").fetch();
		Application application = null;
		for(Application app : applications) {
			if(pushBody.ref.endsWith(app.app_id)) {
				application = app;
				break;
			}
		}
		if(application == null) {
			response.status = 400;
			renderJSON("{\"message\":\"Installer Not Found\"}");
		}

		try {
			Application.refreshRepositoryAndSaveMetadataIntoDB(application);
		} catch (IOException e) {
			response.status = 400;
			renderJSON("{\"message\":\"" + e.getMessage() + "\"}");
		}
		renderJSON("{\"message\":\"Success\"}");
	}
}
