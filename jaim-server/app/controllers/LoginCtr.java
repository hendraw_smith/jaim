package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import models.common.UserInfo;
import play.Logger;
import play.Play;
import play.libs.Json;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.mvc.Controller;
import play.mvc.Router;

import java.util.HashMap;

public class LoginCtr extends Controller {


	public static class ResponseData
	{
		public HashMap<String, String> info;
	}
	
	public static void login() {
		String redirect= Router.getFullUrl("LoginCtr.centrumCallback");
		String centrumURL= Play.configuration.getProperty("centrum.url");
		String centrumClientId=Play.configuration.getProperty("centrum.client_id");
		String centrum=String.format("%s/OAuthCtr/authenticate?client_id=%s&redirect_uri=%s", centrumURL, centrumClientId, redirect);
		if(centrumURL==null || centrumURL.startsWith("#")) {
			String password=Play.configuration.getProperty("authentication.password");
			if(password.equals(request.password)) {
				UserInfo userInfo = new UserInfo();
				userInfo.user_name = request.user;
				userInfo.sessionid = session.getId();
				userInfo.save();
				AdminCtr.lpse();
			} else {
				response.setHeader("WWW-Authenticate","Basic realm=\"My Website\"");
				unauthorized("Jaim");
			}
		} else {
			redirect(centrum);
		}
	}
	
	public static void centrumCallback() {
		Logger.info("centrum callback");
		String code=params.get("code");
		String url="%s/OAuthCtr/token?client_id=%s&client_secret=%s&grant_type=authorization_code&code=%s";
		url=String.format(url, Play.configuration.get("centrum.url"),
				Play.configuration.get("centrum.client_id"),
				Play.configuration.get("centrum.client_secret"),
				code);
		HttpResponse req = WS.url(url).post();
		JsonElement resp = req.getJson();

		if(!req.success()) {
			forbidden("Error saat login" + req.getString());
		} else {
			UserInfo userInfo = Json.fromJson(resp.getAsJsonObject().get("info"), UserInfo.class);
			if(userInfo != null) {
				userInfo.sessionid = session.getId();
				userInfo.centrum_code = code;
				userInfo.save();

				HomeCtr.index();
			}
		}
	}
}
