package controllers;

import models.main.Application;
import play.Play;
import play.mvc.Controller;

import java.io.IOException;

/**
 * Created by dadang on 6/21/18.
 */
public class ApplicationCtr extends Controller {

	public static void update_file(){
		// hanya gitlab.lkpp.go.id yang boleh
		if(Play.mode.isProd() && request.remoteAddress != "103.13.181.62"){
			response.status = 403;
			renderJSON("{\"message\":\"hanya bisa diakses oleh gitlab.lkpp.go.id\"}");
		}
		try {
			Application.refreshRepositoryAndSaveMetadataIntoDB();
		} catch (IOException e) {
			e.printStackTrace();
			badRequest(e.getMessage());
		}

		renderJSON("{\"message\":\"success\"}");
	}
}
