package controllers;

import com.google.gson.Gson;
import models.log.LogMeta;
import models.main.Lpse;
import models.main.ServicePack;
import org.apache.commons.io.FilenameUtils;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;

import java.io.File;

/**
 * Created by dadang on 4/4/18.
 */
public class LoggerCtr extends Controller {

	public static void receiveLog(String meta, File file) {
		String token = request.headers.get("token").value();
		if(file == null){
			badRequest("file can not be empty");
		}
		if(!FilenameUtils.getExtension(file.getName()).equalsIgnoreCase("log")){
			badRequest("file format not supported");
		}
		LogMeta logMeta = null;
		try {
			logMeta = Json.fromJson(meta, LogMeta.class);
		} catch (Exception e){
			e.printStackTrace();
			badRequest("invalid meta value");
		}
		ServicePack sp = ServicePack.findById(logMeta.getSpId());
		if(sp == null) badRequest("Service Pack not found");

		Lpse lpse = sp.getLpse();
		if(lpse == null) badRequest("LPSE not found");
		if(!lpse.token.equals(token)) forbidden("Invalid token");

		String path = sp.getBaseFolder();
		File folder = new File(path);
		if(!folder.exists()) folder.mkdirs();

		String fileName;
		if(logMeta.type.equalsIgnoreCase("update")){
			fileName = sp.getUpdateLogFileName();
		} else {
			fileName = sp.getBackupLogFileName();
		}
		File dest = new File(folder, fileName);
		file.renameTo(dest);
		Logger.info("[JAIM-SERVER] Logs File saved to %s", path);
		renderJSON("{\"status\":\"ok\"}");
	}
}
