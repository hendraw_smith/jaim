package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import controllers.base.BaseApiCtr;
import models.main.Lpse;
import models.main.LpseCustomConf;
import play.mvc.Http;

import java.util.List;

public class LpseCustomConfCtr extends BaseApiCtr {


	public static void getByLpse() {
		Http.Header tokenHead = request.headers.get("uuid");
		if(tokenHead == null) {
			response.status = 401;
			renderJSON("{\"message\":\"Invalid UUID\"}");
		}
		String uuid = tokenHead.value();
		Lpse lpse = Lpse.getLpse(uuid);
		if(lpse == null) {
			response.status = 404;
			renderJSON("{\"message\":\"LPSE Not Found\"}");
		}
		List<LpseCustomConf> customConfs = LpseCustomConf.findByLpse(lpse.lpse_id);
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String result = gson.toJson(customConfs);

		renderJSON(result);
	}
}
