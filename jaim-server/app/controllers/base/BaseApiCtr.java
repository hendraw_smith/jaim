package controllers.base;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import ext.exception.BadRequestException;
import ext.exception.Errors;
import models.main.Audittrail;
import models.main.Lpse;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.mvc.Catch;
import play.mvc.Controller;
import play.mvc.Http;

/**
 * Created by dadang on 8/7/18.
 */
public class BaseApiCtr extends Controller {

	//@Before
	public static void authenticate() throws BadRequestException {
		Http.Header uuiHead = request.headers.get("uuid");
		String uuid = uuiHead.value();
		boolean checkUUID = Play.configuration.getProperty("spse.cekuuid").equals("true");
		if(checkUUID) {
			if(StringUtils.isEmpty(uuid)) {
				throw new BadRequestException(Errors.FORBIDDEN_ACCESS);
			}
			Lpse lpse = Lpse.getLpse(uuid);
			if(lpse == null) {
				throw new BadRequestException(Errors.FORBIDDEN_ACCESS);
			}
		}
	}

	@Catch
	protected static void throwHandler(BadRequestException bad){
		JsonObject errResponse = new JsonObject();
		errResponse.addProperty("status", bad.status);
		errResponse.addProperty("message", bad.message);
		if(bad.paramName != null && !bad.paramName.isEmpty()) {
			JsonArray jsonArray = new JsonArray();
			for(String atr : bad.paramName) {
				jsonArray.add(atr);
			}
			errResponse.add("err_attributes", jsonArray);
		}
		response.status = bad.code;
		response.contentType = "application/json";
		Audittrail.save(bad.auditType, null, null, null);
		renderJSON(errResponse.toString());
	}

	protected static void ok(String message){
		JsonObject errResponse = new JsonObject();
		errResponse.addProperty("status", "SUCCESS");
		errResponse.addProperty("message", message);
		response.contentType = "application/json";

		Logger.debug("[BaseApiCtr] success, %s", errResponse.toString());
		renderJSON(errResponse.toString());
	}

}
