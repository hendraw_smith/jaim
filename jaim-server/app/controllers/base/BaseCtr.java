package controllers.base;

import jobs.UserInfoJob;
import models.common.UserInfo;
import play.mvc.Controller;
import play.mvc.Http;

/**
 * Created by dadang on 7/23/18.
 */

public class BaseCtr extends Controller {
	@play.mvc.Before(unless={"AdminCtr.logout"})
	static void interceptor() {

		if(getUserInfo() == null) {
			forbidden("Anda belum login/Session telah expired");
		}

		Http.Header header = request.headers.get("referer");
		String referer = request.getBase();
		if(header != null && header.values != null && header.values.size() > 0){
			referer = header.values.get(0);
		}
		renderArgs.put("referer", referer);
		renderArgs.put("userInfo", getUserInfo());

	}

	protected static UserInfo getUserInfo(){
		return UserInfoJob.userInfoMap.get(session.getId());
	}
}
