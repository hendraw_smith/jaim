package controllers;

import com.google.gson.JsonObject;
import controllers.base.BaseApiCtr;
import ext.exception.BadRequestException;
import ext.exception.Errors;
import jaim.common.model.BaseApplication;
import jaim.common.model.ChangelogItem;
import jaim.common.model.ResponseCheck;
import jaim.common.util.Version;
import jobs.LpseVersiJob;
import models.main.*;
import models.main.Audittrail.AuditType;
import models.main.ServicePack.ServicePackStatus;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.data.FileUpload;
import play.libs.Json;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/** * LPSE melakukan POST ke JaimServer.
 * Ini untuk meningkatkan keamanan karena JaimServer sudah HTTPS sedangkan LPSE tidak semua HTTPS.
 * 
 *  Kalau JaimServer yang request ke LPSE dan langsung di-response maka kurang aman 
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
public class ServicePackCtr extends BaseApiCtr {

	public static final String JAIM_VERSION = "2.0"; //

	@Deprecated
	public static void getZipPackage(Long sp_id) {
		ServicePack sp=ServicePack.findById(sp_id);
		if(sp==null)
			forbidden("sp_id tidak ditemukan: " + sp_id);
		sp.withLpse();
		if(!sp.lpse.allow_update)
			forbidden("AutoUpdate not allowed.");
		String json=request.params.get("fileAttributes");
		String jaimLogDir=params.get("jaimLogDir");
		String spseRootDir = params.get("spseRootDir");
		String backupDbDir = params.get("backupDbDir");
		if(spseRootDir == null || spseRootDir.isEmpty())spseRootDir = "/home/appserv";
		if(backupDbDir == null || backupDbDir.isEmpty())backupDbDir = "/home/backup_db";

		BaseApplication appOfAgent=new BaseApplication(sp.app_id, null);
		appOfAgent.prepareFilesAttribute(json);
		Logger.debug("[ServicePackCtr] getZipPackage, SPSE file count: %,d" , appOfAgent.fileAttributes.size());
		if(sp.status.equals(ServicePackStatus.DOWNLOADED))
			forbidden("Already downloaded");
		if(sp.status.isTouch())
			sp.generateFiles(appOfAgent, jaimLogDir, spseRootDir, backupDbDir);
		if(sp.needs_update) {
			File file=new File(sp.getZipPackageFile());
			Logger.debug("[ServicePackCtr] getZipPackage: %s, size: %,d", sp, file.length());
			//simulasi jika network lambat
			Lpse lpse = sp.lpse;
			if(lpse == null) badRequest("LPSE not found");
			//token untuk verifikasi ketika request ke server. regenerate setiap request pack.
			lpse.token = RandomStringUtils.randomAlphanumeric(32);
			lpse.save();
			response.setHeader("token", lpse.token);
			renderBinary(file);
		} else {
			sp.status=ServicePackStatus.NO_UPDATE;
			sp.save();
			Logger.debug("[ServicePackCtr] getZipPackage: %s, NoUpdate", sp);
			forbidden("No-Update");
		}
	}

	/**
	 * Menerima confirm dari SPSE
	 * @param sp_id
	 * @throws BadRequestException
	 * Return status migasi. Update dengan file baru, tanpa file baru, tanpa backup db atau dengan backup db
	 */
	public static void getStatusUpdate(Long sp_id) throws BadRequestException {
		Logger.debug("[ServicePackCtr] getStatusUpdate, check status for %s.. ", sp_id);
		ServicePack sp = ServicePack.findById(sp_id);
		if(sp == null) {
			throw new BadRequestException(Errors.SERVICEPACK_NOT_FOUND);
		}
		Lpse lpse = sp.getLpse();
		if(lpse == null) {
			throw new BadRequestException(Errors.LPSE_NOT_FOUND);
		}
		if(!lpse.allow_update){
			throw new BadRequestException(Errors.LPSE_UPDATE_FORBIDDEN);
		}
		if(sp.status.equals(ServicePackStatus.DOWNLOADED)){
			throw new BadRequestException(Errors.SERVICEPACK_ALREADY_DOWNLOADED);
		}
		String json=request.params.get("fileAttributes");
		String jaimLogDir=params.get("jaimLogDir");
		String spseRootDir = params.get("spseRootDir");
		String backupDbDir = params.get("backupDbDir");
		Integer needMigration = params.get("needMigration", Integer.class);
		sp.need_migration = needMigration == null ? 0 : needMigration;
		boolean doBackup = true;
		if(params.get("do_backup") != null) {
			doBackup = params.get("do_backup", Boolean.class);
		}
		sp.do_backup = doBackup;
		if(spseRootDir == null || spseRootDir.isEmpty())spseRootDir = "/home/appserv";
		if(backupDbDir == null || backupDbDir.isEmpty())backupDbDir = "/home/backup_db";

		BaseApplication appOfAgent=new BaseApplication(sp.app_id, null);
		appOfAgent.prepareFilesAttribute(json);
		Logger.debug("[ServicePackCtr] getStatusUpdate, SPSE file count: %,d sp: %s" , appOfAgent.fileAttributes.size(), sp.sp_id);

		if(sp.status.isTouch()) {
			sp.generateFiles(appOfAgent, jaimLogDir, spseRootDir, backupDbDir);
		}
		Audittrail.save(AuditType.CHECK_PATCH, null, null, null);
		if(sp.needs_update) {
			sp.status = ServicePackStatus.READY_TO_UPDATE;
			sp.save();
			//token untuk verifikasi ketika request ke server. regenerate setiap request pack.
			lpse.token = RandomStringUtils.randomAlphanumeric(32);
			lpse.save();
			String withFile = sp.updateWithFile ? "_WITH_FILE" : "_WITHOUT_FILE";
			response.setHeader("token", lpse.token);
			ok(ServicePackStatus.READY_TO_UPDATE.name() + withFile);
		} else if(sp.need_migration > 0){
			sp.status = ServicePackStatus.READY_TO_UPDATE;
			sp.save();
			//token untuk verifikasi ketika request ke server. regenerate setiap request pack.
			lpse.token = RandomStringUtils.randomAlphanumeric(32);
			lpse.save();
			response.setHeader("token", lpse.token);
			ok(ServicePackStatus.READY_TO_UPDATE.name() + "_ONLY_MIGRATION");
		} else {
			sp.status=ServicePackStatus.NO_UPDATE;
			sp.save();
			ok(ServicePackStatus.NO_UPDATE.name());
		}
	}

	/**
	 * Download zip patch baru
	 * @param sp_id
	 * @throws BadRequestException
	 */
	public static void getZipPackageV2(Long sp_id) throws BadRequestException {
		ServicePack sp = ServicePack.findById(sp_id);
		if(sp == null) {
			throw new BadRequestException(Errors.SERVICEPACK_NOT_FOUND);
		}
		Lpse lpse = sp.getLpse();
		if(lpse == null) {
			throw new BadRequestException(Errors.LPSE_NOT_FOUND);
		}
		if(!lpse.allow_update){
			throw new BadRequestException(Errors.LPSE_UPDATE_FORBIDDEN);
		}
		if(!sp.status.equals(ServicePackStatus.READY_TO_UPDATE)){
			throw new BadRequestException(Errors.LPSE_NO_UPDATE);
		}
		File file=new File(sp.getZipPackageFile());
		if(!file.exists()) {
			throw new BadRequestException(Errors.SERVICEPACK_FILE_NOT_FOUND);
		}
		Audittrail.save(Audittrail.AuditType.DOWNLOAD_PATCH, null, null, null);
		Logger.info("[ServicePackCtr] getZipPackageV2: %s, size: %,d", sp, file.length());
		renderBinary(file);
	}

	/**
	 * Download script untuk eksekusi update
	 * @param sp_id
	 * @throws BadRequestException
	 */
	public static void getScriptV2(Long sp_id) throws BadRequestException {
		ServicePack sp = ServicePack.findById(sp_id);
		if(sp == null) {
			throw new BadRequestException(Errors.SERVICEPACK_NOT_FOUND);
		}
		Lpse lpse = sp.getLpse();
		if(lpse == null) {
			throw new BadRequestException(Errors.LPSE_NOT_FOUND);
		}
		if(sp.status.equals(ServicePackStatus.DOWNLOADED)) {
			throw new BadRequestException(Errors.SERVICEPACK_ALREADY_DOWNLOADED);
		}
		File file=new File(sp.getShellScriptFile());
		if(!file.exists()) {
			throw new BadRequestException(Errors.SERVICEPACK_SCRIPT_NOT_FOUND);
		}
		sp.status = ServicePackStatus.DOWNLOADED;
		sp.download_date = new Date();
		sp.save();
		Audittrail.save(Audittrail.AuditType.DOWNLOAD_SCRIPT,null, null, null);
		Logger.debug("[ServicePackCtr] getScriptV2: %s, size: %,d", sp, file.length());
		renderBinary(file);
	}

	@Deprecated
	public static void getScript(Long sp_id) {
		ServicePack sp = ServicePack.findById(sp_id);
		sp.withLpse();
		if(sp.status.equals(ServicePackStatus.DOWNLOADED))
			forbidden("Already downloaded");
		sp.status = ServicePackStatus.DOWNLOADED;
		sp.download_date = new Date();
		sp.save();
		File file = new File(sp.getShellScriptFile());
		Logger.debug("[ServicePackCtr] getScript: %s, size: %,d", sp, file.length());
		renderBinary(file);
	}
	
	/**
	 * SPSE mengirimkan status atas SP tertentu
	 * @param sp_id
	 * @throws IOException 
	 */
	public static void postStatus(long sp_id) throws IOException {
		FileUpload updateLog=params.get("JaimAgent.log", FileUpload.class);
		FileUpload backupLog=params.get("Backup.log", FileUpload.class);
		ServicePack sp=ServicePack.findById(sp_id);
		sp.withLpse();
		if(updateLog != null) {
			//copy to temp
			saveFileLog(sp.getLogFilePathString(), updateLog);
		}
		if(backupLog != null){
			//copy to temp
			saveFileLog(sp.getBackupLogFilePatString(), backupLog);
		}
	}

	private static void saveFileLog(String fileName, FileUpload fileUpload) throws IOException {
		File logFile=new File(fileName);
		Logger.info("[ServicePackCtr] saveFileLog: %s", logFile);
		FileUtils.copyFile(fileUpload.asFile(), logFile);
	}

	private static void updateServicePack(long sp_id, ServicePackStatus spStatus) {
		Logger.info("[UPDATE_STATUS] lpse sp_id: " + sp_id + " status: " + spStatus.name());
		final String token = request.headers.get("token").value();
		if (token == null || token.isEmpty()) {
			Logger.info("[UPDATE_STATUS] Authentication failed - token null or empty.");
			badRequest("Authentication failed.");
		}
		Lpse lpse = Lpse.findByToken(token);
		if (lpse == null) {
			Logger.info("[UPDATE_STATUS] Authentication failed - lpse null.");
			badRequest("Authentication failed.");
		}
		ServicePack sp = ServicePack.findById(sp_id);
		if(sp == null){
			Logger.info("[UPDATE_STATUS] Service Pack not found.");
			badRequest("Service Pack not found");
		}
		if(spStatus.equals(ServicePackStatus.FINISH)) {
			new LpseVersiJob(sp.lpse).in(300);
		}
		sp.status = spStatus;
		sp.save();
		Audittrail.save(Audittrail.AuditType.UPDATE_PATCH, null, null, null);
		renderJSON("{\"status\":\"OK\"}");
	}

	public static void isBackingUp(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.BACKUP_DB);
	}

	public static void isBackingUpFailed(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.BACKUP_DB_FAILED);
	}

	public static void isBackingUpFile(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.BACKUP_FILE);
	}

	public static void isBackingUpFileFailed(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.BACKUP_FILE_FAILED);
	}

	public static void isMigrating(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.MIGRATION);
	}

	public static void isUpdating(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.UPDATE);
	}

	public static void isUpdatingFailed(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.UPDATE_FAILED);
	}

	public static void isFinished(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.FINISH);
	}

	public static void isStarted(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.START);
	}

	public static void dbRequirementPassed(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.DB_REQUIREMENT_PASSED);
	}

	public static void dbRequirementFailed(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.DB_REQUIREMENT_FAILED);
	}

	public static void packageRequirementPassed(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.PACKAGE_REQUIREMENT_PASSED);
	}

	public static void packageRequirementFailed(long sp_id){
		updateServicePack(sp_id, ServicePackStatus.PACKAGE_REQUIREMENT_FAILED);
	}

	@Deprecated
	public static void check(Integer lpse_id, String lpse_versi) throws IOException {
		String json=request.params.get("fileAttributes");
		String uuid=request.params.get("uuid");
		Application app = Application.findById(lpse_versi);
		String realIpAddress = request.remoteAddress;
		Logger.info("[JAIM-SERVER] - Checking update for LPSE %s IP : %s", lpse_id, realIpAddress);
		if(app == null){
			Logger.info("[JAIM-SERVER] - app : %s not found", lpse_versi);
			badRequest("Application not found");
		}
		boolean cekUUID = Play.configuration.getProperty("spse.cekuuid").equals("true");
		Lpse lpse = lpse_id != 999 && !cekUUID ? Lpse.getLpse(lpse_id) : Lpse.getLpse(lpse_id, realIpAddress, uuid);
		if(lpse == null){
			Logger.info("[JAIM-SERVER] - lpse not found");
			badRequest("LPSE not found in JAIM. Contact JAIM Administrator");
		}
		if(!lpse.allow_update){
			Logger.info("[JAIM-SERVER] lpse tidak diizinkan update otomatis");
			forbidden("AutoUpdate not allowed. Contact JAIM Administrator");
		}

		Version reqVersion = new Version(Application.getVersion(lpse_versi));
		Version curVersion = new Version(lpse.spse4_version);
		if(reqVersion.compareTo(curVersion) < 0){
			badRequest("LPSE can not be downgraded!");
		}

		BaseApplication appOfAgent=new BaseApplication(app.app_id, null);
		appOfAgent.prepareFilesAttribute(json);
		boolean needsUpdate = app.isClientNeedUpdate(appOfAgent);
		ResponseCheck resp = new ResponseCheck();
		resp.needUpdate = needsUpdate;
		createNewSp(app, lpse, needsUpdate, resp);
		renderJSON(resp);
	}

	private static void createNewSp(Application app, Lpse lpse, boolean needsUpdate, ResponseCheck resp) {
		if(needsUpdate){
			ServicePack sp = new ServicePack();
			sp.status = ServicePackStatus.TOUCH;
			sp.app_id = app.app_id;
			sp.lpse_id = lpse.lpse_id;
			sp.keterangan = "SYSTEM";
			sp.create_date = new Date();
			sp.do_backup = true;
			sp.save();
			resp.spId = sp.sp_id;
			resp.spseVersion = sp.app_id;
		}
	}

	public static void checkV2(Integer lpse_id, String lpse_versi) throws BadRequestException, IOException {
		String json = request.params.get("fileAttributes");
		String uuid = request.params.get("uuid");
		List<String> nullParams = new ArrayList<>();
		boolean cekUUID = Play.configuration.getProperty("spse.cekuuid").equals("true");
		String realIpAddress = request.remoteAddress;

		if(StringUtils.isEmpty(json)) {
			nullParams.add("fileAttributes");
		}
		if(StringUtils.isEmpty(uuid)) {
			nullParams.add("uuid");
		}
		if(StringUtils.isEmpty(lpse_versi)) {
			nullParams.add("lpse_versi");
		}
		if(lpse_id == null) {
			nullParams.add("lpse_id");
		}
		if(!nullParams.isEmpty()) {
			throw new BadRequestException(Errors.PARAMETER_IS_NULL, nullParams);
		}

		Application app = Application.findById(lpse_versi);
		if(app == null) {
			throw new BadRequestException(Errors.INSTALLER_NOT_FOUND);
		}
		Lpse lpse = lpse_id != 999 && !cekUUID ? Lpse.getLpse(lpse_id) : Lpse.getLpse(lpse_id, realIpAddress, uuid);
		if(lpse == null) {
			throw new BadRequestException(Errors.LPSE_NOT_FOUND);
		}
		if(!lpse.allow_update) {
			throw new BadRequestException(Errors.LPSE_UPDATE_FORBIDDEN);
		}

		Version reqVersion = new Version(Application.getVersion(lpse_versi));
		Version curVersion = new Version(lpse.spse4_version);
		if(reqVersion.compareTo(curVersion) < 0){
			throw new BadRequestException(Errors.LPSE_DOWNGRADE_FORBIDDEN);
		}

		BaseApplication appOfAgent=new BaseApplication(app.app_id, null);
		appOfAgent.prepareFilesAttribute(json);
		boolean needsUpdate = app.isClientNeedUpdate(appOfAgent);
		ResponseCheck resp = new ResponseCheck();
		resp.needUpdate = needsUpdate;
		createNewSp(app, lpse, needsUpdate, resp);
		Audittrail.save(Audittrail.AuditType.CHECK_PATCH, null, null, null);
		renderJSON(resp);

	}

	public static void accept(Long sp_id, String lpse_versi) throws BadRequestException, IOException {
		Logger.info("accept");
		String applicationDate = request.params.get("applicationDate");
		String uuid = request.params.get("uuid");
		Logger.info("applicationDate = %s", applicationDate);
		Logger.info("uuid = %s", uuid);

		List<String> nullParams = new ArrayList<>();
		boolean cekUUID = Play.configuration.getProperty("spse.cekuuid").equals("true");
		String realIpAddress = request.remoteAddress;

		if(StringUtils.isEmpty(applicationDate)) {
			nullParams.add("applicationDate");
		}
		if(cekUUID && StringUtils.isEmpty(uuid)) {
			nullParams.add("uuid");
		}
		if(StringUtils.isEmpty(lpse_versi)) {
			nullParams.add("lpse_versi");
		}
		if(sp_id == null) {
			nullParams.add("sp_id");
		}
		if(!nullParams.isEmpty()) {
			throw new BadRequestException(Errors.PARAMETER_IS_NULL, nullParams);
		}
		final DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date buildDate;
		try {
			buildDate = df.parse(applicationDate);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new BadRequestException(Errors.FORBIDDEN_ACCESS, Arrays.asList("Invalid date format"));
		}

		ServicePack sp = ServicePack.findById(sp_id);
		if(sp == null) {
			throw  new BadRequestException(Errors.SERVICEPACK_NOT_FOUND);
		}

		Logger.info("NO ERRORS");
//		if(!lpse.allow_update) {
//			throw new BadRequestException(Errors.LPSE_UPDATE_FORBIDDEN);
//		}

//		Version reqVersion = new Version(Application.getVersion(lpse_versi));
//		Version curVersion = new Version(lpse.spse4_version);
//		if(reqVersion.compareTo(curVersion) < 0){
//			throw new BadRequestException(Errors.LPSE_DOWNGRADE_FORBIDDEN);
//		}

		Optional<LpseAcceptance> a = LpseAcceptance.getByServicePack(sp);
		if(a.isPresent() && a.get().acceptance_status!=null && a.get().acceptance_status!=""){
			throw new BadRequestException(Errors.SERVICEPACK_ALREADY_ACCEPTED);
		}

		LpseAcceptance acceptance = new LpseAcceptance(sp);
		acceptance.accept();

		Logger.info("OK");
		renderJSON(Json.toJson(new HashMap<String, String>(){{put("response","OK");}}));

	}

	public static void reject(Long sp_id, String lpse_versi) throws BadRequestException, IOException {
		Logger.info("reject");
		String applicationDate = request.params.get("applicationDate");
		String uuid = request.params.get("uuid");
		Logger.info("applicationDate = %s", applicationDate);
		Logger.info("uuid = %s", uuid);

		List<String> nullParams = new ArrayList<>();
		boolean cekUUID = Play.configuration.getProperty("spse.cekuuid").equals("true");
		String realIpAddress = request.remoteAddress;

		if(StringUtils.isEmpty(applicationDate)) {
			nullParams.add("applicationDate");
		}
		if(cekUUID && StringUtils.isEmpty(uuid)) {
			nullParams.add("uuid");
		}
		if(StringUtils.isEmpty(lpse_versi)) {
			nullParams.add("lpse_versi");
		}
		if(sp_id == null) {
			nullParams.add("sp_id");
		}
		if(!nullParams.isEmpty()) {
			throw new BadRequestException(Errors.PARAMETER_IS_NULL, nullParams);
		}
		final DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date buildDate;
		try {
			buildDate = df.parse(applicationDate);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new BadRequestException(Errors.FORBIDDEN_ACCESS, Arrays.asList("Invalid date format"));
		}

		ServicePack sp = ServicePack.findById(sp_id);
		if(sp == null) {
			throw  new BadRequestException(Errors.SERVICEPACK_NOT_FOUND);
		}

		Logger.info("NO ERRORS");
//		if(!lpse.allow_update) {
//			throw new BadRequestException(Errors.LPSE_UPDATE_FORBIDDEN);
//		}

//		Version reqVersion = new Version(Application.getVersion(lpse_versi));
//		Version curVersion = new Version(lpse.spse4_version);
//		if(reqVersion.compareTo(curVersion) < 0){
//			throw new BadRequestException(Errors.LPSE_DOWNGRADE_FORBIDDEN);
//		}

		Optional<LpseAcceptance> a = LpseAcceptance.getByServicePack(sp);
		if(a.isPresent() && a.get().acceptance_status!=null && a.get().acceptance_status!=""){
			throw new BadRequestException(Errors.SERVICEPACK_ALREADY_ACCEPTED);
		}

		LpseAcceptance acceptance = new LpseAcceptance(sp);
		acceptance.reject();

		Logger.info("OK");
		renderJSON(Json.toJson(new HashMap<String, String>(){{put("response","OK");}}));
	}

	public static void getChangeLogs(long sp_id){
		Logger.info("[JAIM] get change logs for %s ", sp_id);
		ServicePack sp = ServicePack.findById(sp_id);
		notFoundIfNull(sp);
		sp.withLpse();
		Lpse lpse = sp.lpse;
		notFoundIfNull(lpse);
		Application app = Application.findById(sp.app_id);
		notFoundIfNull(app);
		Logger.info("lpse = %s",lpse);
		Logger.info("getByDate, date = %s, appId = %s", lpse.getLastBuild(), app.app_id);
		ChangelogItem[] empty = {};
		if(isRejected(sp))
			renderJSON((Json.toJson(empty)));
		ChangelogItem[] changelogItems = Changelog.getChangelogItems(lpse.getLastBuild(), app.app_id);
		Audittrail.save(Audittrail.AuditType.CHANGELOG_DETAIL, null, null, null);
		renderJSON(Json.toJson(changelogItems));
	}

	private static boolean isRejected(ServicePack sp) {
		Optional<LpseAcceptance> acceptance = LpseAcceptance.getByServicePack(sp);
		if(!acceptance.isPresent())
			return false;
		Logger.info("acceptance = %s",acceptance);
		return acceptance.get().isRejected();
	}

	public static void getChangeLogsAfter(String app_id, Date after) throws BadRequestException {
		Logger.info("[ServicePackCtr] getChangeLogs - get changelogs after %s", after);
		List<String> nullParams = new ArrayList<>();
		if(StringUtils.isEmpty(app_id)) {
			nullParams.add("app_id");
		}
		if(after == null) {
			nullParams.add("date after");
		}
		if(!nullParams.isEmpty()) {
			throw new BadRequestException(Errors.PARAMETER_IS_NULL, nullParams);
		}
		Application application = Application.findById(app_id);
		if(application == null) {
			throw new BadRequestException(Errors.APPLICATION_NOT_FOUND);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		ChangelogItem[] changelogItems = Changelog.getChangelogItems(sdf.format(after), application.app_id);

		renderJSON(Json.toJson(changelogItems));
	}

	public static void recordOS(int lpse_id, String info) {
		Logger.info("[RECORD_OS] lpseId: " + lpse_id);
		Logger.info("[RECORD_OS] info: " + info);
		String uuid = request.params.get("uuid");
		boolean cekUUID = Play.configuration.getProperty("spse.cekuuid").equals("true");
		String realIpAddress = request.remoteAddress;
		Lpse lpse = lpse_id != 999 && !cekUUID ? Lpse.getLpse(lpse_id) : Lpse.getLpse(lpse_id, realIpAddress, uuid);

		if (lpse == null) {
			badRequest("LPSE not found in JAIM. Contact JAIM Administrator");
		}
		if (lpse.getClientInfoModel() == null) {
			Logger.info("[RECORD_OS] update client info");
			lpse.setClientInfoModel(info);
			lpse.save();
		}
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("status", "success");
		renderJSON(jsonObject);
	}

	public static void versi() {
		renderText(JAIM_VERSION);
	}

}
