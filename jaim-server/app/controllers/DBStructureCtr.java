/**
 * 
 */
package controllers;

import org.apache.commons.io.IOUtils;
import play.Logger;
import play.Play;
import play.mvc.Controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

/**
 * @author AndikYulianto@yahoo.com
 *
 */
public class DBStructureCtr extends Controller {

	public static void upload(String request_id, int lpse_id) throws IOException
	{
		File fileName=new File(String.format("%s/tmp/%03d/DBStructure.json", Play.applicationPath, lpse_id));
		fileName.getParentFile().mkdirs();
		FileOutputStream out=new FileOutputStream(fileName);
		IOUtils.copy(new GZIPInputStream(request.body), out);
		out.close();
		Logger.debug("[DBStructureCtr] upload: %s, %s", request_id, fileName);

	}
}
