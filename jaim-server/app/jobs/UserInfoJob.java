package jobs;

import models.common.UserInfo;
import org.apache.commons.collections4.MapUtils;
import play.Logger;
import play.Play;
import play.jobs.Job;
import play.libs.Time;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserInfoJob extends Job {

	public static final ConcurrentHashMap<String, UserInfo> userInfoMap = new ConcurrentHashMap<String, UserInfo>();

	int maxInactiveInterval = Time.parseDuration(Play.configuration.getProperty("application.session.maxAge", "30mn")); // default 30 menit

	@Override
	public void doJob() throws Exception {
		if (Play.configuration.get("application.mode").toString().equalsIgnoreCase("dev"))
			return;
		if(!MapUtils.isEmpty(userInfoMap)) {
			long timeNow = new Date().getTime();
			for(Map.Entry<String, UserInfo> entry : userInfoMap.entrySet()) {
				UserInfo userInfo = entry.getValue();
				int timeIdle = (int) ((timeNow - userInfo.lastRequest) / 1000L);
				Logger.debug("timeIde - userInfo : %s/%s - %s", timeIdle, maxInactiveInterval, userInfo.sessionid);
				if (timeIdle >= maxInactiveInterval) {
					UserInfo.clear(userInfo);
				}
			}
		}
	}
}
