package jobs;

import play.Logger;
import play.jobs.Job;
import play.jobs.On;

/**
 * Created by dadang on 9/7/18.
 */
@On("0 0 1 * * ?")
public class LpseVersiCheckJob extends Job {

	public void doJob() {
		Logger.info("[LpseVersiCheckJob] Start refreshing ALL SPSE version..");
		LpseVersiJob.startJob();
		Logger.info("[LpseVersiCheckJob] Refreshing ALL SPSE version FINISH.");
	}
}
