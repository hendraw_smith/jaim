package jobs;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import models.main.Application;
import models.main.Changelog;
import models.git.Commit;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.jobs.Every;
import play.jobs.Job;
import play.libs.Json;
import play.libs.WS;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Created by dadang on 7/9/18.
 * Job menggunakan API gitlab v4
 * Api digunakan untuk mengambil list commit berdasarkan tanggal {since}
 *
 */
@Every("1h")
public class SpseChangeLogJob extends Job {

	public void doJob(){
		Logger.info("[JAIM-SERVER] job start reading commit message..");
		String commitListApi = Play.configuration.getProperty("commit.list.api");
		String privateToken = Play.configuration.getProperty("git.private.token");

		List<Application> apps = Application.find("1=1").fetch();
		for (Application app : apps) {
			Changelog changelog = Changelog.getLastChangelog(app.app_id);
			try(WS.WSRequest request = WS.url(commitListApi)) {
				request.setHeader("PRIVATE-TOKEN", privateToken);
				request.setParameter("ref_name", app.app_id);
				if (changelog != null) {
					request.setParameter("since", changelog.date);
					Logger.info("[JAIM-SERVER] get commit since %s", changelog.date);
				} else {
					Logger.info("[JAIM-SERVER] get commit since the first time.");
				}
				request.timeout("60s");
				WS.HttpResponse response = request.get();
				List<Commit> commits = new ArrayList<>();
				Logger.info("[JAIM-SERVER] API response %s, %s", response.getStatus(), response.getStatusText());
				if (response.success()) {
					Logger.info("[JAIM-SERVER] API success..");
					JsonArray results = response.getJson().getAsJsonArray();
					Logger.info("[JAIM-SERVER] Get %s new commit", results.size());
					for (JsonElement result : results) {
						Commit commit = Json.fromJson(result, Commit.class);
						commits.add(commit);
					}
				}
				if (commits.size() > 0) {
					saveToChangeLogs(commits, app);
				}
			} catch (IOException e) {
				Logger.error(e,"[JAIM-SERVER] get commit gagal");
			}
		}

		Logger.info("[JAIM-SERVER] SpseChangeLogJob finish..");
	}
	
	private void saveToChangeLogs(List<Commit> commits, Application app){
		Logger.info("[JAIM-SERVER] Start Saving to changeLogs..");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		int invalid = 0;
		for (Commit commit : commits) {
			try {
				Logger.info("[JAIM-SERVER] APP ID : %s, ********************************");
				Logger.info("[JAIM-SERVER] APP ID : %s, Message = %s", app.app_id, commit.message);
				Logger.info("[JAIM-SERVER] APP ID : %s, created_at = %s", app.app_id, sdf.format(commit.created_at));
				// check apakah changelog ada/tidak
				Changelog existingChangelog = Changelog.getByCommitId(commit.id);
				if(existingChangelog != null) {
					Logger.info("[JAIM-SERVER] Changelogs for %s already exist!", commit.id);
					continue;
				}

				Pattern pattern = Pattern.compile("(.|\\n)*?(\\d.+)u([0-9]+)\\.([0-9]+)_\\s?((.|\\n)*)");
				final Matcher matcher = pattern.matcher(commit.message);
				if(!matcher.matches()) {
					invalid += 1;
					Logger.info("        [JAIM-SERVER] APP ID : %s, Invalid message pattern for commit short id: %s", app.app_id, commit.id);
					Logger.info("        [JAIM-SERVER] APP ID : %s, Message = %s", app.app_id, commit.message);
					continue;
				}
				final String currentAppVersion = matcher.group(2);
				final String commitDate = matcher.group(3);
				final String commitLastVersion = matcher.group(4);
				final String logs = matcher.group(5).trim();
				if(!logs.contains("#")) {
					continue;
				}
				if(!(app.app_id.endsWith(currentAppVersion) ||
						app.app_id.endsWith(currentAppVersion+"-dev"))){
					Logger.info("[JAIM-SERVER] Changelogs is for another version %s <> %s", app.app_id, currentAppVersion);
					continue;
				}
				String description = "<ul>";
				StringBuilder buildList = new StringBuilder(description);
				for (String s : logs.split("#")) {
					if(!StringUtils.isEmpty(s)) {
						buildList.append("<li>").append(s).append("</li>");
					}
				}
				description = buildList.append("</ul>").toString();

				Changelog changelog = new Changelog();
				changelog.app_id = app.app_id;
				changelog.date = sdf.parse(commitDate);
				changelog.last_version = Integer.parseInt(commitLastVersion);
				changelog.description = description;
				changelog.commit_id = commit.id;
				changelog.save();
			} catch (Exception e){
				Logger.error("[JAIM] %s", e.getMessage());
				continue;
			}
		}
		Logger.info("[JAIM-SERVER] Saving Finish with %s invalid message pattern..", invalid);
	}
}
