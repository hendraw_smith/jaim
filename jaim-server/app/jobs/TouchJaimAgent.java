package jobs;

import models.main.Lpse;
import models.main.ServicePack;
import models.main.ServicePack.ServicePackStatus;
import play.Logger;
import play.jobs.Job;
import play.libs.WS;

public class TouchJaimAgent extends Job {

	private Long sp_id;
	private String version;
	public TouchJaimAgent(Long sp_id)
	{
		this.sp_id=sp_id;
	}

	public TouchJaimAgent(Long sp_id, String version)
	{
		this.sp_id = sp_id;
		this.version = version;
	}
	
	public void doJob()
	{
		ServicePack sp=ServicePack.findById(sp_id);
		if(sp==null)
			return;
		Lpse lpse=Lpse.findById(sp.lpse_id);
		String url=String.format("%s/jaim.JaimCtr/touch?sp_id=%s&version=%s&db=%s", lpse.lpse_url, sp.sp_id, version, sp.do_backup);
		try {
			WS.HttpResponse response = WS.url(url).get();
			Logger.info("[TouchJaimAgent] %s, %s", url, response.getStatusText());
			if(response.success()){
				sp.status = ServicePackStatus.TOUCH;
			} else {
				sp.status = ServicePackStatus.TOUCH_FAILED;
			}
		} catch (Exception e){
			Logger.info("[TouchJaimAgent] %s, FAILED", url);
			sp.status = ServicePackStatus.TOUCH_FAILED;
		}
		sp.save();

	}
}
