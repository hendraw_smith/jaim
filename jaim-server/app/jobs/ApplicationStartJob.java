package jobs;

import models.main.Application;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.Play;
import play.jobs.Job;
import play.jobs.On;
import play.jobs.OnApplicationStart;

import java.io.IOException;

//Sync karena harus tunggu server membaca metadata
@OnApplicationStart
@On("0 0 23 * * ?")
public class ApplicationStartJob extends Job {

	public void doJob() throws IOException {
		StopWatch sw = new StopWatch();
		sw.start();
		if(Play.mode.isProd())
		{
			Logger.info("Application is starting ... preparing metadata and pull repository");
			Application.refreshRepositoryAndSaveMetadataIntoDB();
		}
		sw.stop();
		Logger.info("Application is ready. Startup duration: %s", sw);
	}

}