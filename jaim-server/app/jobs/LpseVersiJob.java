package jobs;

import models.main.Lpse;
import play.Logger;
import play.jobs.Job;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

/**Job untuk dapatkan versi LPSE
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
public class LpseVersiJob extends Job<Boolean> {

	private Lpse lpse;
	private Queue<Lpse> queueLpse;
	private static AtomicBoolean isRunning=new AtomicBoolean(false);
	public LpseVersiJob(Lpse lpse) {
		this.lpse=lpse;
	}
	
	private LpseVersiJob(Queue<Lpse> queueLpse) {
		this.queueLpse=queueLpse;
		this.lpse=queueLpse.poll();
	}
	
	//dapatkan versi untuk semua LPSE
	public static void startJob() {
		if(isRunning.getAndSet(true)){
			return;
		}
		Queue<Lpse> list=new LinkedList<>(Lpse.find("repo_id IS NOT NULL").fetch());
		new LpseVersiJob(list).now();
	}

	public static void startJobForNewLpse() {
		if(isRunning.getAndSet(true)){
			return;
		}
		Queue<Lpse> list=new LinkedList<>(Lpse.find("spse4_version IS NULL").fetch());
		new LpseVersiJob(list).now();
	}
	
	public void onException(Throwable t) {
		Logger.debug("[LpseVersiJob] ERROR: %s, %s", lpse.lpse_url, t);
		new LpseVersiJob(queueLpse).now(); //next
	}
	
	public Boolean doJobWithResult() {
		if(lpse == null) {
			Logger.debug("[LpseVersiJob] DONE");
			isRunning.set(false);
			return false;
		}

		boolean success = lpse.refreshVersiAndSave();
		if(!Objects.isNull(queueLpse) && !queueLpse.isEmpty()) {
			new LpseVersiJob(queueLpse).now();
		} else {
			isRunning.set(false);
		}

		return success;
	}
}
