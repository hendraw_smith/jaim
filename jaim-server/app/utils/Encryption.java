package utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by dadang on 7/3/18.
 */
public class Encryption {

	private static final String DCEKey = "Y95YrM1551CpRb1w";

	public static String encrypt(byte[] bytes) {
		try {
			SecretKey key = new SecretKeySpec(DCEKey.getBytes(), "AES");
			Cipher ecipher = Cipher.getInstance("AES");
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] utf8 = bytes;
			byte[] enc = ecipher.doFinal(utf8);
			String result = Base64.encodeBase64String(enc);

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String decrypt(String str) {
		try {
			SecretKey key = new SecretKeySpec(DCEKey.getBytes(), "AES");
			Cipher dcipher = Cipher.getInstance("AES");
			dcipher.init(Cipher.DECRYPT_MODE, key);
			byte[] dec = Base64.decodeBase64(str.getBytes("UTF-8"));
			byte[] utf8 = dcipher.doFinal(dec);
			String result = new String(utf8, "UTF-8");

			return result;
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}
}
