package utils;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * Created by dadang on 7/4/18.
 */
public class UUIDGenerator {

	/**
	 * UUID tipe 3
	 * @param namespace
	 * @param name
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static UUID generate(String namespace, String name) throws UnsupportedEncodingException {
		String source = namespace + name;
		byte[] bytes = source.getBytes("UTF-8");
		UUID uuid = UUID.nameUUIDFromBytes(bytes);
		return uuid;
	}

	public static String generateString(String namespace, String name) throws UnsupportedEncodingException {
		UUID uuid = generate(namespace, name);
		return uuid.toString();
	}
}
