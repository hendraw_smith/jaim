-- delete constraint service_pack --
ALTER TABLE service_pack DROP CONSTRAINT service_pack_pkey cascade;

-- tambah kolom ID --
ALTER TABLE service_pack ADD COLUMN id serial NOT NULL;

-- Rename lpse_id ke repo_id --
ALTER TABLE service_pack RENAME COLUMN sp_id TO sp_id_old;

-- rename kolom ID ke lpse_id --
ALTER TABLE service_pack RENAME COLUMN id TO sp_id;

-- jadikan lpse_id primary key
ALTER TABLE service_pack ADD PRIMARY KEY (sp_id);

-- CHANGE "NULLABLE" OF "FIELD "sp_id_old"
ALTER TABLE service_pack ALTER COLUMN sp_id_old DROP NOT NULL;