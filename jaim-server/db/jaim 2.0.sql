ALTER TABLE "public"."changelog" ADD COLUMN "commit_id" varchar(255);

ALTER TABLE "public"."service_pack" ADD COLUMN "do_backup" bool DEFAULT true NOT NULL;;

CREATE TABLE "public"."lpse_custom_conf" (
  "id" int4 NOT NULL,
  "key" varchar(255),
  "value" varchar(255),
  "lpse_id" int4,
  "created_by_id" int4,
  "create_date" timestamp,
  PRIMARY KEY ("id"),
  CONSTRAINT "lpse_custom_conf_lpse_id_lpse_lpse_id" FOREIGN KEY ("lpse_id") REFERENCES "public"."lpse" ("lpse_id") ON DELETE CASCADE ON UPDATE CASCADE
)
;

COMMENT ON COLUMN "public"."lpse_custom_conf"."created_by_id" IS 'ID di centrum';

CREATE SEQUENCE seq_lpse_custom_conf
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "public"."lpse_custom_conf"
  ADD COLUMN "info" text;