-- CHANGE "NULLABLE" OF "FIELD "scheduled_date" ----------------
ALTER TABLE "public"."service_pack" ALTER COLUMN "scheduled_date" DROP NOT NULL;


-- CREATE FIELD "updateee" -------------------------------------
ALTER TABLE "public"."service_pack" ADD COLUMN "update_date" Timestamp Without Time Zone;
-- -------------------------------------------------------------

-- CREATE FIELD "need_migration" -------------------------------
ALTER TABLE "public"."service_pack" ADD COLUMN "need_migration" Integer;
-- -------------------------------------------------------------
