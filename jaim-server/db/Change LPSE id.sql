-- delete constraint lpse_pkey --
ALTER TABLE lpse DROP CONSTRAINT lpse_pkey cascade;

-- tambah kolom ID --
ALTER TABLE lpse ADD COLUMN id serial NOT NULL;

-- Rename lpse_id ke repo_id --
ALTER TABLE lpse RENAME COLUMN lpse_id TO repo_id;

-- rename kolom ID ke lpse_id --
ALTER TABLE lpse RENAME COLUMN id TO lpse_id;

-- jadikan lpse_id primary key
ALTER TABLE lpse ADD PRIMARY KEY (lpse_id);

-- Ganti lpse_id di service pack --
Update service_pack as sp set lpse_id = (select lpse_id from lpse as l where l.repo_id = sp.lpse_id);