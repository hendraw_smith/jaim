CREATE TABLE "public"."changelog" (
"id" numeric(19) NOT NULL,
"app_id" character varying NOT NULL,
"date" timestamp(6) DEFAULT now() NOT NULL,
"description" text COLLATE "default" NOT NULL,
"audittype" char(1) COLLATE "default" DEFAULT 'C'::bpchar NOT NULL,
"audituser" varchar(100) COLLATE "default" DEFAULT 'ADMIN'::character varying NOT NULL,
"auditupdate" timestamp(6) DEFAULT now() NOT NULL
)
WITH (OIDS=FALSE)

;

CREATE SEQUENCE seq_changelog
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;