CREATE TABLE "public"."audittrail" (
	"id" Bigint NOT NULL,
	"audit_type" Character Varying( 2044 ) NOT NULL,
	"remote_address" Character Varying( 2044 ) NOT NULL,
	"user_id" Bigint,
	"activity_date" Timestamp Without Time Zone NOT NULL,
	"data_before" Text,
	"data_after" Text,
	"info" Text,
	PRIMARY KEY ( "id" ) );


CREATE SEQUENCE audittrail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE audittrail_id_seq OWNER TO postgres;