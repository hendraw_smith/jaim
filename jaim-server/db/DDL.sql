--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

-- Started on 2016-08-09 11:57:09

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2129 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 182 (class 1259 OID 26910575)
-- Name: app; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE app (
    app_id character varying NOT NULL,
    app_name character varying
);


ALTER TABLE app OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 26910583)
-- Name: app_properties; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE app_properties (
    host_id character varying NOT NULL,
    app_id character varying NOT NULL,
    manifest text,
    version character varying
);


ALTER TABLE app_properties OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 26910599)
-- Name: app_update; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE app_update (
    id bigint NOT NULL,
    host_id character varying,
    manifest text,
    update_date time without time zone,
    duration integer
);


ALTER TABLE app_update OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 26910567)
-- Name: host; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE host (
    host_id character varying NOT NULL,
    host_name character varying,
    lpse_url character varying,
    os_name character varying,
    os_version character varying,
    java_version character varying,
    app_update_id bigint,
    lpse_id integer,
    verified boolean,
    allow_update boolean,
    last_hello timestamp without time zone,
    host_info character varying,
    lpse_nama character varying
);


ALTER TABLE host OWNER TO postgres;

--
-- TOC entry 2119 (class 0 OID 26910575)
-- Dependencies: 182
-- Data for Name: app; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app (app_id, app_name) FROM stdin;
\.


--
-- TOC entry 2120 (class 0 OID 26910583)
-- Dependencies: 183
-- Data for Name: app_properties; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_properties (host_id, app_id, manifest, version) FROM stdin;
\.


--
-- TOC entry 2121 (class 0 OID 26910599)
-- Dependencies: 184
-- Data for Name: app_update; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_update (id, host_id, manifest, update_date, duration) FROM stdin;
\.


--
-- TOC entry 2118 (class 0 OID 26910567)
-- Dependencies: 181
-- Data for Name: host; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY host (host_id, host_name, lpse_url, os_name, os_version, java_version, app_update_id, lpse_id, verified, allow_update, last_hello, host_info, lpse_nama) FROM stdin;
\.


--
-- TOC entry 1998 (class 2606 OID 26910582)
-- Name: pk_app_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app
    ADD CONSTRAINT pk_app_id PRIMARY KEY (app_id);


--
-- TOC entry 2000 (class 2606 OID 26910590)
-- Name: pk_app_properties; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_properties
    ADD CONSTRAINT pk_app_properties PRIMARY KEY (host_id, app_id);


--
-- TOC entry 2002 (class 2606 OID 26910606)
-- Name: pk_app_update; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_update
    ADD CONSTRAINT pk_app_update PRIMARY KEY (id);


--
-- TOC entry 1996 (class 2606 OID 26910608)
-- Name: pk_host; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY host
    ADD CONSTRAINT pk_host PRIMARY KEY (host_id);


--
-- TOC entry 2003 (class 2606 OID 26910616)
-- Name: fk_app_update_host; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_update
    ADD CONSTRAINT fk_app_update_host FOREIGN KEY (host_id) REFERENCES host(host_id);


--
-- TOC entry 2128 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-08-09 11:57:09

--
-- PostgreSQL database dump complete
--

