drop table penerimaan;
create table penerimaan(
id bigserial primary key,
app_id varchar,
build_date timestamp without time zone,
lpse_id int,
notifydate timestamp without time zone, 
acceptance_status varchar,
acceptance_date timestamp without time zone 
);