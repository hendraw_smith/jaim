/**
 * 
 */
package dbstructure;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import jaim.common.dbstructure.DBStructure;
import play.Logger;
import play.test.UnitTest;

/**
 * @author AndikYulianto@yahoo.com
 *
 */
public class DBStructureCompareTest extends UnitTest {

	@Test
	public void doTest() throws IOException
	{
		DBStructure target=DBStructure.getInstance(new File("tmp/000/DBStructure.json"));
		DBStructure master=DBStructure.getInstance(new File("test/dbstructure/spse-4.x.base.json"));
//		DBStructure master=DBStructure.getInstance(new File("tmp/000/DBStructure.json"));
		target.compareDB(master.tables);
		FileUtils.writeLines(new File("tmp/000/ddl1.sql"), target.ddlCommands);
		FileUtils.writeLines(new File("tmp/000/ddl2.sql"), target.ddlForeignKey);
		
		Logger.debug("[DBStructureCompareTest] DONE");
	}
}
