package agent;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import models.main.ServicePack;
import models.main.ServicePack.ServicePackStatus;
import play.Logger;
import play.libs.WS;
import play.test.UnitTest;

public class ServicePackTest extends UnitTest {

//	@Test
	public void doTouch()
	{
		Date scheduled_date=DateUtils.addSeconds(new Date(), 45);
		ServicePack sp=ServicePack.newServicePack(0, "spse-prod-4.1.2", scheduled_date);
		sp.status=ServicePackStatus.TOUCH;
		sp.save();
		String lpse_url="http://192.168.43.10:9000/spse4";
		String url=String.format("%s/jaim/%s", lpse_url, sp.sp_id);
		Logger.debug("[ServicePackTest] %s, %s", url, WS.url(url).get().getStatusText());
	}
	
	@Test
	public void doGetStatus()
	{
		String lpse_url="http://localhost:9000/spse4";
		String fileName="0003000s1";
		String url=String.format("%s/jaim.JaimCtr/status?sp_id=%s", lpse_url, fileName);
		Logger.debug("[ServicePackTest] %s, %s", url, WS.url(url).get().getStatusText());
	}
}
