package jaim.common.dbstructure;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.annotations.Expose;

import play.Logger;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

public class PgTable implements Comparable<PgTable> {

	public enum TableType
	{
		TABLE, SEQUENCE, VIEW;
	}
	
	@Expose
	public String tableName;
	@Expose
	public TableType tableType; //bisa TABLE atau SEQUENCE
	@Expose
	public java.util.List<String> primaryKeyColumns=new ArrayList();
	@Expose
	public String primaryKeyName;
	@Expose
	public Set<PgForeignKey> foreignKeys=new HashSet<>();
	@Expose
	public Map<String, PgColumn> columns=new HashMap();
	
	public List<PgIndex> indexes=new ArrayList<>();

	private  DatabaseMetaData md;
	
	@Override
	public int compareTo(PgTable o) {
		return tableName.compareTo(o.tableName);
	}

	public PgTable(String schema, String tableName, String tableType, DatabaseMetaData md) {
		this.tableName = schema + "." + tableName;
		this.tableType=TableType.valueOf(tableType);
		this.md=md;
		
		try {
			// dapatkan informasi kolom
			ResultSet rs = md.getColumns(null, null, tableName, null);
			int colIndex=1;
			while (rs.next()) {
				//public PgColumn(String columnName, String typeName, Integer columnSize, Integer decimalDigits, String nullable,				String columnDef) 
				PgColumn column=new PgColumn(rs.getString("COLUMN_NAME"), 
						rs.getString("TYPE_NAME"), rs.getInt("COLUMN_SIZE"), 
						rs.getInt("DECIMAL_DIGITS"), rs.getInt("NULLABLE")==ResultSetMetaData.columnNullable ? "NULL" : "NOT NULL", 
						rs.getString("COLUMN_DEF"));
				/* Kadang2 columnSize tidak akurat Untuk numeric perlu dibatasi 64 */
				if(column.typeName.equals("numeric") && column.columnSize > 1000)
					column.columnSize=64;
				//dapatkan checkcontraint
				getCheckConstraint(colIndex, column);
				
				columns.put(column.columnName, column);
			
				colIndex++;
			}
			rs.close();			
			// dapatkan informasi Foreign key
			if (isTable()) {
				rs=md.getImportedKeys(null, null, tableName);
				Map<String, PgForeignKey> mapFk=new HashMap<>();
				while(rs.next())
				{
					String pkTable=rs.getString("PKTABLE_NAME");
					String refColumn=rs.getString("FKCOLUMN_NAME");
					String pkColumn=rs.getString("PKCOLUMN_NAME");
					String pKschema=rs.getString("PKTABLE_SCHEM");
					String fkName=rs.getString("FK_NAME");
					PgForeignKey fk=mapFk.get(fkName);
					if(fk==null)
					{
						fk=new PgForeignKey(fkName, pKschema + "." + pkTable, refColumn, pkColumn);
						mapFk.put(fkName, fk);
					}
					else
						fk.addColumn(refColumn, pkColumn);
				}
				rs.close();
				foreignKeys=new HashSet<>(mapFk.values());
			}
			// dapatkan informasi primary key dan index
			rs = md.getPrimaryKeys(null, null, tableName);
			while(rs.next())
			{
				primaryKeyColumns.add(rs.getString("COLUMN_NAME"));
				primaryKeyName=rs.getString("PK_NAME");//nama constraint untuk PK
			}
			rs.close();
			// dapatkan informasi index
			rs=md.getIndexInfo(null, null, tableName, false, false);
			Map<String, PgIndex> mapIndex=new HashMap<>();
			while(rs.next())
			{
				String indexName=rs.getString("INDEX_NAME"); //maybe null
				if(indexName !=null && primaryKeyName!=null && !primaryKeyName.equals(indexName)) //index ini bukan PK jadi tambahkan 
				{
					PgIndex index=mapIndex.get(indexName);
					boolean nonUnique=rs.getBoolean("NON_UNIQUE");
					if(index==null)
					{
						index=new PgIndex();
						mapIndex.put(indexName, index);
						index.name=indexName;
						if(!nonUnique)
							index.unique="UNIQUE";
						else
							index.unique="";
					}
					String column=rs.getString("COLUMN_NAME");
					String ascOrDesc=rs.getString("ASC_OR_DESC");
					if(ascOrDesc.equals("D"))
						ascOrDesc=" DESC";
					else
						ascOrDesc=" ASC";
					index.columns.add(column + ascOrDesc);
				}
			}		
			indexes.addAll(mapIndex.values());
			rs.close();
		} catch (SQLException e) {
			Logger.error(e, "Error saat dapatkan informasi %s", tableName);
		}
	}
	
	private void getCheckConstraint(int columnIndex, PgColumn column) throws SQLException
	{
		String sql="SELECT conname, pg_get_constraintdef(c.oid) "
				+ "FROM pg_constraint c "
				+ "JOIN pg_namespace n ON n.oid = c.connamespace  "
				+ "WHERE contype in ('c') AND ? = ANY (conkey) AND conrelid::regclass::text=?";
		PreparedStatement ps=md.getConnection().prepareStatement(sql);
		ps.setInt(1, columnIndex);
		ps.setString(2, tableName);
		ResultSet rs=ps.executeQuery();
		if(rs.next())
		{
			column.checkContraintName= rs.getString(1);
			column.checkContraintDef= rs.getString(2);
		}
		rs.close();
	}
	
	public boolean isTable()
	{
		return TableType.TABLE.equals(tableType);
	}	
	
	public String toString()
	{
		return tableName +"/" + tableType;
	}
	
	public String getPrimaryKey() {
		StringBuilder str=new StringBuilder();
		str.append(primaryKeyName).
			append(" PRIMARY KEY ").append('(');
		for(int i=0;i<primaryKeyColumns.size();i++)
		{
			if(i>0)
				str.append(',');
			str.append(primaryKeyColumns.get(i));			
		}		
		str.append(')');
		return str.toString();
	}

	public void compareTable(PgTable tableMaster, List<String> ddlCommands) {
		
		//#1 compare columns
		for(String columnNameMaster: tableMaster.columns.keySet())
		{
			//new column
			PgColumn columnTarget= columns.get(columnNameMaster);
			PgColumn columnMaster=tableMaster.columns.get(columnNameMaster);
			if(columnTarget==null)
			{
				//create DDL
				StringBuilder str=new StringBuilder();
				str.append("\nALTER TABLE ").append(tableMaster.tableName)
					.append(" ADD COLUMN ").append(columnMaster.toString())
					.append(";");
				ddlCommands.add(str.toString());
			}
			else
			{
				
				//Dicek atas kesamaan NAMA dan TYPE
				if(!columnMaster.equalsNameAndType(columnTarget))
				{
					//type
					StringBuilder str=new StringBuilder();
					str.append("\nALTER TABLE ").append(tableMaster.tableName)
					.append(" ALTER COLUMN ").append(columnMaster.columnName)
					.append(" TYPE ").append(columnMaster.getDataType())
					.append(";")
					.append(" /* PREVIOUS: ").append(columnTarget.toString()).append(" */");
					ddlCommands.add(str.toString());
				}
				
				/* **************** NotNull-ness *****************
				#1 columnMaster is NULL but columnTarget NOT NULL
				*/
				if(columnTarget.nullable.equals("NOT NULL") &&
						columnMaster.nullable.equals("NULL"))
				{
					StringBuilder str=new StringBuilder();
					str.append("\nALTER TABLE ").append(tableMaster.tableName)
						.append(" ALTER COLUMN ").append(columnMaster.columnName)
						.append(" DROP NOT NULL;");
					ddlCommands.add(str.toString());
				}
				//#2 master is NOT-NULL but target NULL
				else
				{
					if(columnMaster.nullable.equals("NOT NULL") && columnTarget.nullable.equals("NULL") )
					{
						StringBuilder str=new StringBuilder();
						str=new StringBuilder();
						str.append("\nALTER TABLE ").append(tableMaster.tableName)
							.append(" ALTER COLUMN ").append(columnMaster.columnName)
							.append(" SET NOT NULL;");
						ddlCommands.add(str.toString());
					}
				}
				
				
				/*******************************DEFAULT  ************************* /
				 */
				//1 terkait default: jika ada DEFAULT di versi 3 dan tidak ada di SPSE 4 maka DROP
				if(columnTarget.hasDefault() && !columnMaster.hasDefault())
				{
					String str=String.format("\nALTER TABLE %s ALTER COLUMN %s DROP DEFAULT;", columnMaster.columnName, tableMaster.tableName);
					ddlCommands.add(str);
				}
				
				//2 terkait default: jika ada DEFAULT di versi 4 dan tidak ada di SPSE 3 maka CREATE DEFAULT
				if(columnMaster.hasDefault() && !columnMaster.columnDef.equals(columnTarget.columnDef))
				{
					String str=String.format("\nALTER TABLE %s ALTER COLUMN %s SET DEFAULT %s;", 
							columnMaster.columnName, tableMaster.tableName, columnMaster.columnDef);
					ddlCommands.add(str);
				}
			}
		}
		
		//#2 compare PK 
		if(!primaryKeyColumns.equals(tableMaster.primaryKeyColumns))
		{
			if(primaryKeyName!=null)
			{
				StringBuilder str=new StringBuilder();
				str.append("\nALTER TABLE ").append(tableMaster.tableName)
					.append(" DROP CONSTRAINT ")
					.append(primaryKeyName).append(";");
				ddlCommands.add(str.toString());
			}
			
			 StringBuilder str=new StringBuilder();
			str.append("\nALTER TABLE ").append(tableMaster.tableName)
				.append(" ADD CONSTRAINT ").append(tableMaster.primaryKeyName)
				.append(" PRIMARY KEY ")
				.append('(')
				.append(StringUtils.join(tableMaster.primaryKeyColumns, ','))
				.append(");");
			//comment
			str.append("/* PREVIOUS: ALTER PRIMARY KEY from: '")
				.append(getPrimaryKey())
				.append("' */");
			ddlCommands.add(str.toString());
		}
		
		//#3 compare FK dilakukan terpisah
		
		
		//#4 compare indexes
		checkIndexes(ddlCommands, tableMaster);
		
	}
	
	/**Check apakah ada index yang harus didrop & dicreate ?
	 * 
	 * @param str
	 * @param tableMaster
	 */
	private void checkIndexes(List<String> ddlCommands, PgTable tableMaster) {
		StringBuilder str=new StringBuilder();
		//#1: check index yg harus di-drop
		if(indexes!=null)
			for(PgIndex pgDropIndex: indexes)
				if(!tableMaster.indexes.contains(pgDropIndex))
					str.append("\nDROP INDEX ")
						.append(pgDropIndex.name)
						.append(";");
		
		//#2: check index yg harus di-create
		if(tableMaster.indexes!=null)
			for(PgIndex pgNewIndex: tableMaster.indexes)
				if(indexes!=null && !indexes.contains(pgNewIndex))
					getDDLCreateIndex(str, pgNewIndex);
		if(str.length()>0)
			ddlCommands.add(str.toString());
	}

	/**Check foreign key dengan beberapa kondisi
	 * 
	 * @param str tempat meyimpan DDL
	 * @param pgTable4
	 * @param ddlCommands 
	 */
	public void getDDLAlterForeignKey(PgTable pgTable4, List<String> ddlCommands) {
		
		//Kondisi 1: apakah ada foreign key yg harus di-DROP?
		for(PgForeignKey pgDropFK: foreignKeys)
			if(!pgTable4.foreignKeys.contains(pgDropFK))
			{
				StringBuilder str=new StringBuilder();
				str.append("\nALTER TABLE ").append(tableName)
					.append(" DROP CONSTRAINT ").append(pgDropFK.name)
					.append(";");
				ddlCommands.add(str.toString());
			}

		//kondisi 2: apakah ada foreign key baru?
		for(PgForeignKey pgNewPk: pgTable4.foreignKeys)
		{
			if(!foreignKeys.contains(pgNewPk))
			{
				StringBuilder str=new StringBuilder();
				str.append("\nALTER TABLE ").append(tableName)
				.append(" ADD CONSTRAINT ")
				.append(" ").append(pgNewPk.toString())
				.append(";");
				ddlCommands.add(str.toString());
			}
		}
	}

	public void getDDLCreateSequence(List<String> ddlCommands) {
		//if sequence is not exists then create
		StringBuilder str=new StringBuilder();
		str.append("CREATE SEQUENCE ")
			.append(tableName)
			.append(" INCREMENT 1 MINVALUE 1 CACHE 1;");
		ddlCommands.add(str.toString());
	}

	/**get DDL to Create Table except Foreign key
	 * 
	 * @return
	 */
	public void getDDLCreateTable(List<String> ddlCommands)
	{
		StringBuilder str=new StringBuilder();
		str.append("\n\nCREATE TABLE ").append(tableName).append("\n(");
		//1 Create columns
		int i=0;
		for(PgColumn pgColumn: columns.values())
		{
			if(i>0)
				str.append("\n,");
			str.append(pgColumn.toString());
			i++;
		}
		
		str.append(");");
		ddlCommands.add(str.toString());
		
		//2 Create PK
		str=new StringBuilder();
		if(primaryKeyColumns.size()>0)
		{
			str.append("\nALTER TABLE ").append(tableName)
				.append(" ADD CONSTRAINT ").append(getPrimaryKey())
				.append(";");
		}
		ddlCommands.add(str.toString());
		
		//3 Create FK tidak dijalankan di sini 
			
		//4 Create Index
		for(PgIndex index: indexes)
		{
			str=new StringBuilder();
			getDDLCreateIndex(str, index);
			ddlCommands.add(str.toString());
		}
		
	}
	
	/**Untuk CreateForeign key harus dijalankan terakhir
	 * 
	 * @return
	 */
	public String getDDLCreateForeignKey()
	{
		StringBuilder str=new StringBuilder();
		for(PgForeignKey pgNewPk: foreignKeys)
		{
			if(!foreignKeys.contains(pgNewPk))
			{
				str.append("\nALTER TABLE ").append(tableName)
				.append(" ADD CONSTRAINT ")
				.append(" ").append(pgNewPk.toString())
				.append(";");
			}
		}
		return str.toString();
	}

	private void getDDLCreateIndex(StringBuilder str, PgIndex index) {
		str.append("\nCREATE ")
		.append(index.unique)
		.append(" INDEX ")
		.append(index.name)
		.append(" ON ")
		.append(tableName)
		.append(" (")
		.append(StringUtils.join(index.columns,','))
		.append(");");		
	}

	/**Dapatkan nama tabel dg ketentuan
	 *  jika skema public maka buang nama schema
	 * @return
	 */
	public String getSimpleTableName() {
		return tableName.replace("public.", "");
	}
}
