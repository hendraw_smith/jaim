/**
 * 
 */
package jaim.common.dbstructure;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.StopWatch;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;

import jaim.common.dbstructure.PgTable.TableType;
import play.Logger;
import play.db.DB;

/**
 * @author AndikYulianto@yahoo.com
 *
 */
public class DBStructure {

	@Expose
	public Map<String,PgTable> tables;
	
	public List<String> ddlCommands=new ArrayList();
	public List<String> ddlForeignKey=new ArrayList<>();
	
	public void reverseEngineer()
	{
		StopWatch sw=new StopWatch();
		sw.start();
		try {
			Connection conSpse3=DB.getConnection();			
			tables=new HashMap<>();

			DatabaseMetaData md= conSpse3.getMetaData();
			//Step 1, get tables, views, sequence
			String[] schemas={"ekontrak","public"};
			for(String schema: schemas)
			{
				ResultSet rs=md.getTables(null, schema, null, new String[]{"TABLE", "SEQUENCE","VIEW"});
				while(rs.next())
				{
					String tableName=rs.getString("TABLE_NAME");
					String tableType=rs.getString("TABLE_TYPE");
					PgTable pgTable=new PgTable(rs.getString("TABLE_SCHEM"),
							tableName, tableType, md);			
					tables.put(pgTable.tableName, pgTable);
				}
				rs.close();
				
			}
			

		} catch (Exception e) {
			Logger.error(e, "[ddlView.Database] error: %s", e);
		}
		sw.stop();
		Logger.debug("[DatabaseStructure] generate, duration: %s", sw);
	}
	
	public String getJson()
	{
		return new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
				.create().toJson(tables);
	}
	
	/**Dapatkan instance dari DBStructure berdasarkan  json
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	public static DBStructure getInstance(File file) throws IOException
	{
		String json=FileUtils.readFileToString(file, "UTF-8");
		Type type=new TypeToken<Map<String,PgTable>>(){}.getType();
		DBStructure db=new DBStructure(); 
		db.tables= new Gson().fromJson(json, type);
		return db;
	}
	
	public void compareDB(Map<String,PgTable> masterTables) {
		
		Map<String,PgTable> targetTables=tables;
		
		/**Table target selalu diawali nama schema, termasuk 'public'
		 */
		for(PgTable masterTable: masterTables.values())
		{
			String tableName=masterTable.tableName;
			if(!tableName.contains("."))
				tableName="public." + tableName;
			PgTable targetTable= targetTables.get(tableName);
			//tidak ada di target
			if(targetTable==null)
			{
				if(masterTable.tableType.equals(TableType.TABLE))
				{
					masterTable.getDDLCreateTable(ddlCommands);
					String str=masterTable.getDDLCreateForeignKey();
					if(!str.isEmpty())
					{
						ddlCommands.add(str);
						Logger.debug("[compareDB] CREATE TABLE %s",  tableName);

					}
				}
				else
					masterTable.getDDLCreateSequence(ddlCommands);
			}
			//ada di target tapi mungkin beda struktur
			else
			{
				//compare table DDL; Tidak untuk sequence
				if(masterTable.tableType.equals(TableType.TABLE))
				{
					targetTable.compareTable(masterTable, ddlCommands); 
					targetTable.getDDLAlterForeignKey(masterTable, ddlForeignKey); //FK dimasukkan ke list DDL terpisah
					Logger.debug("[compareDB] ALETER TABLE %s",  tableName);

				}
			}
		}
	}
	
	
	
}
