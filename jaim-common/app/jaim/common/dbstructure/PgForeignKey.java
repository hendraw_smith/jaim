package jaim.common.dbstructure;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**Equality dari FK TIDAK ditentukan oleh 'name'
 * tapi ditentukan oleh 'columns' karena bisa berbeda nama
 * 
 * @author Mr. Andik
 *
 */
public class PgForeignKey  {
	
	@Expose
	public String name;
	@Expose
	public String tableName;
	@Expose
	public List<String> columns=new ArrayList();
	@Expose
	public List<String> pkColumns=new ArrayList(); //column in PK (master table)
	
	public PgForeignKey(String name, String tableName, String column, String pkColumn)
	{
		this.name=name;
		this.tableName=tableName;
		addColumn(column, pkColumn);
	}
	
	public void addColumn(String column, String pkColumn)
	{
		columns.add(column);
		pkColumns.add(pkColumn);
	}
	
	public String toString()
	{
		return String.format(" %s FOREIGN KEY (%s) REFERENCES %s (%s)", name, StringUtils.join(columns, ','),
				tableName, StringUtils.join(pkColumns, ','));
	}


	
	public boolean equals(Object o)
	{
		PgForeignKey key=(PgForeignKey)o;
		return columns.equals(key.columns) && pkColumns.equals(key.pkColumns);
	}
	
	public int hashCode()
	{
		return (tableName +  StringUtils.join(columns, ',')).hashCode();
	}
	
	
}
