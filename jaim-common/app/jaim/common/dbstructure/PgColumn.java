package jaim.common.dbstructure;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.google.gson.annotations.Expose;

public class PgColumn implements Comparable<PgColumn>{
	public PgColumn(String columnName, String typeName, Integer columnSize, Integer decimalDigits, String nullable,
			String columnDef) {
		super();
		this.columnName = columnName;
		this.typeName = typeName;
		this.nullable = nullable;
		this.columnDef = columnDef;
		checkContraintName="";
		checkContraintDef="";
		if(isNeedSize())
		{
			this.columnSize = columnSize;
			this.decimalDigits = decimalDigits;
		}
		
	}

	@Expose
	public String columnName;
	@Expose
	public String typeName; //see DDL data type
	@Expose
	public Integer columnSize;
	@Expose
	public Integer decimalDigits;
	@Expose
	public String nullable;
	@Expose
	public String columnDef;
	@Expose
	public String checkContraintName;
	@Expose
	public String checkContraintDef;
		
	public String toString()
	{
		StringBuilder str=new StringBuilder();
		str.append(columnName).append(' ');
	
		str.append(getDataType());
		str.append(' ');
		str.append(nullable);
		str.append(' ');
		str.append(checkContraintName==null ? "": checkContraintName);
		str.append(checkContraintDef==null ? "":checkContraintDef);
		if(columnDef!=null)
			str.append(" DEFAULT ").append(columnDef);
		return str.toString();
	}

	@Override
	public int compareTo(PgColumn o) {
		return columnName.compareTo(o.columnName);
	}
	
	public String getDataType()
	{
		StringBuilder str=new StringBuilder();
		str.append(typeName);
		if(isNeedSize())
		{
				str.append('(').append(columnSize);
			if(decimalDigits>0)
				str.append(',').append(decimalDigits);
			str.append(')');
		}
		
		return str.toString();
	}
	
	private boolean isNeedSize()
	{
		switch (typeName) {
		//yang ini memerlukan ukuran
		case "varchar":
		case "char":
		case "bpchar":
		case "numeric":
			return true;
		}
		return false;
	}
	
	/**Check if equals AND and TYPE ONLY
	 * 	-> DEFAULT && NULL maybe not equals
	 * @param column
	 * @return
	 */
	public boolean equalsNameAndType(PgColumn column)
	{
		boolean sizeEquals=(columnSize==null && column.columnSize ==null) || columnSize.equals(column.columnSize);
		return columnName.equals(column.columnName) && typeName.equals(column.typeName) && sizeEquals;
	}
	
	public boolean hasDefault()
	{
		return columnDef!=null && columnDef.contains("DEFAULT");
	}

}
