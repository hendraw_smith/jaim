package jaim.common.dbstructure;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class PgIndex {
	@Expose
	public String name;
	@Expose
	public String unique; //
	@Expose
	public List<String> columns=new ArrayList<>(); //column with ASC or DESC flag -> pkt_nama DESC, lls_id ASC
	
	public String toString()
	{
		if(unique==null)
			unique="";
		return String.format("%s %s (%s)",  name, unique, StringUtils.join(columns, ',') );
	}
	
	public boolean equals(Object o)
	{
		PgIndex idx=(PgIndex)o;
		return toString().equals(idx.toString());
	}
	
	public int hashCode()
	{
		return toString().hashCode();
	}
}
