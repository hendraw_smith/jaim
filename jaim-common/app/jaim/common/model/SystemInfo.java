package jaim.common.model;

import com.google.gson.annotations.SerializedName;

public class SystemInfo {
	public String zip;
	public String jvm;
	@SerializedName("Free Temp Storage")
	public TmpStorage tmpStorage;
	public String curl;
	@SerializedName("System Start")
	public String systemStart;
	@SerializedName("Java Total Memory*")
	public String javaTotalMemory;
	@SerializedName("PostgreSQL")
	public String postgreSQL;
	@SerializedName("Java Max Memory*")
	public String javaMaxMemory;
	@SerializedName("Free File Storage")
	public FreeFileStorage freeFileStorage;
	@SerializedName("JDK")
	public String jdk;
	@SerializedName("Java Free Memory")
	public String javaFreeMemory;
	@SerializedName("Database Tablespace Size (pg_default)")
	public String dbTablespaceSize;
	public String pg_dump;
	@SerializedName("Core")
	public String core;

	private class TmpStorage {
		public Long freeSpace;
		public Long totalSpace;
		public Integer percentFree;
	}

	private class FreeFileStorage {
		public Long freeSpace;
		public Long totalSpace;
		public Integer percentFree;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("PostgreSQL: ").append(postgreSQL).append(" ::: ");
		sb.append("Pg_dump: ").append(pg_dump).append(" ::: ");
		sb.append("ZIP: ").append(zip).append(" ::: ");
		sb.append("JVM: ").append(jvm).append(" ::: ");
		sb.append("Curl: ").append(curl).append(" ::: ");
		sb.append("JDK: ").append(jdk).append(" ::: ");
		sb.append("Core: ").append(core);
		return sb.toString();
	}
}
