package jaim.common.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 6/4/2018
 */
public class ResponseCheck {

    @SerializedName("needs_update")
    public boolean needUpdate;
    @SerializedName("sp_id")
    public Long spId;
    @SerializedName("changelogs")
    public ChangelogItem[] changelogs;
    @SerializedName("spse_version")
    public String spseVersion;

}
