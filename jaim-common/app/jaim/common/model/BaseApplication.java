package jaim.common.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.*;
/**
 * Dipakai di Agent maupun Server
 * Untuk agent, tidak perlu simpan ke DB
 * @author User
 */
@MappedSuperclass
public class BaseApplication extends BaseTable {

	static final long serialVersionUID = 4099932L;
	public String root;

	public String name;
	
	public BaseApplication(String app_id, String root) {
		this.app_id = app_id;
		this.name = app_id;
		this.root = root;
	}
	
	@Transient
	public Map<String, FileAttribute> fileAttributes=new HashMap<>();
	
	public String getPath()
	{
		return String.format("%s/%s", root, app_id);
	}
	

	/**
	 * Path to ignore. Hanya untuk Jaim Server
	 * Pengecekan dilakukan dengan String.startsWith() untuk pattern yg diakhiri tanda *
	 * dan dicek dengan equalsIgnoreCase() untuk selainnya
	 */
	@Transient
	@Deprecated //ignored file didefinisikan di {JaimServer}/conf/ignore.jaim
	public List<String> ignored;
	
	@Transient
	public boolean active;
	@Transient
	public Date lastModified;
	/**
	 * app_id harus diawali dengan spse- dan diikuti versi spse, contoh spse-4.3
	 */
	@Id
	public String app_id;
	
	
	/**
	 * Get list of files in this App
	 * 
	 * @return
	 * @throws IOException 
	 */
	public void prepareFilesAttribute() throws IOException {

		fileAttributes=new HashMap<>();
		String rootDirStr=String.format("%s/%s", root, name);
		preparedFilesAttribute(rootDirStr);
	}
	
	public void prepareFilesAttribute(String json) {
		if(StringUtils.isEmpty(json))
			return;
		Type type=new TypeToken<Map<String, FileAttribute>>(){}.getType();
		fileAttributes=new Gson().fromJson(json, type);

	}
	
	/**
	 * Prepare attribute of files AND list if ignored files;
	 * Also remove ignored files 
	 * 
	 * @param rootDirStr
	 * @throws IOException
	 */
	private void preparedFilesAttribute(String rootDirStr) throws IOException {
		long lastModif=0;
		File ignoredFile=new File(rootDirStr + "/.ignored");
		if(ignoredFile.exists()) {
			InputStream io=new FileInputStream(ignoredFile);
			ignored=IOUtils.readLines(io, "UTF-8");
			io.close();
		}
		
		fileAttributes=new HashMap<>();
		File rootDir=new File(rootDirStr);
		if(rootDir.exists()) {
			if(!rootDir.isDirectory())
				throw new IOException(rootDir + " is not directory");
		}
		IOFileFilter filter=new IOFileFilter() {
			@Override
			public boolean accept(File pathname) {
				return (!pathname.getName().equals(".git"));
			}

			@Override
			public boolean accept(File arg0, String arg1) {
				return true;
			}
		};

		Collection<File> files=FileUtils.listFiles(rootDir, filter, filter);
		
		for(File file: files) {
			String path=FilenameUtils.separatorsToUnix(file.getPath());
			if(file.isFile() && !path.contains("/.git/")) {
				FileAttribute attr=new FileAttribute(file, rootDirStr);
				if(!isIgnored(attr)) {
					fileAttributes.put(attr.path, attr);
					if(attr.modified_date>lastModif)
						lastModif=attr.modified_date;
				}
			}
		}
		lastModified=new Date(lastModif);
	}


	/**
	 * Test if this file should be ignored
	 * 
	 * @param attr
	 * @return
	 */
	private boolean isIgnored(FileAttribute attr) {
		if(ignored==null)
			return false;
		for(String strIgnore: ignored) {
			/** wildcard /abc/ac.* 
			 * 
			 */
			if(strIgnore.endsWith("*") && attr.path.startsWith(strIgnore.replace("*", ""))){
				return true;
			} else if(strIgnore.equalsIgnoreCase(attr.path)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Sama jika punya name sama
	 * 
	 */
	public boolean equals(Object obj) {
		BaseApplication app=(BaseApplication)obj;
		return app.name.equals(app.name);
	}
	
	public int hashCode()
	{
		return  name.hashCode();
	}
	
}
