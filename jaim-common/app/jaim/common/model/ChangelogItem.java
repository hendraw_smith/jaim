package jaim.common.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author HanusaCloud on 6/5/2018
 */
public class ChangelogItem {

    @SerializedName("head")
    private final String head;
    @SerializedName("descriptions")
    private final String[] descriptions;

    public ChangelogItem(String head, String[] descriptions) {
        this.head = head;
        this.descriptions = descriptions;
    }

    public String getStandardTime() {
        if (head == null || head.isEmpty()) {
            return "-";
        }
        try {
            Date date = new SimpleDateFormat("yyyyMMdd").parse(head);
            return new SimpleDateFormat("dd-MM-yyyy").format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "-";
        }
    }

    public Date getTime() {
        if (head == null || head.isEmpty()) {
            return null;
        }
        try {
            Date date = new SimpleDateFormat("yyyyMMdd").parse(head);
            return date;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
