package jaim.common.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dadang on 9/17/18.
 */
public class ErrorMessage {

	public String status;
	public String message;
	@SerializedName("err_attributes")
	public String[] errAttributes;
}
