package jaim.common.model;

import jaim.agent.JaimConfig;
import play.cache.Cache;
import play.data.binding.As;
import play.data.binding.types.DateTimeBinder;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.io.Serializable;
import java.util.Date;

/**
 * Table yang berisikan jadwal update yang dijadwalkan untuk update SPSE dengan JAIM
 */

@Table(name = "update_schedule")
public class UpdateSchedule extends BaseTable implements Serializable {

	public static final String CACHE_KEY = "LPSE_UPDATE_SCHEDULE";

	@Id
	public Long id;
	@As(binder= DateTimeBinder.class)
	public Date update_date;
	public Boolean executed = false;
	public Long sp_id;
	public String spse_version;
	public Boolean do_backup;

	public UpdateSchedule(){

	}

	public UpdateSchedule(Long sp_id, String version){
		this.spse_version = version;
		this.sp_id = sp_id;
		this.id = sp_id;
	}

	public static UpdateSchedule findNew(){
		return find("executed = ?", false).first();
	}

	public static UpdateSchedule findReady(){
		return find("executed = ? and update_date is not null", false).first();
	}

	public static UpdateSchedule override(Long sp_id, String version, Boolean doBackup){
		UpdateSchedule oldUpdate = findNew();
		if(oldUpdate != null){
			oldUpdate.executed = true;
			oldUpdate.save();
		}
		UpdateSchedule newUpdate = new UpdateSchedule();
		newUpdate.id = sp_id;
		newUpdate.update_date = new Date();
		newUpdate.sp_id = sp_id;
		newUpdate.spse_version = version;
		newUpdate.do_backup = doBackup;
		newUpdate.save();
		Cache.set(UpdateSchedule.CACHE_KEY, newUpdate);

		return newUpdate;
	}

	public static void delete(UpdateSchedule updateSchedule){
		if(updateSchedule != null){
			updateSchedule.delete();
			Cache.delete(UpdateSchedule.CACHE_KEY);
		}
	}

	public String serviceUrl() {
		return String.format("%s/servicePack/%s/changelogs", JaimConfig.jaimServer, sp_id);
	}
}
