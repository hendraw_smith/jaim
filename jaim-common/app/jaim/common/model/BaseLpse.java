package jaim.common.model;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.text.SimpleDateFormat;
import java.util.Date;

@MappedSuperclass
public class BaseLpse extends BaseTable {

	/**
	 * lpse_ID diset sesuai dengan yg di LPSE
	 */
	@Id
	public Integer lpse_id;
	public Integer repo_id;
	public String lpse_url;
	public String os_name;
	public String os_version;
	public String java_version;
	public String token;
	public String lpse_nama;
	//info tentang versi spse4 yg aktif; 
	public String spse4_version;
	public Long spse4_build;
	public String ip_address;

	public String getLastBuild() {
		if (spse4_build == null || spse4_build == 0) {
			return formatToStandard(new Date());
		}
		return formatToStandard(toDate(spse4_build.toString()));
	}

	public String formatToStandard(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	public Date toDate(String date) {
		if (date == null || date.equals("")) {
			return null;
		}
		try {
			return new SimpleDateFormat("yyyyMMdd").parse(date);
		} catch (Exception e) {
			return null;
		}
	}

	@Transient
	public String getBuild(){
		return String.format("%su%s", spse4_version, spse4_build);
	}
}
