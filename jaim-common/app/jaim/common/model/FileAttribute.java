package jaim.common.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;

public class FileAttribute implements Serializable{

	/**
	 */
	private static final long serialVersionUID = -8182672517041373750L;

	public String path;
	public Long modified_date;
	public String md5Hash;
	public Long size;
	
	public FileAttribute()
	{
		
	}
	
	public String toString()
	{
		return path;
	}
	
	public FileAttribute(File file, String root) throws IOException
	{
		String name=file.getPath();
		//ensure in unix style
		name=FilenameUtils.separatorsToUnix(name);
		path=name.substring(root.length()+1); //NOT start with /
		
		size=file.length();
		modified_date=file.lastModified();
		FileInputStream is=new FileInputStream(file);
		md5Hash=DigestUtils.md5Hex(is);
		is.close();
	}
	
	/**Check equality with path & MD5Hash
	 * @param fa
	 * @return
	 */
	@Override
	public boolean equals(Object o) {
		FileAttribute fa=(FileAttribute)o;
		return (path.equals(fa.path) 
				&& fa.md5Hash.equals(md5Hash));
	}
	
	@Override
	public int hashCode()
	{
		return path.hashCode() + md5Hash.hashCode();
	}
}
