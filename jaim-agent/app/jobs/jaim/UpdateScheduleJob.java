package jobs.jaim;

import jaim.common.model.UpdateSchedule;
import play.Logger;
import play.cache.Cache;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

/**
 * Created by dadang on 4/19/18.
 */
@OnApplicationStart(async = true)
public class UpdateScheduleJob extends Job {

	public void doJob(){
		Logger.debug("[JAIM-LOGS] Checking new update..");
		UpdateSchedule updateSchedule = UpdateSchedule.findReady();
		if(updateSchedule != null){
			Cache.set(UpdateSchedule.CACHE_KEY, updateSchedule);
			Logger.info("[JAIM-LOGS] there is new update saved to cache!");
			return;
		}
		Logger.info("[JAIM-LOGS] there is not new update saved to cache!");
	}
}
