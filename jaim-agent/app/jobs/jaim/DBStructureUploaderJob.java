package jobs.jaim;

import jaim.agent.JaimConfig;
import jaim.common.dbstructure.DBStructure;
import okhttp3.Response;
import org.apache.commons.io.IOUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.jobs.Job;
import play.libs.WS;

import java.io.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPOutputStream;

/**
 * @author AndikYulianto@yahoo.com
 *
 */
public class DBStructureUploaderJob extends Job {

	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	
	private String request_id;

	private int lpse_id;
	/**
	 * @param request_id
	 */
	public DBStructureUploaderJob(String request_id, int lpse_id) {
		this.request_id = request_id;
		this.lpse_id = lpse_id;
	}

	public void doJob() throws IOException {
		if(isRunning.getAndSet(true))
			return;
		Logger.info("[JAIM-LOGS] [DBStructureUploaderJob] START upload StrukturDatabase ke Jaim");
		DBStructure db=new DBStructure();
		db.reverseEngineer();
		String json=db.getJson();
		File jsonFile=new File(Play.applicationPath + "/tmp/DbStructure.json.gz");
		GZIPOutputStream out=new GZIPOutputStream(new FileOutputStream(jsonFile));
		IOUtils.copy(new ByteArrayInputStream(json.getBytes()), out);
		out.close();
		//kirim json ini ke JaimServer
		String url=String.format("%s/DBStructure/upload/%s/%s",JaimConfig.jaimServer, request_id, lpse_id);
		try (Response response = WS.okUrl(url).body(new FileInputStream(jsonFile)).post()){
			Logger.info("[JAIM-LOGS] [DBStructureUploaderJob] DONE upload StrukturDatabase ke Jaim: %s, size: %,d", response.body().string(), jsonFile.length());
			Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, false);
		}catch (Exception e) {
			Logger.error(e, "[DBStructureUploaderJob] GAGAL upload StrukturDatabase ke Jaim");
			Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, true);
		}
		isRunning.set(false);
	}
	
	public void onException(Throwable t)
	{
		isRunning.set(false);
	}
}
