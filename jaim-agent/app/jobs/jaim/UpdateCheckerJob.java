package jobs.jaim;

import jaim.agent.JaimConfig;
import jaim.common.model.UpdateSchedule;
import okhttp3.Response;
import play.Logger;
import play.cache.Cache;
import play.jobs.Every;
import play.jobs.Job;
import play.libs.WS;

import java.util.Date;

/**
 * Created by dadang on 4/17/18.
 */
@Every("60s")
public class UpdateCheckerJob extends Job {

	public void doJob(){
		// Check koneksi ke jaim
		try (Response response = WS.okUrl(JaimConfig.jaimServer+"/servicePack/versi").get()){
			Logger.debug("UpdateCheckerJob: response : %s", response.body().string());
			Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, false);
		} catch (Exception e){
			Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, true);
			return;
		}

		if(Cache.get(UpdateSchedule.CACHE_KEY) == null){
			Logger.debug("[JAIM-LOGS] No Update..");
			return;
		}

		UpdateSchedule updateSchedule = Cache.get(UpdateSchedule.CACHE_KEY, UpdateSchedule.class);
		if(updateSchedule == null || updateSchedule.update_date == null || updateSchedule.update_date.compareTo(new Date()) > 0){
			Logger.debug("[JAIM-LOGS] No Active Update..");
			return;
		}

		Logger.info("[JAIM-LOGS] New update will be executed..");
		if(!ServicePackDownloaderJob.getInstance(updateSchedule)){
			Logger.info("[JAIM-LOGS] Update job still running");
		}
	}
}
