package jobs.jaim;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import jaim.agent.JaimConfig;
import jaim.agent.ShellScriptExecutor;
import jaim.common.model.UpdateSchedule;
import models.common.CONFIG;
import models.jcommon.config.Configuration;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.cache.Cache;
import play.jobs.Job;
import play.libs.Json;
import play.libs.WS;
import play.mvc.results.BadRequest;

import java.io.*;
import java.net.ConnectException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Job ini melakukan download dari JaimServer atas sp_id tertentu 1. file
 * pkg.xxx.zip 2. file JaimAgent.xxx.sh Keduanya disimpan di folder yg
 * ditentukan oleh jaim.agent.JaimConfig
 * 
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
public class ServicePackDownloaderJob extends Job {

	/**Masih terjadi keanehan perilaku bahwa jika diset TIMEOUT > 60detik maka
	 * akan berlaku TIMEOUT menjadi 60 detik. Kalau < 60 detik bisa
	 */
	private Long sp_id;
	private static AtomicBoolean isRunning = new AtomicBoolean(false);

	private String url;

	private UpdateSchedule updateSchedule;

	public static boolean getInstance(Long sp_id) {
		if (isRunning.getAndSet(true))
			return false;
		new ServicePackDownloaderJob(sp_id).now();
		return true;
	}

	public static boolean getInstance(UpdateSchedule updateSchedule) {
		if (isRunning.getAndSet(true))
			return false;
		new ServicePackDownloaderJob(updateSchedule).now();
		return true;
	}

	private ServicePackDownloaderJob(Long sp_id) {
		this.sp_id = sp_id;
	}

	private ServicePackDownloaderJob(UpdateSchedule updateSchedule) {
		this.sp_id = updateSchedule.sp_id;
		this.updateSchedule = updateSchedule;
	}

	public void doJob() throws Exception {
		Logger.info("[ServicePackDownloaderJob] Download begins..");
		//Step 1, check ke jaim server
		url = String.format("%s/v2/servicePack/%s/confirm", JaimConfig.jaimServer, sp_id);
		JaimConfig.baseApp.prepareFilesAttribute();
		String json = Json.toJson(JaimConfig.baseApp.fileAttributes);
		try (Response response = WS.okUrl(url).setParameter("fileAttributes", json)
				.setParameter("jaimLogDir", JaimConfig.workingDir)
				.setParameter("spseRootDir", JaimConfig.spseRoot)
				.setParameter("backupDbDir", JaimConfig.backupDbDir)
				.setParameter("needMigration", checkMigration())
				.setParameter("do_backup", updateSchedule.do_backup).post()){
			final String token = response.header("token");
			String responsebody = response.body().string();
			JsonElement je = JsonParser.parseString(responsebody);
			String status = je.getAsJsonObject().get("status").getAsString();
			String message = je.getAsJsonObject().get("message").getAsString();
			Logger.info("[ServicePackDownloaderJob] status: %s, %s, %s", response.code(), status, message);
			if (response.isSuccessful()) {
				Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, false);
				StopWatch sw = new StopWatch();
				sw.start();
				// jika ada update, download files dan scriptnya
				if(message.toUpperCase().startsWith("READY_TO_UPDATE")) {
					// Step 2, download zip jika ada file baru/dimodifikasi
					if (message.toUpperCase().endsWith("WITH_FILE")) {
						url = String.format("%s/v2/servicePack/%s/zip", JaimConfig.jaimServer, sp_id);
						try(Response response1 = WS.okUrl(url).get()) {
							ResponseBody respBody = response1.body();
							String contentType = response1.header("content-type");
							if (contentType.equalsIgnoreCase("application/json")) {
								responsebody = respBody.string();
								je = JsonParser.parseString(responsebody);
								status = je.getAsJsonObject().get("status").getAsString();
								message = je.getAsJsonObject().get("message").getAsString();
								throw new BadRequest(status + ", " + message);
							}
							InputStream is = respBody.byteStream();
							save(is, "pkg." + JaimConfig.app_id + ".zip");
						}
					}
					// Step 3, download script
					url = String.format("%s/v2/servicePack/%s/script", JaimConfig.jaimServer, sp_id);
					try(Response response2 = WS.okUrl(url).get()) {
						ResponseBody respBody = response2.body();
						String contentType = response2.header("content-type");
						if (contentType.equalsIgnoreCase("application/json")) {
							status = je.getAsJsonObject().get("status").getAsString();
							message = je.getAsJsonObject().get("message").getAsString();
							throw new BadRequest(status + ", " + message);
						}
						String script = respBody.string();
						// replace _SHELL dengan /bin/bash atau /bin sh
						if (new File("/bin/bash").exists()) {
							script = script.replace("_SHELL", "/bin/bash");
						} else {
							script = script.replace("_SHELL", "/bin/sh");
						}
						script = script.replaceAll("_TOKEN_", token);
						script = script.replace("_LOGGER_URL_", JaimConfig.jaimServer + "/log");
						script = script.replace("_LOGGER_ACTIVITY_URL_", JaimConfig.jaimServer + "/servicePack/" + sp_id + "/");
						File scriptFile = save(script, "JaimAgent." + JaimConfig.app_id + ".sh");
						if (scriptFile.setExecutable(true, true)) {
							sw.stop();
							Logger.info("[ServicePackDownloaderJob] sp_id:%s, dur: %s", sp_id, sw);
							//update schedule jadi executed
							deleteUpdateSchedule(updateSchedule);
							//update versi di config
							if (StringUtils.isNotEmpty(updateSchedule.spse_version)) {
								Configuration.updateConfigurationValue(CONFIG.PPE_VERSI.category, updateSchedule.spse_version);
							}
							//jalankan script
							Logger.info("Jalankan skrip %s", scriptFile);
							new ShellScriptExecutor(scriptFile).in(1);
						}
					}
				} else if(message.equalsIgnoreCase("NO_UPDATE")) {
					//update schedule jadi executed
					deleteUpdateSchedule(updateSchedule);
				}
			} else {
				throw new BadRequest(status + ", " + message);
			}
		} catch (ConnectException e) {
			Logger.error(e, "[ServicePackDownloaderJob] Gagal Download Update");
			Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, true);
		}
		isRunning.set(false);
		Logger.info("[ServicePackDownloaderJob] Download finish..");
	}


	public void onException(Throwable e) {
		isRunning.set(false);
		Logger.error("[ServicePackDownloaderJob] ERROR: %s\n\tURL: %s", e, url);
	}

	private File save(String str, String fileName) throws IOException {
		InputStream stream = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
		return save(stream, fileName);
	}

	private File save(InputStream is, String fileName) throws IOException {
		String tmpFolder = JaimConfig.workingDir;
		File file = new File(String.format("%s/%s", tmpFolder, fileName));
		OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
		IOUtils.copy(is, out);
		out.close();
		return file;
	}

	private void deleteUpdateSchedule(UpdateSchedule updateSchedule){
		if(updateSchedule != null){
			Logger.info("[ServicePackDownloaderJob] %s is executed.", updateSchedule.id);
			updateSchedule.executed = true;
			updateSchedule.save();
			Cache.delete(UpdateSchedule.CACHE_KEY);
			return;
		}
		Logger.info("[JAIM-LOGS] Update schedule is null.");
	}

	/**
	 * return 0 jika tidak perlu atau versi spse < 4.3
	 * return 1 jika hanya butuh migration
	 * return 2 jika hanya butuh backup
	 * return 3 jika butuh migration dan backup
	 * @return
	 */
	private int checkMigration() throws Exception {
		if(Configuration.getConfigurationValue(CONFIG.PPE_VERSI.toString()).endsWith("4.2")){
			return 0;
		}
		Runtime runtime = Runtime.getRuntime();
		String path = String.format("%s/%s", JaimConfig.spseRoot, JaimConfig.app_id);
		boolean needMigration = false;
		boolean needBackup = false;
		Process process = runtime.exec(path + "/spse4 migration");
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String out;
		while ((out = stdInput.readLine()) != null) {
			Logger.info("[ServicePackDownloaderJob] command output: %s", out);
			if(out.contains("Sistem akan Melakukan Migration")) {
				needMigration = true;
			} else if (out.contains("Harap Dilakukan Backup Database SPSE")) {
				needBackup = true;
			}
		}
		if(needMigration && needBackup) {
			return 3;
		} else if (needMigration) {
			return 1;
		} else if (needBackup) {
			return 2;
		}
		return 0;
	}

}
