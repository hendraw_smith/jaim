/**
 *
 */
package jobs.jaim;

import jaim.agent.JaimConfig;
import jaim.common.model.ResponseCheck;
import jaim.common.model.UpdateSchedule;
import models.jcommon.config.Configuration;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.libs.Json;
import play.libs.WS;

import java.io.IOException;

/**
 * @author AndikYulianto@yahoo.com
 *
 */
@NoTransaction
//@OnApplicationStart(async = true)
//@Every("1h")
public class JaimJob extends Job {

	@Override
	public void doJob() throws IOException {
		if("true".equals(System.getProperty("jaim.checker.disabled")) && Play.mode== Play.Mode.DEV){
			Logger.info("[JAIM-LOGS] [SPSE UPDATE] Jaim update checker job DISABLED!");
			return;
		}
		Logger.info("[JAIM-LOGS] Check update start..");
		doWithJob();
		Logger.info("[JAIM-LOGS] Check update finish..");
	}

	public void doWithJob() throws IOException {
		UpdateSchedule updateSchedule = UpdateSchedule.find("executed = ?", false).setAutoCloseConnection(true).first();
		if(updateSchedule != null){
			Logger.info("[JAIM-LOGS] There is pending update. No need to check to JAIM Server");
			return;
		}
		int lpse_id = Configuration.getInt("ppe.id");
		String lpse_versi = Configuration.getConfigurationValue("ppe.versi");
		String url = String.format("%s/servicePack/%s/check", JaimConfig.jaimServer, lpse_id);
		JaimConfig.baseApp.prepareFilesAttribute();
		String json = Json.toJson(JaimConfig.baseApp.fileAttributes);
		String uuid = Play.configuration.getProperty("spse.uuid");
		try (Response response = WS.okUrl(url).setParameter("fileAttributes", json)
				.setParameter("lpse_versi", lpse_versi).setParameter("uuid", uuid).post()){
			String responsebody = response.body().string();
			Logger.info("[JAIM-LOGS] code: " + response.code() + " message: %s", responsebody );
			if (!StringUtils.isEmpty(responsebody)) {
				Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, false);
				Logger.info("[JAIM-LOGS] response: " + responsebody);
				ResponseCheck responseCheck = Json.fromJson(responsebody, ResponseCheck.class);
				if (!responseCheck.needUpdate) {
					Logger.info("[JAIM-LOGS] Application already up to date!");
					return;
				}
				updateSchedule = new UpdateSchedule();
				updateSchedule.id = responseCheck.spId;
				updateSchedule.sp_id = responseCheck.spId;
				updateSchedule.spse_version = responseCheck.spseVersion;
				updateSchedule.do_backup = true;
				updateSchedule.save();
				Logger.info("[JAIM-LOGS] " + Json.toJson(updateSchedule));
				Logger.info("[JAIM-LOGS] new Update inserted. Waiting for PPE to set schedule");
			}
		} catch (Exception e) {
			Logger.error(e, "[JAIM-LOGS] Ada kendala akses Jaim Server");
			Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, true);
		}
	}
}
