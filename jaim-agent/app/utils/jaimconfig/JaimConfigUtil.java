package utils.jaimconfig;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import jaim.agent.JaimConfig;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.libs.Json;
import play.libs.WS;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author HanusaCloud on 11/23/2018
 */
public class JaimConfigUtil {

    private static final String CONFIG_CACHE_KEY = "CUSTOM_CONFIG";

    public static void setConfig() {
        String uuid = Play.configuration.getProperty("spse.uuid", "");
        if (!StringUtils.isEmpty(uuid)) {
            Logger.debug("JaimConfigUtil - request config from jaim");
            try (Response response = WS.okUrl(JaimConfig.jaimServer + "/lpse/conf").setHeader("uuid", uuid).get()){
                Logger.debug("JaimConfigUtil - httpCode: " + response.code());
                String responsebody = response.body().string();
                Logger.debug("JaimConfigUtil - response: " + responsebody);
                if (!StringUtils.isEmpty(responsebody)) {
                    Type type = new TypeToken<List<ConfigModel>>() {}.getType();
                    // remove all old config
                    String oldConfJson = Cache.get(CONFIG_CACHE_KEY, String.class);
                    if(StringUtils.isNotEmpty(oldConfJson)) {
                        List<ConfigModel> oldConfigs = Json.fromJson(oldConfJson, type);
                        for(ConfigModel cm : oldConfigs) {
                            Play.configuration.remove(cm.key);
                        }
                    }
                    // add all new config
                    boolean confReplaced = Cache.safeReplace(CONFIG_CACHE_KEY, responsebody, null);
                    Logger.debug("[JaimConfigUtil] save - config replaced: " + confReplaced);
                    if(confReplaced) {
                        List<ConfigModel> newConfigs = Json.fromJson(responsebody,type);
                        for (ConfigModel model : newConfigs) {
                            Play.configuration.put(model.key, model.value);
                        }
                    }
                }
            } catch (Exception e) {
                Logger.error(e, e.getMessage());
            }
        }
    }

    private static class ConfigModel {

        @SerializedName("key")
        public String key;
        @SerializedName("value")
        public String value;

    }

}
