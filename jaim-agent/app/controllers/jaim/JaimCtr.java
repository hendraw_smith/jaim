package controllers.jaim;

import com.google.gson.JsonObject;
import jaim.agent.JaimConfig;
import jaim.agent.JaimSysInformation;
import jaim.common.model.UpdateSchedule;
import jobs.jaim.DBStructureUploaderJob;
import jobs.jaim.ServicePackDownloaderJob;
import okhttp3.Response;
import play.Logger;
import play.libs.Json;
import play.libs.WS;
import play.libs.ws.FileParam;
import play.mvc.Before;
import play.mvc.Controller;
import utils.jaimconfig.JaimConfigUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static jaim.common.model.UpdateSchedule.delete;

/**
 * Controller ini menerima request dari JaimServer saja.
 * Security harus memastikan hal ini.
  * @author andik
 *
 */
public class JaimCtr extends Controller {

	/**Sistem akan memastikan bahwa request ini dari jaim
	 * Jika mau matikan securty, tambahkan -DjaimServer.security.disabled=true
	 */
	private static boolean securityCheckDisabled="true".equals(System.getProperty("jaimServer.security.disabled","true"));

	@Before
	public static void securityCheck() {
		/**
		 * TODO Mencari metode lain selain validasi IP Jaim Server
		 */
		if(play.Play.mode.isProd() && !securityCheckDisabled) {
			if(!request.remoteAddress.contains("103.13.181.200")) {
				renderJSON("{\"message\":\"Who are you?\"}");
			}
		}
	}

	/**Jaim server melakukan touch, yg akan men-trigger Job melakukan download dari JaimServer
	 * @param spid
	 */
	public static void touch(Long spid, String version, Boolean db) {
		UpdateSchedule updateSchedule = UpdateSchedule.override(spid, version, db);
		if(ServicePackDownloaderJob.getInstance(updateSchedule))
			renderText("OK");
		else {
			delete(updateSchedule);
			response.status=500;
			renderText("ServicePack is being downloaded");
		}
	}
	
	/**
	 * JaimServer meminta status atas sp_id tertentu.
	 * @param sp_id
	 */
	public static void status(String sp_id) {
		//get log
		if(sp_id.contains(".."))
			forbidden();
		File updateLog = new File(String.format("%s/jaim/JaimAgent.%s.log", System.getProperty("java.io.tmpdir"), sp_id));
		File backupLog = new File(String.format("%s/jaim/backup_db_logs/Backup.%s.log", System.getProperty("java.io.tmpdir"), sp_id));
		List<FileParam> fileParams = new ArrayList<>();
		if(updateLog.exists()){
			Logger.info("[JAIM-LOGS] [JaimCtr] Update logs is exist..");
			fileParams.add(new FileParam(updateLog, "JaimAgent.log"));
		}
		if(backupLog.exists()){
			Logger.info("[JAIM-LOGS] [JaimCtr] Backup logs is exist..");
			fileParams.add(new FileParam(backupLog, "Backup.log"));
		}

		if(updateLog.exists() || backupLog.exists()) {
			String url=String.format("%s/servicePack/status/%s", JaimConfig.jaimServer, sp_id);
			try(Response response = WS.okUrl(url).files(fileParams.toArray(new FileParam[0])).post()){
				Logger.debug("JaimCtr::status : %s => %s", sp_id, response.code());
			}
			Logger.info("[JAIM-LOGS] [JaimCtr] status: %s, Found", sp_id);
		} else {
			Logger.info("[JAIM-LOGS] [JaimCtr] status: %s, NotFound: %s", sp_id, updateLog);
			forbidden("FileNotFound");
		}
	}

	public static void dbStructure(String request_id, int lpse_id) {
		new DBStructureUploaderJob(request_id, lpse_id).now();
		renderText("OK"); 
	}

	public static void getSystemInfo() {
		JsonObject result = new JsonObject();
		result.addProperty("message", "success");
		result.add("data", Json.toJsonTree(JaimSysInformation.getInformation()));
		renderJSON(JaimSysInformation.getInformation());
	}

	public static void confirmConfig() {
		JaimConfigUtil.setConfig();
	}
}
