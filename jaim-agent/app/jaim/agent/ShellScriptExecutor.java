package jaim.agent;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import play.Logger;
import play.db.Configuration;
import play.db.DB;
import play.jobs.Job;
import utils.JdbcUrl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**Jalankan shell script.
 * @author andik
 */
public class ShellScriptExecutor extends Job {

	private File script;
	public ShellScriptExecutor(File scriptFile)
	{
		this.script=scriptFile;
	}
	
	public void doJob()
	{
		try {
			if(SystemUtils.IS_OS_WINDOWS) {
				Logger.error("[ShellScriptExecutor] script is not executable in Windows: %s", script);
			}	
			else {
				Logger.info("[JAIM-LOGS] executing script..");
				Configuration configuration = new Configuration(DB.DEFAULT);
				String configDb = configuration.getProperty("db.url");
				String host = JdbcUrl.getHostName(configDb);
				Integer port = JdbcUrl.getPort(configDb);
				if (port <= 0) {
					port = 5432;
				}
				String dbName = JdbcUrl.getDatabaseName(configDb);
				String username = configuration.getProperty("db.user", "postgres");
				String password = configuration.getProperty("db.pass", "postgres");
				ProcessBuilder processBuilder = new ProcessBuilder(
						script.getPath(),
						username,
						host,
						String.valueOf(port),
						dbName,
						password);
				Process proc = processBuilder.start();
				InputStream is=proc.getInputStream();
				InputStream err=proc.getErrorStream();
				
				String str=IOUtils.toString(is,"UTF-8") + IOUtils.toString(err,"UTF-8");
				
				Logger.info("[JAIM-LOGS] [ShellScriptExecutor] script: %s\nResult: %s", script, str);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
