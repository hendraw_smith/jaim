package jaim.agent;

import jaim.common.model.BaseApplication;
import play.Logger;
import play.Play;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JaimConfig {

	public static final String JAIM_CONNECTION_FAILED = "JAIM_CONNECTION_FAILED";

	public static final String JAIM_CONNECTION_TIMEOUT = Play.configuration.getProperty("jaim.ConnectionTimeout","5min"); // default connection timeout, harus besar karena bisa lpse ada dari sabang - merauke

	/**Ketiga field ini bisa di-override dengan -Djaim.xxx=abc ketika mode development
	 * Saat production, maka otomatis akan berisi sbb
	 * app_id: spse-prod-4.2 (yaitu parent dari webapp)
	 * spseRoot: /home/appserv
	 * workingDir: $TEMP (yaitu TEMP dari UNIX)
	 */
	public static String app_id;
	public static String spseRoot;
	public static final String workingDir = System.getProperty("jaim.workingDir", System.getProperty("java.io.tmpdir") + "/jaim"); // default /tmp/jaim
	public static final String jaimServer = System.getProperty("jaim.jaimServer", "https://jaim.lkpp.go.id"); // default https://jaim.lkpp.go.id
	public static String backupDbDir;
	public static BaseApplication baseApp;
	
	static  {
		// dapatkan /home/appserv
		spseRoot = System.getProperty("jaim.spseRoot", Play.applicationPath.getAbsoluteFile().getParentFile().getParentFile().getAbsolutePath());
		//dapat base folder spse
		app_id = System.getProperty("jaim.app_id", Play.applicationPath.getAbsoluteFile().getParentFile().getName());			
		Path pathDir = Paths.get(workingDir);
		if(!Files.exists(pathDir)){
			try {
				Files.createDirectories(pathDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		backupDbDir = System.getProperty("jaim.backupDbDir", Play.applicationPath.getAbsoluteFile()
				.getParentFile().getParentFile().getParentFile().getAbsolutePath() + "/backup_db");
		Path backupPathDir = Paths.get(backupDbDir);
		if(!Files.exists(backupPathDir)){
			try {
				Files.createDirectories(backupPathDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Logger.info("[JAIM-LOGS] [JaimConfig] app_id : %s, spseRoot : %s, jaimServer : %s, WorkingDir : %s, backupDir : %s",
				app_id, spseRoot, jaimServer, workingDir, backupDbDir);
		baseApp = new BaseApplication(JaimConfig.app_id, JaimConfig.spseRoot);
	}
}
