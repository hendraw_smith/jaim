package jaim.agent;

import models.jcommon.sysinfo.SystemInformation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class JaimSysInformation {

	public Map<String, Object> informations;
	private JaimSysInformation() {
		SystemInformation lpseSysInformation = SystemInformation.getSystemInformation();
		informations = lpseSysInformation.informations;
		try {
			informations.put("zip", getZipStatus());
			informations.put("curl", getCurlStatus());
			informations.put("PostgreSQL", getPostgresStatus());
			informations.put("pg_dump", getPgDumpStatus());
			informations.put("jvm", getJVMDetail());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Map<String, Object> getInformation () {
		JaimSysInformation jaimSysInformation = new JaimSysInformation();
		return jaimSysInformation.informations;
	}

	private String getZipStatus() throws IOException {
		return execCommand("zip -L", 1);
	}

	private String getCurlStatus() throws IOException {
		return execCommand("curl -V", 2);
	}

	private String getPostgresStatus() throws IOException {
		return execCommand("psql -V", 1);
	}

	private String getPgDumpStatus() throws IOException {
		return execCommand("pg_dump -V", 1);
	}

	private String getJVMDetail() {
		return "version: " + System.getProperty("java.vm.version") + ". " +
				"name: " + System.getProperty("java.vm.name") + ". " +
				"vendor: " + System.getProperty("java.vm.vendor");
	}

	private String execCommand(String command, int readLine) throws IOException {
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(command);
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String out;
		StringBuilder result = new StringBuilder();
		int i = 0;
		while ((out = stdInput.readLine()) != null && i < readLine) {
			result.append(". ").append(out);
			i++;
		}

		return result.substring(2);
	}
}
